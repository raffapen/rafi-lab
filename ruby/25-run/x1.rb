#!/usr/bin/env ruby

require 'Bento'

# which(cmd) :: string or nil
#
# Multi-platform implementation of "which".
# It may be used with UNIX-based and DOS-based platforms.
#
# The argument can not only be a simple command name but also a command path
# may it be relative or complete.
#
def which(cmd)
	cmd = cmd.to_s
	return nil if cmd.empty?

	case RbConfig::CONFIG['host_os']
	when /cygwin/
		exts = nil
	when /dos|mswin|^win|mingw|msys/
		pathext = ENV['PATHEXT']
		exts = pathext ? pathext.split(';').select{ |e| e[0] == '.' } : ['.com', '.exe', '.bat']
	else
		exts = nil
	end

	if cmd[File::SEPARATOR] || (File::ALT_SEPARATOR && cmd[File::ALT_SEPARATOR])
		if exts
			ext = File.extname(cmd)
			if !ext.empty? && exts.any?{ |e| e.casecmp(ext).zero? } && File.file?(cmd) && File.executable?(cmd)
				return File.absolute_path(cmd)
			end
			exts.each do |ext|
				exe = "#{cmd}#{ext}"
				return File.absolute_path(exe) if File.file?(exe) && File.executable?(exe)
			end
		else
			return File.absolute_path(cmd) if File.file?(cmd) && File.executable?(cmd)
		end
  	else
		paths = ENV['PATH']
		paths = paths ? paths.split(File::PATH_SEPARATOR).select{ |e| File.directory?(e) } : []
		if exts
			ext = File.extname(cmd)
			has_valid_ext = !ext.empty? && exts.any?{ |e| e.casecmp(ext).zero? }
			paths.unshift('.').each do |path|
				if has_valid_ext
					exe = File.join(path, "#{cmd}")
					return File.absolute_path(exe) if File.file?(exe) && File.executable?(exe)
				end
				exts.each do |ext|
					exe = File.join(path, "#{cmd}#{ext}")
					return File.absolute_path(exe) if File.file?(exe) && File.executable?(exe)
				end
			end
		else
			paths.each do |path|
				exe = (~path)/cmd
				return File.absolute_path(exe) if File.file?(exe) && File.executable?(exe)
			end
		end
	end
	nil
end

# IO.popen(["putty", "10.0.10.164"])
# system("putty", "10.0.10.164")

bb
puts which('node')
