require 'zip'

Zip::File.open("test.vob.zip") do |zip|
	zip.each do |f|
		path = File.join("test", f.name)
		FileUtils.mkdir_p(File.dirname(path))
		zip.extract(f, path) unless File.exist?(path)
	end
end
