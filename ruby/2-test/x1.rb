
require 'minitest/autorun'

class MyTest < MiniTest::Test

	def initialize(name)
		puts "MyTest!"
		@mytest = "oh!"
		super(name)
	end

end

class Test1 < MyTest

	def setup
	end
	
	def teardown
	end
	
	def test_1
		assert_equal "oh!", @mytest
	end
end
