require 'yaml'

module Psych
module Visitors

class YAMLTree < Psych::Visitors::Visitor
  def emit_coder(c)
	case c.type
	when :scalar
	  @emitter.scalar c.scalar, nil, c.tag, c.tag.nil?, false, Nodes::Scalar::ANY
	when :seq
	  @emitter.start_sequence nil, c.tag, c.tag.nil?, c.style
	  c.seq.each do |thing|
		accept thing
	  end
	  @emitter.end_sequence
	when :map
	  @emitter.start_mapping nil, c.tag, c.implicit, c.style
	  c.map.each do |k,v|
		accept k
		accept v
	  end
	  @emitter.end_mapping
	when :object
	  accept c.object
	end
  end
end

end # Visitors
end # Psych

class Array
	def encode_with(coder)
		coder.style = Psych::Nodes::Mapping::FLOW
		coder.tag = nil
		coder.seq = self
	end
end

puts ['a','b','c'].to_yaml
