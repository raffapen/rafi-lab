rem = <<rem
@echo off
setlocal
call devito-setenv.bat
%DEVITO_RUBY% %~dpn0.bat %*
exit /b %errorlevel%
rem

require 'Bento'
require 'Devito'

# Bento::program = "bat2exe"
Bento::Log.to = ENV["DEVITO_LROOT"] + "\\logs\\#{File.basename($0, ".*")}.log"

#----------------------------------------------------------------------------------------------

docopt = <<DOCOPT
Batch-to-exe compiler.

Usage:
  bat2exe [options] <batfile>

Options:
  -i --icon <icon>  Icon file.

  -h --help      Show this screen.
  --version      Show version.
  -n --nop       Print commands, don't execute.

DOCOPT

#----------------------------------------------------------------------------------------------

class Command < Bento::Docopt::Command
	def initialize(docopt)
		super(docopt)
	end

	def run
		bat2exe = Devito::CmdCompiler.new(options.batfile, options.icon)
		bat2exe.compile
	end
end

#----------------------------------------------------------------------------------------------

Command.new(docopt).run!
