#!/usr/bin/env ruby

require 'Bento'
require 'binding_of_caller'

#----------------------------------------------------------------------------------------------

def report_binding(b)
	r = b.receiver
	if r.is_a?(Class) || r.is_a?(Module)
		c = r.name
	else
		c = r.class.name
	end
	
	c
end

#----------------------------------------------------------------------------------------------

module B

class Error < RuntimeError; end

def B.caller_module_error_class(b)
	r = b.receiver
	if r.is_a?(Class) || r.is_a?(Module)
		c = r.name
	else
		c = r.class.name
	end
	
	klass = c.split("::")[-1]
	err_class = eval(c + "::Error") rescue nil
	return err_class if err_class
	
	mod_name = c.split("::")[0..-2].join("::")
	mod_name = klass if mod_name.empty?
	err_class = eval(mod_name + "::Error") rescue nil
	
	rescue
		nil
end

def B.error(*opt)
	eclass = caller_module_error_class(binding.of_caller(1))
	if eclass
		raise eclass, *opt
	else
		raise B::Error, *opt
	end
end

end

#----------------------------------------------------------------------------------------------

module A

class Error < B::Error; end
	
class Foo
	def initialize
		puts "created A::Foo"
	end
	
	def bar
		jojo
		Bento.fread('x1.rb1')
		#rescue => x
		#	B.error(x, "failed in A::jojo")
	end
	
	def jojo
		B.error("failed in Foo::jojo")
	end
	
	class Error < A::Error; end
end

def A.fail_in_module
	B.error "error in A.fail_in_module"
end

end # module A

#----------------------------------------------------------------------------------------------

def main
	# A.fail_in_module
	# B.error("failed in main")
	foo = A::Foo.new
	foo.bar
end

#----------------------------------------------------------------------------------------------

begin
	bb
	main
rescue A::Foo::Error => x
	puts "Foo::Error: #{x}"
rescue A::Error => x
	bb
	puts "A::Error: #{x}"
rescue B::Error => x
	bb
	puts "application error: #{x}"
rescue => x
	puts "internal error: #{x}"
	bb
end

#----------------------------------------------------------------------------------------------
