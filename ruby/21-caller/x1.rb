#!/usr/bin/env ruby

require 'Bento'
require 'binding_of_caller'

def report_binding(b)
	r = b.receiver
	if r.is_a?(Class)
		c = r.name
	else
		c = r.class.name
	end
	
	klass = c.split("::")[-1]
	mod_name = c.split("::")[0..-2].join("::")
	mod = eval(mod_name)

	c
end

def caller_module_error_class(binding)
end

module B

class E
	def self.error(x)
		# puts "binding.receiver=" + report_binding(binding)
		# puts "binding.of_caller(0).receiver=" + report_binding(binding.of_caller(0))
		# puts "binding.of_caller(1).receiver=" + report_binding(binding.of_caller(1))
		# puts "binding.of_caller(1).receiver=" + binding.of_caller(1).receiver.class.name
		
		eclass = caller_module_error_class(binding.of_caller(1))
	end
end

def B.error(x)
	puts x
end

end

module A

class Error < Exception
	def initialize(*opt)
		super(*opt)
	end
end
	
class Foo
	def initialize
		puts "A::Foo"
	end
	
	def bar
		jojo
	end
	
	def jojo
		x = binding.receiver
		bb
		B::E.error(binding, "jojo")
		B.error("jojo1")
		bb
	end
end

end # module A

def main
	foo = A::Foo.new
	foo.bar
end


main
