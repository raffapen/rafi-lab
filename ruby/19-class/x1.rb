
class A
	private_class_method :new

	attr_reader :name

	def A.create(name)
		@name = name
		puts name
	end
end

class B < A
	#public_class_method :new

	def B.make(name)
		@name = name
		puts name
	end
end


begin
	A.new
rescue => x
	puts x.to_s
end

A.create('jojo')

begin
	B.new
rescue => x
	puts x.to_s
end

B.make('vivi')
