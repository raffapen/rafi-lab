
require 'Bento'

module M

class A
	include Bento::Class

	constructors :is

	attr_reader :name

	def is(name)
		@name = name
		puts name
	end
end

class B < A
	#public_class_method :new

	constructors :is

	def is(name)
		bb
		super(name)
		bb
	end

	def B.make(name)
		@name = name
		puts name
	end
end

end # module M

begin
	M::A.new
rescue => x
	puts x.to_s
end

M.A('jojo')

begin
	M::B.new
rescue => x
	puts x.to_s
end

bb
M::B.make('vivi')
M.B('vivi')

puts 'fin'
