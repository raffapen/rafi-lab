
require 'Bento'

module M

class A
	include Bento::Class

	constructors :is

	attr_reader :name

	def is(name)
		@name = name
		puts Self.foo
	end

	def self.foo
		"A::foo"
	end
end

class B < A
	include Bento::Class
	constructors :is

	def is(name = "")
		super(name)
		puts Self.foo
	end
	
	def self.foo
		"B::foo"
	end
end

end # module M

bb
a = M.A("a")
b = M.B()
b = M.B
puts M::B.foo

puts 'fin'
