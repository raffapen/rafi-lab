
module Fn
	def fn(*args)
		puts args
	end
end

class A
	extend Fn

	def foo; end

	fn :foo

	def foo
		puts "foo"
	end
end


class A
	def bar
		puts "bar"
	end
end

a = A.new
a.foo
a.bar

