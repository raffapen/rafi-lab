module Funkify
  def self.included(klass)
    klass.extend ClassMethods
  end

  module ClassMethods
    def auto_curry(*names)
		puts names
    end
  end
end

module ClassMethods1
	def auto_curry(*names)
		puts names
	end
end

class C1
  # include Funkify
  extend ClassMethods1

  auto_curry add(x, y); end
  
  auto_curry def add(x, y)
    x + y
  end
end


c = C1.new
puts c.add(1,2)
