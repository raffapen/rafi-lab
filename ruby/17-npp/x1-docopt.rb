require 'Bento'
require "pp"

docopt = <<DOCOPT
Notepad++ - Text editor

Usage:
  notepad++ [--multiInst] [--no-plugin] [--loadingTime]  
			[--no-session] [--openSession <session>]
			[-l<Language>] [-L<langCode>]
            [--alwaysOnTop] [--no-tabbar] [--systemtray] [-x<LeftPos>] [-y<TopPos>]
			[--qn <EsterEggName> | --qt <Text> | --qf <CntentFileName>]
			[--ro] [-r] [-n<LineNumber>] [-c<ColumnNumber>] [-p<Position>] [<filePath>]
  notepad++ [--version | -h | --help]

Arguments:
  <filePath>                File or folder name to open (absolute or relative path name)

Options:
  --multiInst               Launch another Notepad++ instance
  --no-plugin               Launch Notepad++ without loading any plugin
  --loadingTime             Display Notepad++ loading time

  --config <cfg>            Global configuration directory
  --hostconfig <cfg>        Host configuration directory
  --userconfig <cfg>        User configuration directory
  --appconfig <cfg>         Application configuration directory

  --no-session              Launch Notepad++ without previous session
  --openSession <session>   Open a session. filePath must be a session file

  -l <Language>             Open filePath by applying indicated programming language
  -L <langCode>             Apply indicated localization, langCode is browser language code

  --alwaysOnTop             Make Notepad++ always on top
  --no-tabbar               Launch Notepad++ without tabbar
  --systemtray              Launch Notepad++ directly in system tray
  -x <LeftPos>              Move Notepad++ to indicated left side position on the screen
  -y <TopPos>               Move Notepad++ to indicated top position on the screen

  --ro                      Make the filePath read only
  -r                        Open files recursively. This argument will be ignored
                              if <filePath> contain no wildcard character
  -n <LineNumber>           Scroll to indicated line on filePath
  -c <ColumnNumber>         Scroll to indicated column on filePath
  -p <Position>             Scroll to indicated position on filePath

  --qn <EsterEggName>       Launch ghost typing to display easter egg via its name
  --qt <Text>               Launch ghost typing to display a text via the given text
  --qf <CntentFileName>     Launch ghost typing to display a file content via the file path

  -h --help                 Print help message
  --version                 Print version

DOCOPT

class Command < Bento::Docopt::Command
	def initialize(docopt)
		super(docopt)
	end

	def run
		pp options
	end
end

#----------------------------------------------------------------------------------------------

bb
Command.new(docopt).run!
