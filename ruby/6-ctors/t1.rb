
require 'minitest/autorun'
require_relative 'x1.rb'

class Test1 < Minitest::Test

	def setup
	end
	
	def test_is
		
		assert_equal "i am spartacus", M.A("spartacus").name
		assert_equal "i am spartacus", M::A.is("spartacus").name
		assert_raises NoMethodError do
			M.A("spartacus").is("no can do")
		end
	end
	
	def test_create
		assert_equal "world created.", M::A.create("world").name
		assert_raises NoMethodError do
			M.A("spartacus").create("no can do")
		end
	end
	
	def test_members
		assert_equal [:name], M::A.class_members
	end

	def test_ctors
		assert_equal [:is, :create], M::A.ctors
	end

	def test_new
		assert_raises NoMethodError do
			M::A.new("no can do")
		end
	end

#	def test_
#	end
end

class Test1 < Minitest::Test

	def setup
	end
	
	def test_is
		
		assert_equal "i am spartacus", M.B("spartacus").name
		assert_equal "i am spartacus", M::B.is("spartacus").name
		assert_raises NoMethodError do
			M.B("spartacus").is("no can do")
		end
	end
	
	def test_create
		assert_equal "world created.", M::B.create("world").name
		assert_raises NoMethodError do
			M.B("spartacus").create("no can do")
		end
	end
	
	def test_members
		assert_equal [:name], M::B.class_members
	end

	def test_ctors
		assert_equal [:is, :create], M::B.ctors
	end

	def test_new
		assert_raises NoMethodError do
			M::B.new("no can do")
		end
	end

#	def test_
#	end
end
