
module Class1
	def self.included(base)
		base.extend(ClassMethods)
	end
	
	module ClassMethods
		def constructors(*ctors)
			klass = self.name
			mod = eval(klass.split("::")[0..-2].join("::"))

			class_eval("private_class_method :new")

			ctors.each do |ctor|
				mod.class_eval(<<END) if ctor == :is
def #{klass}(*args)
	x = #{klass}.send(:new)
	x.send(:is, *args)
	x
end
END
				class_eval("private :" + ctor.to_s)
				class_eval(<<END)
def self.#{ctor}(*args)
	x = self.send(:new)
	x.send(:#{ctor}, *args)
	x
end
END
			end
		end
		
		def members(*vars)
			class_eval("@@members ||= []")
			vars.each do |v|
				class_eval("@@members << :#{v}")
			end
		end
	end
end

module ClassMethods

	def constructors(*ctors)
		class_eval("@@ctors ||= []")
		class_eval("@@ctors += " + ctors.to_s)
#		puts "@@ctors.include? :is " + class_eval("@@ctors").include?(:is).to_s
	
		klass = self.name
		mod = eval(klass.split("::")[0..-2].join("::"))

		class_eval("private_class_method :new")

		ctors.each do |ctor|
			mod.class_eval(<<END) if ctor == :is
				def #{klass}(*args)
					x = #{klass}.send(:new)
					x.send(:is, *args)
					x
				end
END
			begin
				class_eval("private :" + ctor.to_s)
			rescue
			end

			class_eval(<<END)
				def self.#{ctor}(*args)
					x = self.send(:new)
					x.send(:#{ctor}, *args)
					x
				end
END
		end
	end
	
	def method_added(m)
#		puts m
#		@@ctors ||= []
#		puts "include? " + (class_eval("@@ctors").include?(m) ? "yes" : "no")
		class_eval("@@ctors ||= []")
		class_eval("private :" + m.to_s) if class_eval("@@ctors").include?(m)
	end
	
	def members(*vars)
		class_eval("@@members ||= []")
		vars.each do |v|
			class_eval("@@members << :#{v}")
		end
	end
	
	def class_members
		class_eval("@@members")
	end
	
	def ctors
		class_eval("@@ctors")
	end

end

module Klass
	def self.included(base)
		base.extend(ClassMethods)
	end
end

module M

module Meat
	attr_reader :name

	def is(text, *opt)
		@name = "i am " + text
	end

	def create(name, *opt)
		@name = name + " created."
	end

#	def self.is(*args)
#		x = self.send(:new); x.send(:is, *args); x
#	end

#	def self.create(*args)
#		x = self.send(:new); x.send(:create, *args); x
#	end
	
#	private :is, :create
#	private_class_method :new
end

# def self.A(*args)
#	x = A.send(:new); x.send(:is, *args); x
# end

class A
	include Klass
	# extend ClassMethods
	
	constructors :is, :create
	members :name

	include Meat
end

class B
	extend ClassMethods

	include Meat

	constructors :is, :create
	members :name
end

end # M
