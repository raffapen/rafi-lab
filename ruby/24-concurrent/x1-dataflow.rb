#!ruby

require 'Bento'
require 'concurrent'
require 'benchmark'

def foo1
	a = Concurrent::dataflow { sleep 4; 10 }
	b = Concurrent::dataflow { sleep 1; 20 }
	c = Concurrent::dataflow(a, b) { |a, b| [] << a << b }
end

def foo2
	ff = []
	ff << Concurrent::dataflow { sleep 4; 4 }
	v = [5, 1, 3]
	v.each do |n|
		ff << Concurrent::dataflow { sleep n; n }
	end
	Concurrent::dataflow(*ff) { |*ff| ff }.value
end

def foo3
	ff = []
	ff << Concurrent::dataflow { sleep 4; [4] }
	v = [5, 1, 3]
	v.each do |n|
		ff << Concurrent::dataflow { sleep n; [n] }
	end
	Concurrent::dataflow(*ff) { |*ff| ff }.value
end

v = nil
t = Benchmark.measure { v = foo3 }.real
bb
puts "fin"