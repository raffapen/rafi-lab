#!/usr/bin/env ruby

require 'Machina'

def foo1(m)
	# y = m.to_yaml
	json = Bento::JSON.from_json(Machina.db["select json from machines where name=?", name].cut.first)
	h = json.to_h.to_h
	y = Bento::YAML.from_obj(" #{name}" => h).to_yaml.sub(/^\" /,'"')
	fname = "d/#{name.gsub(/:/, "_")}.yaml"
	Bento.fwrite(fname, y)
end

def foo2(m)
	m.save_as_yaml(force: true)
end

def foo3
	Machina.Machines().save_as_yaml('d/jojo.yaml', force: true)
end

def foo4
	name = 'zaza:vmwx:ub15'
	m = Machina.Machine(name)
	y = m.save_as_yaml(force: true)
end

bb
# m = Machina::Machine.from_yaml(~"zaza_vmwx_ub15.yaml")
m = Machina.Machine("d/zaza:vmwx:win7-target")
m.save_as_yaml("d/zaza_vmwx_win7-target.1.yaml", force: true)
m = Machina::Machine.from_yaml(~"d/zaza_vmwx_win7-target.yaml", :replace)
m.save_as_yaml("d/zaza_vmwx_win7-target.2.yaml")
puts "fin"

