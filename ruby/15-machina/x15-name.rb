#!/usr/bin/env ruby

require 'Machina'

def foo1
	name = nil
	name = Machina.MachineName('~centos7-1511') rescue nil
	name = Machina.MachineName('ub15-1') rescue nil
	name = Machina.MachineName('rafi_ub15-1') rescue nil
	name = Machina.MachineName('nx1:vmwx:~centos7-1511') rescue nil
end

def foo2
	name = Machina::MachineName.from_tags('[t:fedora-latest]')
	puts name.to_s
end

bb
foo2
puts "fin"