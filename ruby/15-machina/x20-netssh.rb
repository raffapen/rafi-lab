#!/usr/bin/env ruby

require 'Machina'
require 'net/ssh'
require 'base64'

def foo1
	m = Machina.Machine('jojo1')
	cred = m.credentials
	ip = m.ip0
	vmid = m.vmid
	xroot = Devito.xroot # ~ENV['DEVITO_XROOT']
	devito_guest = Bento.fread(xroot/"core/linux/scripts/envenido/root/etc/devito.guest")
	devito64 = Base64.strict_encode64(devito_guest)

	cmd64 = Base64.strict_encode64(<<~END)
		#!/bin/bash

		echo "VMID='#{vmid}'" >> /etc/environment
		echo "#{devito64}" | base64 -d > /etc/devito
		echo ". /etc/devito" > /etc/profile.d/devito.sh

		END

	Net::SSH.start(ip.to_s, cred.username, :password => cred.password) do |ssh|
		s = ssh.exec!('cat /etc/os-release')
		s = ssh.exec!("echo '#{cmd64}' | base64 -d > /tmp/intro")
		s = ssh.exec!('. /tmp/intro')
		puts
	end
end

def foo2
	m = Machina.Machine('devito-lin1')
	Net::SSH.start(m.ip0.to_s, "rafi") do |ssh|
		s = ssh.exec!('cat /etc/os-release')
		puts s
	end
end

def foo3
	m = Machina.Machine('jojo1')
	m.ssh do |ssh|
		puts ssh.exec!('echo "$VMID"')
	end
	puts
end

bb
foo3
puts
