
require 'Bento'

bb
t0 = %w(Name Time Description Parent)
t1 = ["v2", "2017-09-02 15:09:11", "Dummy snapshot.", "v1"]
t2 = ["v1", "2016-01-14 13:54:35", "a very very very very long comment", ""]

tt = [t0, t1, t2]
a = tt.reduce([0] * t0.size) { |rr,r| rr.zip(r.map(&:size)).map(&:max) }.map {|n| n+3}
if a.reduce(&:+) > 96
end

tt.each do |t|
	l = ""
	a.each_index do |i|
		l += t[i].ljust(a[i])
	end
	puts l
end
bb
puts "fin"