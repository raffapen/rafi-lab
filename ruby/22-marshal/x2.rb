#!/usr/bin/env ruby

require 'Bento'

def foo3(*opt, desc: "")
	puts opt
	puts desc
	42
end

def marshal64(*args)
	Base64.strict_encode64(Marshal.dump(args))
end

def demarshal64(d64)
	Marshal.load(Base64.decode64(d64))
end

def exec64(cmd, *args)
	args64 = marshal64(*args)
	cmd.gsub!(/__ARGS__/, "*demarshal64('#{args64}')")
	x = eval(cmd)
	y = demarshal64(marshal64(x)).first
	y
end

bb
foo3(:a, :b, :c, desc: "jojo")
exec64("foo3(__ARGS__)", :a, :b, :c, desc: "jojo")
exec64("foo3(__ARGS__)", desc: "jojo")
puts exec64("foo3(__ARGS__)", "jojo")
