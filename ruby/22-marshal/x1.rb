#!/usr/bin/env ruby

require 'Bento'
require 'base64'

def foo1(obj)
	d1 = Marshal.dump(obj)
	d2 = Base64.strict_encode64(d1)
end

def foo2(d2)
	d1 = Base64.decode64(d2)
	obj = Marshal.load(d1)
end

def marshal64(*obj)
	Base64.strict_encode64(Marshal.dump(obj))
end

def demarshal64(d64)
	Marshal.load(Base64.decode64(d64))
end

def marshal_args(*args)
	d1 = Marshal.dump(*args)
	d2 = Base64.strict_encode64(d1)
end

def demarshal_args(args64)
	Marshal.load(Base64.decode64(args64))
end

def foo3(*opt, desc: "")
	puts opt
	puts desc
end

def foo3_(*opt, desc: "")
	args64 = marshal64(*opt, desc: desc)
	foo3(*demarshal64(args64))
end

def exec64(cmd, *args)
	args64 = marshal64(*args)
	cmd1 = cmd.gsub(/__ARGS__/, "*Bento.demarshal64('#{args64}')")
	eval(cmd1)
end

bb
foo3(:a, :b, :c, desc: "jojo")
foo3_(:a, :b, :c, desc: "jojo")
exec64("foo3(__ARGS__)", :a, :b, :c, desc: "jojo")
exec64("foo3(__ARGS__)", desc: "jojo")
exec64("foo3(__ARGS__)", "jojo")
