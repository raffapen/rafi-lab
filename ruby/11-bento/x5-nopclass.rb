
require 'Bento'

module M

class A
	def initialize
	end

	def foo(x)
		puts "foo"
		self
	end

	def bar(x, y)
		puts "bar"
		self
	end

#	class A_nop
#		def foo; self; end
#		def bar; self; end
#	end

	def unless(x)
		return self if x
		A_nop.new
	end

	class << self
		A_nop = Class.new(Object) do
			A.instance_methods(false).each do |m|
				define_method(m) { |*args| self }
			end
		end
		A.const_set("A_nop", A_nop)
	end
end

end # M

bb
M::A.new.foo(1).bar(1,2)
M::A.new.unless(false).foo(1).bar(1,2)
M::A.new.unless(true).foo(1).bar(1,2)

m1 = A.instance_methods(false)
A_nop = Class.new(Object) do
	m1.each do |m|
		define_method(m) { self }
	end
end


a1 = A_nop.new

puts "fin"
