
require 'Bento'

#----------------------------------------------------------------------------------------------

def colorize(text, color_code)
  "\e[#{color_code}m#{text}\e[0m"
end

def foo1(btline, color: 31)
	if parts = btline.match(/^(?<file>.+):(?<line>\d+):in `(?<code>.*)'$/)
		absolute_path = parts[:file].sub /^#{Regexp.escape(File.join(Dir.getwd, ''))}/, ''
		lineno = parts[:line].to_i
	else
		return nil
    end

	fclines = 2
	indent = ' ' * 10

	start_line = lineno - 1 - 1 * fclines
	start_line = 0 if start_line < 0

	additional = btline + "\n"
#	additional << indent + "[FILE]\n"

	additional << open(absolute_path) {|f| f.readlines[start_line, 1 + 2 * fclines]}.map.with_index{|line, i|
	  ln = start_line + i + 1
	  line = line.chomp
	  '%s%4d|%s' % [ln == lineno ? indent[0..-3] + "->" : indent, ln, ln == lineno ? colorize(line, color) : line]
	}.join("\n") << "\n" rescue ''

	additional << "\n"
end

def foo(x, color: 31)
	puts "Exception: " + colorize(x.message, color)
	puts "Cause:     " + colorize(x.cause, color) if x.cause != nil
	puts
	x.backtrace.each { |line| puts foo1(line, color: color) }
	""
end

#----------------------------------------------------------------------------------------------

module App1

class A
	include Bento::Class
	
	constructors :is, :create
	members :name, [:data, Array], [:address, :_address?]

	def __identity
		@name
	end

	def is(name)
		@name = name if name
		@data = [10]
		@address = "sdfsdf sdfsdf"
		__ready
	end
	
	def _address?(a)
		a.split(/\s/).count > 1
	end
end

end # module App1

#----------------------------------------------------------------------------------------------

bb
begin
	a = App1.A("jojo")
	a1 = App1.A(nil)
rescue Exception => x
	bb
	puts foo(x)
end

puts "fin"
