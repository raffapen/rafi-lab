
require 'Bento'
require 'Bento/lib/ControlledFiles'

bb
# cfiles = Bento::ControlledFiles.create("jojo", ".", source: "source")
cfiles = Bento.ControlledFiles("jojo", ".")
if !cfiles.up_to_date?
	s = cfiles.sync
	case s
	when :need_sync
	when :merge_in_progress
		puts "merge is pending on:"
		puts cfiles.unmerged_elements
	end

	x = cfiles.up_to_date?
end
puts
