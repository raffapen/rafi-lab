
require 'Bento'
# require 'Bento/lib/Box'
require 'Machina'

#----------------------------------------------------------------------------------------------

module App1

ROOT = Bento.realdir(__FILE__)

#----------------------------------------------------------------------------------------------

class AppData < Bento::AppData
	include Bento::Class

	constructors :is, :create

	def is(*opt)
		super(*opt)
	end
end

#----------------------------------------------------------------------------------------------

class Box < Bento::Box
	include Bento::Class

	constructors :is, :create

	def create(*opt)
		super(*opt, app: "app1", source: "c:/skel") #"x30/skel")
	end
end

end # module App1

#----------------------------------------------------------------------------------------------

# Test context
## machina: create db and initialize it in test context
# Devito context
## machina: populate machina app in devito context
# create app box from view
# Entering a box, opening a box
# Log within box

def puts_appdata(d)
	puts "root: #{d.root}"
	puts "data: #{d.data_root}"
	puts "app:  #{d.app_root}"
	puts "user: #{d.user_root}"
end

def f1
	a1 = App1::AppData()
end

def f2
	b1 = Machina::Box.create()
end

def f3
	b1 = Machina::Box.create(:test)
	b1.enter
	d1 = Machina::app_data
	d2 = App1::app_data
	# b1.remove!
end

def f4
	# b0 = Bento::Box.create
	b0 = Machina::Box.create(:test)
	b0.enter
	# d1 = Machina::AppData.create
	d1 = Machina.app_data
	db = Machina.db
	puts
end

def f5
	Bento.Box("machina").enter
	info "this is a message"
	d1 = Machina.app_data
	db = Machina.db
	puts
end

begin
	bb
	f5
	puts

rescue => x
	bt(x)
	bb
end
puts "fin"
