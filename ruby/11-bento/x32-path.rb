
require 'Bento'

module M1
	def self.foo=(x)
		@@foo = x
	end
	
	def self.foo
		@@foo
	end
end

def foo(name, *args, **nargs)
	puts args
end

class String
	def ~@
		Pathname.new(self)
	end
end

class Pathname
	def ~@
		self
	end
end

bb
M1.foo = 42
puts M1.foo

bb
foo("jojo")
foo("jojo", 1, 2, 3)
foo(vivi: "vivi", koko: "koko")
foo("jojo", 1, 2, 3, vivi: "vivi", koko: "koko")

p = ~"v:/app"/:media/:bsplayer
p /= "v1.2"
p = ~p
puts p
puts "fin"