
require 'Bento'
require 'json'

def struct(*args)
	Struct.new(*args)
end

def ostruct(*args)
	OpenStruct.new(*args)
end

def hashtab(j)
	t1 = {}
	tabs = {}
	j.keys.each do |k|
		j1 = j[k]
		j1.keys.each do |f|
			if j1[f].is_a?(String)
				t1[f] = 1
			elsif j1[f].is_a?(Hash)
				j1[f].keys.map { |k| "#{f}_#{k}" }.each { |f| t1[f] = 1 }
			elsif j1[f].is_a?(Array)
				tabs["t1_" + f] = 1
			else
				raise "error"
			end
		end
	end
	create_table("t1", field_names: t1.keys << "name")
	tabs.keys.each do |t|
		create_table(t, %w(id, name))
	end
	
	puts t1.keys
	puts tabs.keys
end

def hashskel(h)
	skel = {}
	h.keys.each do |k|
		j = h[k]
		j.keys.each do |f|
			if j[f].is_a?(String)
				skel[f] = 1
			elsif j[f].is_a?(Hash)
				skel[f] = {}
				j[f].keys.each { |g| skel[f][g] = 1 }
			elsif j[f].is_a?(Array)
				skel[f] = []
			else
				raise "error"
			end
		end
	end
	skel
end

def hashskel1(j)
	skel = [:id, :name]
	j.keys.each do |f|
		if j[f].is_a?(Fixnum)
			skel << f.to_sym
		elsif j[f].is_a?(Hash)
			skel << [f.to_sym] + j[f].keys.map(&:to_sym)
		elsif j[f] == []
			skel << [f.to_sym, []]
		else
			raise "error"
		end
	end
	skel
end

js = <<END
{
	"rafie-linux1" : { "hostname": "172.16.82.142", "credentials" : { "username": "root", "passwd": "taligent" }, "os": "linux", "arch": "x86", "tags": [] },
	"rafie-dora" : { "hostname": "172.16.82.101", "credentials" : { "username": "root", "passwd": "RvShos", "domain": "jojo.com" }, "os": "linux", "arch": "ppc", "tags": ["dora"] }
}
END

js1 = <<END
{
	{ "name": 0, "hostname": 1, "credentials" : { "username": 1, "passwd": 1, "domain": 1 }, "os": 1, "arch": 1, "tags": [] }
}
	[ "name", "hostname", [ "credentials", "username", "passwd" ], "os", "arch", { "tags": [] } ]
	[ :name, :hostname, Credentials[:username, :passwd], :os, :arch, { "tags": [] } ]
	[ :name, :hostname, [:credentials, :username, :passwd, :domain], :os, :arch, [:tags, []] ]
END

js2 = <<END
{
	"a" : { "k1" : "s1",  "k2" : { "k1" : "s1", "k2" : "s2" }, "k3" : [ "s1", "s2" ] },
	"b" : { "k5" : "s1",  "k2" : { "k1" : "s1", "k2" : "s2" }, "k4" : [ "s1", "s2" ] } 
}
END


j = JSON.parse(js)
# s0 = hashskel(j)
# s1 = hashskel1(s0)

skel = Bento::HashSkel.from_hash(j)

bb
puts "fiN"

