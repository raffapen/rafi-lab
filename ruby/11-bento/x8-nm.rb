
require "Firestarter"

Bento.nop = true

dev = Firestarter::NetworkManager.Device("eno16777736")

m1 = dev.mac
bb
d2 = Firestarter::NetworkManager::Device.by_mac(m1)

eths = Firestarter::NetworkManager::Devices.ethernet
if eths.count > 1
	puts 'too many eth devices'
	exit
end

eth = eths.first
bb
links = Firestarter::NetworkManager::Connections.device(eth.name)
if links.count > 1
	puts 'too many links'
	exit
end

link = links.first

# link.name = eth.name # or "eth0"

link.set_static("172.16.88.100", "255.255.255.0", "172.16.88.1")
# eth.set_static("172.16.88.100", "/24", "172.16.88.1")
# eth.set_static(ip: "172.16.88.100/24", gw: "172.16.88.1")

puts "fin"

# nmcli c modify eth0 ipv4.addresses 10.0.10.99/24
# nmcli c modify eth0 ipv4.gateway 10.0.10.1
# nmcli c modify eth0 ipv4.dns 10.0.10.10
# nmcli c modify eth0 ipv4.method manual
