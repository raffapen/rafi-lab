
require 'Bento'

class VMSettingsFile
	class << self
		def special(re)
			@@specials ||= []
			@@specials << Regexp.new("^" + re.source + "(\..*|$)")
		end
	end
	
	class Reader
		def initialize(settings, m = nil)
			@settings = settings
		end

		def [](x)
			v = @settings[x]
			v.hash? ? Reader.new(v) : v
		end

		def method_missing(m, *args)
			v = @settings[m.to_s]
			v.hash? ? Reader.new(v) : v
		end

		def value
			@settings.fetch(:value, nil)
		end

		def entry
			@settings.fetch(:entry, nil)
		end

		def settings
			@settings
		end

		def indexes
			keys.select(&:int?)
		end

		def keys
			@settings.keys
		end

		def each
			keys.each { |k| yield(self[k]) }
		end

		def each_index
			indexes.each { |k| yield(self[k]) }
		end
		
		def to_s; value; end
		def to_str; value; end
		
		def to_i; value; end
		def to_int; value; end
		
		def to_a; [value]; end
		def to_ary; [value]; end
	end

	def method_missing(m, *args)
		v = @settings[m.to_s]
		v.hash? ? Reader.new(v) : v
	end

	def settings
		Reader.new(@settings).settings
	end

	def load_file(file)
		raw = Bento.fread(file).lines.map {|x| x.match /(.*) = "(.*)"/; [$1,$2] }
		raw.reject!{|x| x.first == ".encoding"}

		@settings = Bento::Hoh.new
		raw.each do |entry|
			key = entry.first
			fields = key.split(".")
			val = entry[1]
			@@specials.each do |sp|
				m = key.scan(sp)
				next if m.size == 0
				fields = m.first[0..-2] + m.first.last.split(".").drop_while(&:empty?)
				break
			end
			a = fields.map {|x| Integer(x) rescue x}
			
			# a += [val].map {|x| Integer(x) rescue x}.map {|x| x.string? ? ((y = x.to_bool) == nil ? x : y) : x}
			v = [val].map {|x| Integer(x) rescue x}.map {|x| x.string? ? ((y = x.to_bool) == nil ? x : y) : x}.first
			a += [{:value => v, :entry => entry.first}]
			
			h = a.reverse.reduce {|s, x| {x => s}}
			@settings << h
		end
	end

	def pathname
		@file
	end

	def to_s
		@file.to_s
	end
end

class VMXFile < VMSettingsFile
	special /(ethernet)(\d+)/
	special /(floppy)(\d+)/
	special /(hpet)(\d+)/
	special /(ide)(\d+:\d+)/
	special /(pciBridge)(\d+)/
	special /(scsi)(\d+:\d+)/
	special /(scsi)(\d+)/
	special /(serial)(\d+)/
	special /(sharedFolder)(\d+)/
	special /(usb)\:(\d+)/
	special /(vmci)(\d+)/

	def initialize(file)
		load_file(file)
	end
end

class VMSDFile < VMSettingsFile
	special /(snapshot)\.(mru)(\d+)/
	special /(snapshot)(\d+)\.(disk)(\d+)/
	special /(snapshot)(\d+)/

	def initialize(file)
		load_file(file)
	end
end

class Snapshot
	attr_reader :name

	def initialize(snap)
		@snap = snap
	end

	def name
		@snap.displayName
	end

	def time
		high = @snap.createTimeHigh
		low = @snap.createTimeLow
		low = 2 ** 32 + low if low < 0
		Time.at((high * 2**32 + low) / 10 ** 6)
	end

	def description
		@snap.description
	end
end

class Snapshots
	include Enumerable

	def initialize(vmsd)
		@vmsd = vmsd
	end

	def latest
		v = sort {|a,b| a.time <=> b.time }
		v.last
	end

	def each
		return if @vmsd.snapshot == nil
		@vmsd.snapshot.each_index { |snap| yield Snapshot.new(snap) }
	end
end

def snaptime(snap)
	high = snap.createTimeHigh
	low = snap.createTimeLow
	low = 2 ** 32 + low if low < 0
	t = Time.at((high * 2**32 + low) / 10 ** 6)
end

vmx = VMXFile.new('x/vmx1')
bb
puts vmx.displayName
puts vmx.cpuid.coresPerSocket
puts vmx.pciBridge[0].present
puts vmx.usb[0].deviceType

vmsd = VMSDFile.new("x/vmsd1")
bb
t = []
t << snaptime(vmsd.snapshot[0])
t << snaptime(vmsd.snapshot[1])
t << snaptime(vmsd.snapshot[2])

puts vmsd.snapshot[0].uid
puts vmsd.snapshot[0].filename
bb
vmsd.snapshot[0].disk.each do |disk|
	bb
	puts disk.fileName
	puts disk.node
end
