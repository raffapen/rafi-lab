
require 'Bento'
require 'upsert'

# db = Bento::DB.create_from_hash(h, path: "x25.db")
# id = db["select id from t1 where name='ub15'"]["id"]

def f1
#   hosts = YAML.load_file('x/h2.yaml')
#	db = Bento::SQLite::DB.create_from_hash(hosts, path: "x25.db")
	db = Bento::SQLite.DB(path: "x25.db")
	x1 = db["pragma table_info('t1')"]
	t1 = db.table("t1")
end

def f2
	hosts = YAML.load_file('x/h2.yaml')
	sk = Bento::SQLite::HashSkel.from_hash(hosts)
	cols = sk.colnames
	colsp = sk.colspec
	tab = Bento::SQLite::Table.create("t2", )
	bb
end

def f3
	db = Bento::SQLite::DB.create(path: "x27.db")
	# db = Bento::SQLite.DB(path: "x25.db")
	hosts = YAML.load_file('x/h2.yaml')
	f = Bento::SQLite::ColSpec.new({ "id" => "id integer primary key", 
		"name" => "name text", 
		"json" => "json text" })
	# t2 = Bento::SQLite::Table.create(db, "t2", f)
	t2 = db.create_table("t2", f)
	sk = Bento::SQLite::HashSkel.from_hash(hosts)
	colsp = sk.colspec
	t2.create_columns(colsp)
	bb
end

def get_nash(db, table, name, key)
	r = db.one("select json from #{table.to_s} where name=?;", name.to_s)[0]
	return r if !key
	r = JSON.parse(r)[key]
	t = db.one("select #{key.to_s} from #{table.to_s} where name=?;", name.to_s)[0]
	error "incompatible nash record" if r != t
	r
end

def f4
	db = Bento::SQLite::DB.create(path: "x27.db")
	puts "hosts table exist" if db.table(:hosts).exist?

	hosts_n = db.nash(:hosts)
	# hosts_nt = Bento::SQLite::NashTable.create(db, :hosts)

	# hosts_tab = Bento::SQLite.NashTable(db, :hosts)
	# hosts_tab = db.nash_table(:hosts)

	hosts = YAML.load_file('x/h2.yaml')

	# create table :hosts, create hash records in table :hosts
	hosts_n.set(hosts)

	hosts = YAML.load_file('x/h2a.yaml')
	hosts_n.set(hosts)

	all_hosts = hosts_n.all
	puts hosts == all_hosts ? "all hosts equal" : "all hosts not equal"

	# select record with key=:ub15
	bb
	arch = get_nash(db, "hosts", "ub15", "arch")
	ub15 = hosts_n[:ub15]
	arch = ub15["arch"]
	ub15["arch"] = "arm"
	ub15.arch = "arm"
	ub15.update!
	ub15.set!(hosts["ub15"])

	hosts["ub15"]["arch"] = "arm"
	puts "ub15 is arm" if hosts["ub15"]["arch"] == "arm"
	hosts_n["ub15"] = hosts["ub15"]
	ub15 = hosts_n[:ub15]

	hosts1 = hosts_nt.to_h
	puts Bento::YAML.from_obj(hosts1).to_yaml
end

def f5
	db = Bento::SQLite::DB.create(path: "x27.db")
	hosts = db.nash(:hosts)
	hosts_yaml = YAML.load_file('x/h2.yaml')
	hosts.set(hosts_yaml)
	# db.update("hosts", "name='ub15'", ["arch"], "arm")
	db.update("hosts", ["name=?", 'ub15'], ["arch"], "arm")
	arch = db.nash(:hosts)["ub15"].arch
	puts arch
end

bb
f5
puts "fin"
