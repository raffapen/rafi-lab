
require 'Bento'
require 'minitest/autorun' 

#----------------------------------------------------------------------------------------------

class VMSettingsFile
	class << self
		def special(re)
			@@specials ||= []
			@@specials << Regexp.new("^" + re.source + "(\..*|$)")
		end
	end

	#------------------------------------------------------------------------------------------

	class Reader < Bento::Value
		def initialize(settings, m = nil)
			@settings = settings
		end

		def [](x)
			v = @settings[x]
			v.hash? ? Reader.new(v) : v
		end

		def method_missing(m, *args)
			v = @settings[m.to_s]
			v.hash? ? Reader.new(v) : v
		end

		def value
			@settings.fetch(:value, nil)
		end

		def self.coerce_value(x)
			{:value => x}
		end
		
		def entry
			@settings.fetch(:entry, nil)
		end

		def settings
			@settings
		end

		def indexes
			keys.select(&:int?)
		end

		def keys
			@settings.keys
		end

		def each
			keys.each { |k| yield(self[k], k) }
		end

		def each_index
			indexes.each { |k| yield(self[k]) }
		end
	end

	#------------------------------------------------------------------------------------------

	def method_missing(m, *args)
		v = @settings[m.to_s]
		v.hash? ? Reader.new(v) : v
	end

	def settings
		Reader.new(@settings).settings
	end

	def load_file(file)
		raw = Bento.fread(file).lines.map {|x| x.match /(.*) = "(.*)"/; [$1,$2] }
		raw.reject!{|x| x.first == ".encoding"}

		@settings = Bento::Hoh.new
		raw.each do |entry|
			key = entry.first
			fields = key.split(".")
			val = entry[1]
			@@specials.each do |sp|
				m = key.scan(sp)
				next if m.size == 0
				fields = m.first[0..-2] + m.first.last.split(".").drop_while(&:empty?)
				break
			end
			a = fields.map {|x| Integer(x) rescue x}
			
			# a += [val].map {|x| Integer(x) rescue x}.map {|x| x.string? ? ((y = x.to_bool) == nil ? x : y) : x}
			v = [val].map {|x| Integer(x) rescue x}.map {|x| x.string? ? ((y = x.to_bool) == nil ? x : y) : x}.first
			a += [{:value => v, :entry => entry.first}]
			
			h = a.reverse.reduce {|s, x| {x => s}}
			@settings << h
		end
	end

	def pathname
		@file
	end

	def to_s
		@file.to_s
	end
end

#----------------------------------------------------------------------------------------------

class VMXFile < VMSettingsFile
	special /(ethernet)(\d+)/
	special /(floppy)(\d+)/
	special /(hpet)(\d+)/
	special /(ide)(\d+:\d+)/
	special /(pciBridge)(\d+)/
	special /(scsi)(\d+:\d+)/
	special /(scsi)(\d+)/
	special /(serial)(\d+)/
	special /(sharedFolder)(\d+)/
	special /(usb)\:(\d+)/
	special /(vmci)(\d+)/

	def initialize(file)
		load_file(file)
	end
end

#----------------------------------------------------------------------------------------------

class VMSDFile < VMSettingsFile
	special /(snapshot)\.(mru)(\d+)/
	special /(snapshot)(\d+)\.(disk)(\d+)/
	special /(snapshot)(\d+)/

	def initialize(file)
		load_file(file)
	end
end

#----------------------------------------------------------------------------------------------

class Snapshot
	attr_reader :name

	def initialize(snap)
		@snap = snap
	end

	def name
		@snap.displayName
	end

	def time
		high = @snap.createTimeHigh
		low = @snap.createTimeLow
		low = 2 ** 32 + low if low < 0
		Time.at((high * 2**32 + low) / 10 ** 6)
	end

	def description
		@snap.description
	end
end

#----------------------------------------------------------------------------------------------

class Snapshots
	include Enumerable

	def initialize(vmsd)
		@vmsd = vmsd
	end

	def latest
		v = sort {|a,b| a.time <=> b.time }
		v.last
	end

	def each
		return if @vmsd.snapshot == nil
		@vmsd.snapshot.each_index { |snap| yield Snapshot.new(snap) }
	end
end

#----------------------------------------------------------------------------------------------

class T1 < Minitest::Test

	def test_1
		bb
		vmx = VMXFile.new('x/vmx1')
		assert_equal "~win7sp1-ent-x64", vmx.displayName
		assert_equal 2, vmx.cpuid.coresPerSocket
		assert_equal true, vmx.pciBridge[0].present.value
		assert_equal "hid", vmx.usb[0].deviceType
	end

	def test_2
		bb
		vmsd = VMSDFile.new("x/vmsd1")
		assert_equal Time.new(2012, 06, 06, 12, 05, 21, "+03:00"), Snapshot.new(vmsd.snapshot[0]).time
		assert_equal Time.new(2013, 06, 17, 21, 31, 58, "+03:00"), Snapshot.new(vmsd.snapshot[1]).time
		assert_equal Time.new(2014, 06, 12, 17, 52, 30, "+03:00"), Snapshot.new(vmsd.snapshot[2]).time

		assert_equal 1, vmsd.snapshot[0].uid
		assert_equal "vt-win7sp1-x64-Snapshot1.vmsn", vmsd.snapshot[0].filename
		
		disks = {0 => { :fileName => "vt-win7sp1-x64.vmdk", :node => "scsi0:0" }}
		vmsd.snapshot[0].disk.each do |disk, id|
			assert_equal disks[id][:fileName], disk.fileName
			assert_equal disks[id][:node], disk.node
		end
	end
end
