
require 'Bento'

# [1,2,3,4].permutation { |x| puts x.join("-") }

def foo(a, b = [])
	if a == []
		puts b.join("-")
	else
		for i in 0..(a.size-1)
			x = a[i]
			a1 = a.dup
			a1.delete_at(i)
			foo(a1, b + [x])
		end
	end
end

bb
foo([1, 2, 3, 4])
