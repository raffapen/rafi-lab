
require 'Bento'

class Foo
	include Bento::Class
	
	def that(*opt)
		jojo, vivi = nil, nil
		flags binding, locals: [:jojo, :vivi]
		
		puts jojo
		puts vivi
	end
	
	def test1
		jojo = nil
		test2 binding
		puts jojo
	end
	
	def test2(binding_)
		eval("jojo=nil", binding_)
		binding_.local_variable_set(:jojo, 17)
	end
		
end

foo = Foo.new
bb
foo.test1
foo.that :jojo
foo.that :vivi
