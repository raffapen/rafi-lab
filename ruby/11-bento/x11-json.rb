
require 'Bento'
require 'json'
require 'yaml'
require 'pp'

js = <<END
{
	"rafie-linux1" : { "hostname": "172.16.82.142", "credentials" : { "username": "root", "passwd": "taligent" }, "os": "linux", "arch": "x86", "tags": [] },
	"rafie-dora" : { "hostname": "172.16.82.101", "credentials" : { "username": "root", "passwd": "RvShos" }, "os": "linux", "arch": "ppc", "tags": ["dora"] }
}
END

js1 = <<END
(
	(rafie-linux1 :hostname 172.16.82.142 :username root :passwd taligent :os linux :arch x86 (tags))
	(rafie-dora :hostname 172.16.88.101 :username root :passwd RvShos :os linux :arch ppc (tags dora))
)
END

ya = <<END
rafie-linux1:
  hostname: 172.16.82.142
  credentials:
    username: root
    password: kernel
  ostype: linux
  arch: x86
  tags: [a, b]

rafie-dora:
  hostname: 172.16.82.101
  credentials:
    username: root
    password: RvShos
#  ostype: linux
  arch: ppc
  tags: [dora]
END

j = JSON.parse(js)
# pp j

y = YAML.load(ya)
# pp y

y1 = y["rafie-dora"]
# pp y1
# pp y["rafie-dora"]["credentials"]

puts JSON.pretty_generate(y)
puts YAML.dump(y)
# puts "fin"
