
require 'Bento'

class Smart1
	def foo(a, b = 17, *opt, name: "jojo")
		puts "impala"
		puts "#{a}: #{b}"
		puts "haha!" if opt.include? :haha
		puts name
	end
end

module Int1

	def foo(a, b = 10, *opt, name: "name")
		puts "#{a}: #{b}"
		puts "haha!" if opt.include? :haha
	end

	# define_method(:bar, instance_method(:foo))# do |*args| foo(*args); end
	smart_foo = Smart1.instance_method(:foo)
	define_method(:bar) do |*args|
		smart_foo.bind(smart).call(*args)
	end

end

class A
	include Int1
	
	def smart
		Smart1.new
	end
end

#----------------------------------------------------------------------------------------------

module Int2
	def foo(a, b, *opt); end
	def bar(a, b, c); end
end

module Interface
	
	def implement_interface(iface, smart_meth_sym)
		iface.instance_methods.each do |imeth_sym|
			define_method(imeth_sym) do |*args|
				smart = self.send(smart_meth_sym)
				smart.send(imeth_sym, *args)
			end
		end
	end

	def interfaces(*ifaces)
		ifaces.each do |iface|
			iface.instance_methods.each do |imeth_sym|
				raise "bla!" if iface.instance_method(imeth_sym).parameters != instance_method(imeth_sym).parameters
			end
		end
	end

end

class Impl1
	extend Interface
	
	def foo(a, b, *opt)
		puts "Impl1: #{a}: #{b}"
		puts "haha!" if opt.include? :haha
	end
	
	def bar(a, b, c)
		puts "Impl1: #{a + b + c}"
	end

	interfaces Int2
end

class Impl2
	extend Interface

	def foo(a, b, *opt)
		puts "Impl2: #{a}: #{b}"
		puts "ho!ho!ho!" if opt.include? :haha
	end
	
	def bar(a, b, c)
		puts "Impl2: #{a * b * c}"
	end

	interfaces Int2
end

class B
	extend Interface

	class << self
	
		define_method(:implement_interface1) do |iface, smart_meth_sym|
			iface.instance_methods.each do |imeth_sym|
				define_method(imeth_sym) do |*args|
					smart = self.send(smart_meth_sym)
					smart.send(imeth_sym, *args)
					# imeth = smart.class.instance_method(imeth_sym)
					# imeth.bind(smart).call(*args)
				end
			end
		end

	end

	implement_interface Int2, :smart
	
	# attr_reader :smart
	def smart
		@a == 1 ? @impl1 : @impl2
	end
	
	def a=(k)
		@a = k
	end
	
	def initialize
		@a = 1
		@impl1 = Impl1.new
		@impl2 = Impl2.new
		# @smart = Impl1.new
	end
		
end

#----------------------------------------------------------------------------------------------

class Impl3
	include Bento::Class
	
	def foo(a, b, *opt)
		puts "Impl1: #{a}: #{b}"
		puts "haha!" if opt.include? :haha
	end
	
	def bar(a, b, c)
		puts "Impl1: #{a + b + c}"
	end

	interfaces Int2
end

class Impl4
	include Bento::Class

	def foo(a, b, *opt)
		puts "Impl2: #{a}: #{b}"
		puts "ho!ho!ho!" if opt.include? :haha
	end
	
	def bar(a, b, c)
		puts "Impl2: #{a * b * c}"
	end

	interfaces Int2
end

class C
	include Bento::Class

	implement_interface Int2, :smart
	
	def smart
		@a == 1 ? @impl1 : @impl2
	end
	
	def a=(k)
		@a = k
	end
	
	def initialize
		@a = 1
		@impl1 = Impl3.new
		@impl2 = Impl4.new
	end

	def bar(a, b, c)
		puts "C: #{a} #{b} #{c}"
	end
end

#----------------------------------------------------------------------------------------------

bb
b = C.new
b.a = 1
b.foo(10, 20, :haha)
b.a = 2
b.foo(10, 20, :haha)
b.bar(2, 3, 4)

# a = A.new
# a.foo(10, 20)
# a.bar(10, 20)
# a.bar(10, 20, :haha)

puts "fin"
