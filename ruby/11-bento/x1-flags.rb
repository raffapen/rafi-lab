
require "Bento"

class Foo


	def flags(flagsyms, optsym, binding, nullables = [])
		opt = eval("#{optsym}", binding)
		nullables.each do |n|
			x = eval("#{n}", binding)
			if x.symbol?
				opt += [x]
				eval("#{n} = nil", binding)
			end
		end
		eval("#{optsym} = #{opt}", binding)

		flags = eval("#{flagsyms}", binding)
		flags.each do |f|
			if opt.include? f
				instance_variable_set("@#{f}", true)
				opt.delete f
			end
		end
		eval("#{optsym} = #{opt}", binding)
	end

	def foo(a, b, c = nil, d = nil, *opt, name: nil)
		flags [:f1, :f2], :opt, binding, [:c, :d]
		pp [a, b, c, d, opt, self]
	end
end

bb
Foo.new.foo("a", "b", "c", "d", :f1)
Foo.new.foo("a", "b", :f1, :f2)
Foo.new.foo("a", "b", "c", :f1)
Foo.new.foo("a", "b", "c", :f1, :f2)
Foo.new.foo("a", "b", :f3)
puts "fin"

