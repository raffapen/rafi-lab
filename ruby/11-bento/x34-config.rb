
require 'Bento'

class Config
	@@sets = %i(global host user)

	@@global_yaml = <<~END
	foo:
	  bar: 42
	END

	@@user_yaml = <<~END
	foo1:
	  bar: 31
	END

	@@host_yaml = <<~END
	foo:
	  bar: 17
	END

	def load_yaml(set)
		eval("@#{set} = YAML.load(@@#{set}_yaml)")
	end

	def initialize
		@@sets.each { |set| load_yaml(set) }
	end

	def read(name, h)
		keys = name.split('/')
		v = h.is_a(Hash)
		keys.each do |k|
			v = v[k] if v != nil
		end
		v
	end

	def write(name, val, h)
		keys = name.split('/')
		v = h.is_a(Hash)
		keys[0..-2].each do |k|
			v[k] = {} if v[k] == nil
			v = v[k]
		end
		v[keys.last] = val
	end
	
	def effective_sets(sets = nil)
		sets == nil || sets == [] ? @@sets : sets
	end
	
	def readx(name, sets: nil)
		v = {}
		# sets = sets.map(&:to_s)
		effective_sets(sets).each do |set|
			h = instance_variable_get("@#{set}")
			v1 = read(name, h)
			v[set] = v1 if v1
		end
		v
	end

	def readv(name, sets: nil)
		x = readx(name, sets: sets)
		case x.keys.size
		when 0; v = nil
		when 1; v = v.first
		else
			v = []
			effective_sets(sets).each do |set|
				v1 = x[set]
				v << v1 if v1
			end
		end
		v
	end

	def[](name, *opt)
		readv(name, sets: opt)
	end

	def[]=(name, *opt, val)
		if opt == []
			x = readx(name)
			if x.keys.size > 0
				set = nil
				effective_sets.reverse.each do |s|
					if x[s] != nil
						set = s 
						break
					end
				end
				h = eval("@#{set}")
				write(name, val, h)
			else
				h = eval("@#{effective_sets.last}")
				write(name, val, h)
			end
		else
			set = opt.first
			h = eval("@#{set}")
			write(name, val, h)
		end
		val
	end
end

begin
bb
cc = Config.new
# h = {"foo"=>{"bar"=>42}}
# cc.write("foo/bar", 17, h)
pp cc.readx("foo/bar")
pp x = cc["foo/bar"]
pp x = cc["foo/bar", :user]
pp x = cc["foo/bar", :global, :host]
pp x = cc["foo/bar", :host, :global]
cc["foo/bar"] = "glob"
# pp x = cc["foo/bar"]
pp cc.readx("foo/bar")
cc["foo/bar", :host] = "host"
# pp x = cc["foo/bar"]
pp cc.readx("foo/bar")
cc["foo/bar", :user] = "user"
# pp x = cc["foo/bar"]
pp cc.readx("foo/bar")
rescue => x
	bt(x)
end
puts
