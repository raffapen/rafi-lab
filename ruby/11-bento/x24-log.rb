require 'Bento'
require 'pp'

#----------------------------------------------------------------------------------------------

def colorize(text, color_code)
  "\e[#{color_code}m#{text}\e[0m"
end

def print_bt(x)
	print "\r" << (' ' * 50) << "\n"
	stacktrace = x.backtrace.map do |call|
		if parts = call.match(/^(?<file>.+):(?<line>\d+):in `(?<code>.*)'$/)
			file = parts[:file].sub /^#{Regexp.escape(File.join(Dir.getwd, ''))}/, ''
			line = "#{colorize(file, 36)} #{colorize('(', 37)}#{colorize(parts[:line], 32)}#{colorize('): ', 37)} #{colorize(parts[:code], 31)}"
		else
			line = colorize(call, 31)
		end
		line
	end
	puts "Stack trace:\n"
	stacktrace.each { |line| puts line }
end

#----------------------------------------------------------------------------------------------

module Bento
class Error  < RuntimeError
	attr_reader :errors

#	def initialize(*args)
#		super(*args)
#		# @errors ||= []
#		# @errors << args[0] if args.size > 0
#	end

	def chain
		ch = [self]
		c = cause
		while c != nil
			ch << c
			c = c.cause
		end
		ch
	end
end
end

#----------------------------------------------------------------------------------------------

class Jojo
	class Error < Bento::Error; end

	def bar
		error "Something bad happened"
	end

	def foo
		warn "This is the last warning!"
		bar
	end

	def initialize
		foo
	end
end

#----------------------------------------------------------------------------------------------

class Vivi
	class Error < Bento::Error; end

	def beh
		begin
			j = Jojo.new
		rescue => x
			bb
			error "cannot create Jojo. beh.", x
		end
	end

	def self.meh
		begin
			j = Jojo.new
		rescue => x
			bb
			error "cannot create Jojo. meh." # , x
		end
	end
end

#----------------------------------------------------------------------------------------------

def moo
	error "moo, man!"
end

#----------------------------------------------------------------------------------------------

Bento::Log.to = "jojo"

begin
	bb
	Vivi.new.beh
	# Vivi.meh
rescue => x
	puts "Caught #{x.class.name}"
	bb
	print_bt(x)
end

begin
	moo
rescue => x
	puts "Caught #{x.class.name}"
	bb
	print_bt(x)
end

fatal "I'm done here"
