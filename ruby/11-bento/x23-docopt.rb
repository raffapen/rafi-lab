# require "docopt"
require 'Bento'
require "pp"

doc = <<DOCOPT
Naval Fate.

Usage:
  #{__FILE__} ship new <name>...
  #{__FILE__} ship <name> move <x> <y> [--speed=<kn>]
  #{__FILE__} ship shoot <x> <y>
  #{__FILE__} mine (set|remove) <x> <y> [--moored|--drifting]
  #{__FILE__} -h | --help
  #{__FILE__} --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --speed=<kn>  Speed in knots [default: 10].
  --moored      Moored (anchored) mine.
  --drifting    Drifting mine.

DOCOPT

class Command < Bento::Docopt::Command

	def run!
		puts "Hello"
	end
end

c = Command.new(doc)
pp c.args
bb
exit(0)

class Command
	def initialize(*opt)
		@global_args = ::Docopt::docopt(*opt, options_first: true)
	end
	
	def run!
		pp @global_args
	end
end

Command.new(doc).run!
bb
exit(0)

begin
  pp ::Docopt::docopt(doc, options_first: true)
rescue ::Docopt::Exit => e
  bb
  puts "catch"
  puts e.message
end
