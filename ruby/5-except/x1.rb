
class Error < StandardError

	attr_reader :stack

	def initialize(msg, exception = nil, *opt, error: nil)
		super(msg)
		byebug
		bt = exception.backtrace if exception
		bt = error.backtrace if error
		@stack = error ? error.stack : []
		@stack << [msg, backtrace]
	end

	def story
		@stack.each { |x| puts x[0] + ":\n" + x[1] + "\n---" }
	end
end

class A
	def initialize
		a
	end
	
	def a
		b
		rescue Error => x
			raise Error.new("a failed", error: x)
	end
	
	def b
		c
		rescue Error => x
			raise Error.new("b failed", error: x)
		rescue => x
			raise Error.new("b failed", x)
	end
	
	def c
		raise Error.new("c failed")
	end
end


begin
	a = A.new
rescue Error => x
	puts x.story
end

