
class X

def init_members(bg, *args)
	args.each do |a|
		if a.is_a?(Symbol)
			x = eval(a.to_s, bg)
			bg.local_variable_set("@#{a}".to_sym, x)
		end
		# obj.instance_eval("@#{a.to_s} = #{a.to_s}") 
	end
end

def foo(a)
	init_members binding, :a
	puts @a
end

end

x = X.new
x.foo(10)
