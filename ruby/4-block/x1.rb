
class A

	@@errors = []

	def live_to_tell
		begin
			yield #if block_given?
		rescue => x
			@@errors << x
		end
	end

	def foo
		raise "shit!"
	end

	def test
		live_to_tell { foo }
	end
	
	def finish
		puts @@errors
	end
end

a = A.new
a.test
a.test
a.test
a.finish
puts "The End."
