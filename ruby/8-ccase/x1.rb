
require 'pp'

configspec = <<END
element * CHECKEDOUT

# element * .../USER_mcu_BRANCH_br/LATEST

# mkbranch USER_mcu_BRANCH_br
element /mcu/...   mcu_7.2.0.3.0
element /mvp/...   mvp_7.2.0.2.0
element /web/...   WebMcu_7.1_40_251009.43_dora-7.2_1.1

element /adapters/...        adapters_7.31.1_dora-7.2_3.0
element /dialingInfo/...     dialingInfo_7.27.1_dora-7.2_1.1
element /mediaCtrlInfo/...   mediaCtrlInfo_7.27.1_dora-7.2_1.0

element /NBU_COMMON_CORE/...      nbuCCore_3.1.7_dora-7.2_1.1
element /NBU_H323_STACK/...       nbuH323_3.7.9_dora-7.2_1.1
element /NBU_SIP_STACK/...        nbuSip_3.26.5_dora-7.2_1.2
element /NBU_SCCP_STACK/...       nbuSccp_1.1.10_dora-7.2_1.1
element /NBU_FEC/...              nbuFec_7.1.10_dora-7.2_1.1
element /NBU_RTP_RTCP_STACK/...   nbuRtpRtcp_3.1.0_dora-7.2_1.1

element /mpc/...       mpc_7.2.0.2.1
element /map/...       map_7.2.0.2.0
element /mf/...        mf_7.2.0.2.0
element /mpInfra/...   mpInfra_7.2.0.2.0

element /dpm/...            dpm_1.1.8_dora-7.2_1.0
element /dspInfra/...       dspInfra-Audio_7.1.0.75.0_dora-7.2_1.0
element /dspC64Audio/...    dspC64Audio_2.0.1.34_dora-7.2_1.0
element /dsp8144Audio/...   dsp8144Audio_07.01.07_dora-7.2_1.0
element /dsp8144Video/...   dsp8144Video_7.1.0.75.0_dora-7.2_1.0

element /swInfra/...       swInfra_0.92.1_dora-7.2_3.0
element /boardInfra/...    boardInfra_0.60.1_dora-7.2_3.0
element /configInfra/...   configInfra_1.1.6_dora-7.2_1.1
element /loggerInfra/...   loggerInfra_1.33.0_dora-7.2_1.1
element /rvfc/...          rvfc_2.85.4_dora-7.2_3.0

element /securityApp/...     securityApp_0.9.2_dora-7.2_1.1
element /securityInfra/...   securityInfra_2.2.27_dora-7.2_1.0

element /bsp8548/...        090915_BSP8548_2.1_0_4
element /bspLinux8548/...   091004_MAIO_0.1.18

element /freemasonBuild/...   freemasonBuild_1.2.0-rc10_dora-7.2

element * /main/0
# end mkbranch
END

def spec_to_cspec(spec)
	vobs = {}
	spec.lines.each do |line|
#		next if line.strip.empty?
#		next if line =~ /^#/
		next if ! (line =~ /^\s*element\s+(.*)/)
		a = $1.split(/\s+/)
		a[0] =~ /[\/\\]([^\/\\]+)[\/\\]\.\.\./
		vob = $1
		tag = a[1]
		
		next if !vob || !tag
		vobs[vob] = tag
	end
	vobs
end

pp spec_to_cspec(configspec)
