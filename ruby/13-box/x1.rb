
require_relative 'Box'

#----------------------------------------------------------------------------------------------

module App1

class Data < Lab::AppData

	@@app_name = "app1"
	
end

#----------------------------------------------------------------------------------------------

module Devito
class DataScheme
	def initialize(app_name)
		@app_name = app_name.to_s
	end

	def data_root
		p = ENV["#{app_name.upcase}_DATA"]
		return Pathname.new(p) if !!p 
		Pathname.new(ENV["DEVITO_SITE"])/"dat/app"/app_name
	end

	def host_root
		p = ENV["DEVITO_HROOT"]
		p = ENV["DEVITO_SITE"])/"net"/System.hostname if !p
		p = Pathname.new(p)/"app"/app_name
		
	end

	def user_root
		Pathname.new(ENV["DEVITO_UROOT"])/"app"/app_name
	end

	def hu_root
		Pathname.new(ENV["DEVITO_HUROOT"])/"app"/app_name
	end

	def local_root
		Pathname.new(ENV["DEVITO_LROOT"])/"app"/app_name
	end

	def lu_root
		Pathname.new(ENV["DEVITO_LUROOT"])/"app"/app_name
	end
end
end # module Devito

#----------------------------------------------------------------------------------------------

class Box < Lab::Box

	@@app_data_class = App1::Data

end

class Boxes < Lab::Boxes
end

end # module App1

#----------------------------------------------------------------------------------------------

bb
b1 = Lab.Box1('b1')
pust b1.db_name
puts "fin"
