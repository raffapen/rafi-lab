
require 'Bento'
require 'Bento/lib/Test'

module Lab

#----------------------------------------------------------------------------------------------

class Config

	@@box = nil
	@@pending_data_path = nil

	#------------------------------------------------------------------------------------------
	# Data
	#------------------------------------------------------------------------------------------

	# Data path determination process:
	# (i)   APPNAME_DATA env var
	# (ii)  BOX env var => Box(BOX)
	# (iii) box=`cat boxes/.box` => Box(box)
	# (iv)  new Box

	def Config.data_path
		return @@pending_data_path if @@pending_data_path

		dat1a = ENV["#{AppData.app_name}_DATA"]
		if !data
			if !@@box
				begin
					@@box = Confetti.Box()
				rescue
					@@box = Confetti::Box.create()
				end
				pedning_data_path = nil
			end
			data = @@box.data_path
			write_default_box
		end
		Pathname.new(data)
	end

	#------------------------------------------------------------------------------------------

	def Config.pending_data_path=(path)
		@@pending_data_path = path
	end

	#------------------------------------------------------------------------------------------

	def Config.host_data_path
		net = Config.data_path/"net"
		host = net/(inside_the_box? ? "host" : System.hostname)
		if Dir.exists?(net)
			FileUtils.cp_r(net/".host", host) if !Dir.exist?(host)
		end
		host
	end

	#------------------------------------------------------------------------------------------

	def Config.user_data_path
		users = Config.data_path/"usr"
		user = users/(inside_the_box? ? "user" : System.user)
		if Dir.exists?(users)
			FileUtils.cp_r(users/".user", user) if !Dir.exist?(user)
		end
		user
	end

	#------------------------------------------------------------------------------------------
	# DB
	#------------------------------------------------------------------------------------------

	def Config.db_path
		Config.data_path/"db/confetti.db"
	end

	def Config.db_log_path
		Config.data_path/"db/log"
	end

	#------------------------------------------------------------------------------------------
	# Logs
	#------------------------------------------------------------------------------------------

	def Config.user_log_path
		Config.user_data_path/"log"
	end

	def Config.host_log_path
		Config.host_data_path/"log"
	end

	#------------------------------------------------------------------------------------------
	# Tests
	#------------------------------------------------------------------------------------------

	def Config.test_source_path
		Config.confetti_root/"test"
	end

	#------------------------------------------------------------------------------------------
	# Boxes
	#------------------------------------------------------------------------------------------

	def Config.boxes_path
		Config.confetti_root/"boxes"
	end

	def Config.default_box_filename
		Config.boxes_path/".box"
	end

	def Config.inside_the_box?
		!ENV["CONFETTI_DATA"]
	end

	def Config.box
		return nil if !inside_the_box?
		if @@box == nil
			name = Config.box_name
			@@box = Lab.Box(name) if name
		end
		@@box
	end

	def Config.set_box(box)
		@@box = box
		write_default_box
	end

	def Config.box_name
		name = ENV["BOX"]
		return name if name
		fname = Config.default_box_filename
		name = File.read(fname) if File.exists?(fname)
		name
	end

	def Config.write_default_box
		ENV["BOX"] = @@box.id
		IO.write(Config.default_box_filename, @@box.id)
	end

	def self.remove_box(name)
		remove_default_box if name == Config.box_name
	end

	def self.remove_default_box
		ENV["BOX"] = nil
		File.delete(Config.default_box_filename) rescue ''
	end

end # Config

#----------------------------------------------------------------------------------------------

class Box
	include Bento::Class

	constructors :is, :create
	members :id, :root, :fs_source

	attr_reader :id, :root

	#------------------------------------------------------------------------------------------

	def is(id = nil, *opt)
		init_flags([], opt)

		if !id
			id = Config.box_name
			raise "Box: cannot determine test ID" if !id
		end

		@id = id
		@root = Box.root_path(@id)
		Config.pending_data_path = root

		json = JSON.parse(File.read(root/'box.json'))
	end

	#------------------------------------------------------------------------------------------

	# opt: :keep
	def create(*opt, source: 'box', make_fs: true)
		init_flags([:nopush], opt)

		@id = Box.make_id
		@root = Box.root_path(@id)
		Config.pending_data_path = root

		@views = Views.new

		raise "Box @id exists: aborting" if File.exists?(root)
		FileUtils.mkdir_p(root)

		@fs_source = Config.test_source_path/source/"fs"
		create_fs if make_fs

		create_db

		write_cfg

		Config.set_box(self)
		
		rescue => x
			error x.to_s
			puts x.backtrace.join("\n")
			# exception x, "Box: creation failed"
			remove!
			raise "Box: creation failed"
	end

	#------------------------------------------------------------------------------------------

	def name
		id
	end

	#------------------------------------------------------------------------------------------

	def self.root_path(id)
		Config.boxes_path/id
	end
	
	#------------------------------------------------------------------------------------------

	def data_path
		root
	end

	def db_name
		self.class.name.downcase
	end

	def db_path
		root/"db"/"#{db_name}.db"
	end

	def log_path
		root/"logs"
	end
		
	#------------------------------------------------------------------------------------------

	def self.make_id
		id = Time.now.strftime("%y%m%d-%H%M%S")
		while File.directory?(Config.boxes_path/id)
			id = Time.now.strftime("%y%m%d-%H%M%S%L")
		end
		id
	end

	#------------------------------------------------------------------------------------------

	def write_cfg
		File.write(@root/"box.json", JSON.generate({ :views => view_names }))
	end

	#------------------------------------------------------------------------------------------
	# Construction
	#------------------------------------------------------------------------------------------

	def create_fs
		# this is required to create empty directories, which git ignores
		zip = @fs_source.to_s + ".zip"
		Bento.unzip(zip, @root) if File.file?(zip)
		
		# copy content of fs_source, not fs_source itself
		FileUtils.cp_r(@fs_source.to_s + "/.", @root) if File.directory?(@fs_source)
		# FileUtils.cp_r(Dir.glob(@fs_source/"*"), @root) if File.directory?(@fs_source)
	end

	#------------------------------------------------------------------------------------------

	def create_db
		Confetti::Database.create
		script = Config.test_source_path/"db/data.sql"
		Confetti::Database.execute_script(script) if File.exists?(script)
	end

	#------------------------------------------------------------------------------------------

	def view_names
		@views == nil ? [] : @views.names
	end

	def add_view(view)
		@views << view
		write_cfg
	end

	def remove_view(view)
		@views.delete(view)
		write_cfg
	end

	def remove_view!(view)
		name = view.name
		view.remove!
		@views.delete(name)
		write_cfg
	end

	#------------------------------------------------------------------------------------------
	# Open/close
	#------------------------------------------------------------------------------------------

	def open
		@views.each do |v|
			v.start rescue ''
		end
	end

	def close
		@views.each do |v|
			v.stop rescue ''
		end
	end

	#------------------------------------------------------------------------------------------
	# Removal
	#------------------------------------------------------------------------------------------

	def remove!
		abort = false
		failed_objects = []

		while ! @views.empty? do
			view = @views.first
			begin
				view.remove!
				@views.shift
				write_cfg
			rescue
				failed_objects << "view #{view}"
				abort = true
			end
		end
		raise "box #{id} was not removed: #{failed_objects}" if abort

		write_cfg

		begin
			Database.close
		rescue => x
			bb
			puts x.to_s
		end
		
		begin
			FileUtils.rm_r(@root)
		rescue => x
			bb
			failed_objects << "directory #{@root}"
		end

		Config.remove_box(id)
	end

end # class Box

#----------------------------------------------------------------------------------------------

class Box1 < Box
	include Bento::Class
	constructors :is, :create
end

#----------------------------------------------------------------------------------------------

class Boxes
	include Enumerable

	def initialize
		@names = Dir[Config.boxes_path/'*'].select {|f| File.directory?(f)}.map {|f| File.basename(f)}.sort
	end

	def each
		@names.each { |name| yield Confetti.Box(name) }
	end

	def print
		default_box = Config.box
		default_name = default_box ? default_box.name : ""
		each do |box|
			name = box.name
			puts name + (default_name == name ? " *" : "")
		end
	end
	
	def self.print
		Boxes.new.print
	end

end # class Boxes

#----------------------------------------------------------------------------------------------

end # module Lab
