@echo off

setlocal
set a=
set x=
for %%i in (%*) do call :yyy %%i
endlocal & set __args=%a%
call :rest %__args%
exit /b

:rest
echo %*
exit /b

:shift

:yyy
if not "%x%" == "" set a=%a% %1
set x=1
exit /b
