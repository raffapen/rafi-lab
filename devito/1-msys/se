#!/bin/bash

. devito-setenv classico-perl
perl << __END__

use strict;
use Win32::Console;
use File::Basename;
use File::Find::Rule;
use Cwd;
use Scaffolds;
# use Log::Log4perl qw(:easy);

sub terminate
{
	exit(1);
}

$SIG{INT} = 'terminate';
$SIG{QUIT} = 'terminate';
$SIG{ABRT} = 'terminate';
$SIG{TERM} = 'terminate';

our $root = $ENV{DEVITO_CORE} . "/scripts/env";

# Log::Log4perl::init($ENV{DEVITO_CORE} . "/scripts/bin/setenv.log.conf");
# Log::Log4perl::MDC->put("user", $ENV{username});
# my $logger = get_logger("setenv");
# $logger->info("$0 @ARGV");

my $arg = shift;
my $env;
my $envpath;

if ($arg eq "" || $arg eq "-h" || $arg eq "--help")
{
	help();
	exit();
}
elsif ($arg eq "-e")
{
	$envpath = shift;
}
else
{
	$env = $arg;
	$envpath = "." if $env eq ".";
}

my $args = join(' ', @ARGV);

$envpath = "$root/$env" if !$envpath;
die "Error: invalid environment '$env'\n" if ! setenv($envpath, $args);
exit(0);

sub setenv
{
	my ($envpath, $args) = @_;

	my $title;

	my $envfile;
	if (-f "$envpath/start.bat")
	{
		$envfile = "start.bat";
	}
	elsif (-f "$envpath/env.bat")
	{
		$envfile = "env.bat";
	}
	else
	{
		return 0;
	}
	
	if (open(TITLE, "<$envpath/title"))
	{
		my @title = <TITLE>;
		close TITLE;
		$title = $title[0];
	}
	else
	{
		open(ENVFILE, "<$envpath/$envfile");
		while (<ENVFILE>)
		{
			next if $_ !~ "^:title(.*)";
			$title = $1;
			last;
		}
		close(ENVFILE);
		
#		print "envfile: $envfile\n";
#		print "title: $title\n";
		$title = "[" . basename(cwd()) . "]" if !$title;
	}
	
	my $c = Win32::Console->new(STD_OUTPUT_HANDLE);
	my $t = $ENV{ENVSTR};
	my $t0 = $t;
	if ($t eq "")
	{
		$t = $title;
	}
	else
	{
		$t .= " : $title";
	}
	$c->Title($t);
	$ENV{ENVSTR} = $t;

	my $rc;
	if ($ENV{DEVITO_USE_TCC})
	{
		# exit system("$ENV{TakeCommand_command} /k $envpath\\$envfile $args");
		# $rc = system("call tcc.bat /k $envpath\\$envfile $args");
#		print "1: $ENV{TakeCommand_command} /k $envpath\\$envfile $args\n";
		$rc = system("$ENV{TakeCommand_command} /k $envpath\\$envfile $args");
	}
	else
	{
		my $cmd = "cmd /k \"" . u2w("$envpath/$envfile");
		$cmd .= " $args" if $args ne "";
		$cmd .=  "\"";
#		print "2: $cmd\n";
		$rc = system($cmd);
	}

	$c->Title($t0);
	exit($rc);
}

sub help
{
	my %all;
	my $maxlen = 0;
	my @dirs = File::Find::Rule->directory()->mindepth(1)->maxdepth(1)->in($root);
	foreach (sort @dirs)
	{
		if (-d $_)
		{
			my $env = basename($_);
			$all{$env} = title($env);
			my $n = length($env);
			$maxlen = $n if $n > $maxlen;
#			print $env . "\t" . title($env) . "\n";
		}
	}

	print <<END;
Start a new shell session with the selected environment.

Usage:
  se <environment> <args>   Invoke <environment>
  se . <args>               Pick up environment in working dir
  se -e <dir> <args>        Pick up environment (env.bat file) in <dir>
  se -h [<environment>]     Explain <environment> or print this screen

(See also: inenv)

Available environments:

END

	foreach my $env (sort keys %all)
	{
		my $n = length($env);
		print $env . " " x ($maxlen - $n + 2) . $all{$env} . "\n";
	}
}

sub title
{
	my $env = shift;
	my $envpath = "$root/$env";
	if (open(TITLE, "<$envpath/title")) # or die "$env: invalid environment.\n";
	{
		my @title = <TITLE>;
		close TITLE;
		return trim($title[0]);
	}
	else
	{
		return $env;
	}	
}
__END__
