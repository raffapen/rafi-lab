@echo off

setlocal

if not exist v:\ net use v: \\devito1\devito\vue
if not exist x:\ net use x: \\devito1\devito\vue\devito1\root

set DEVITO_XROOT=x:
%DEVITO_XROOT%\os\windows\sys\msys2\usr\bin\mintty -i /msys2.ico -t "Devito" /usr/bin/bash -c /x/core/windows/scripts/bin/devito-setenv --login
