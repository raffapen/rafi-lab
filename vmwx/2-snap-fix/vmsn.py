
import struct
import os
import sys
import io
import operator
from array import array
from functools import reduce

import paella

#==============================================================================================

HEADER_SIZE = 12
GROUP_SIZE = 80
GROUP_NAME_SIZE = 64

#==============================================================================================
		
class MetaTag():

	def __init__(self, tag, indices):
		self.group = tag.group
		self.file = tag.file
		self.tag = tag
		self.name = tag.name
		self.indices = indices
		self.meta = True
	
	#------------------------------------------------------------------------------------------

	def __getitem__(self, tag_index):
		tag = self.group.find_tag(self.name, *(self.indices + (tag_index,)))
		if not tag:
			raise KeyError("%s: tag not found. identifier should be tag data index" % (tag_index))
		return tag # either Tag or MetaTag

	#------------------------------------------------------------------------------------------

	def __setitem__(self, tag_id, value):
		raise NotImplementedError("Currently doesn't support write operations", tag_id, value)

	#------------------------------------------------------------------------------------------

	def __contains__(self, tag_index):
		return not (self.group.find_tag(*(self.indices + (tag_index,))) == None)

	#------------------------------------------------------------------------------------------

	def __str__(self):
		return str(self.group.name) + "." + self.name

#==============================================================================================

class Tag():
	def __init__(self, group, offset):
		self.group = group
		self.offset = offset

		f = self.file = group.file

		f.seek(offset)
		self.flags = f.read_byte() # +1
		name_size = f.read_byte() # +1
		
		self.empty = self.flags == 0 and name_size == 0
		
		self.indices = []
		
		if self.empty:
			self.name = ""
			self.data_size = 0
			self.data_offset = 0
			self.header_size = f.tell() - self.offset
			return

		self.name = f.read(name_size).decode() # +name_size
		
		indices_depth = (self.flags >> 6) & 0x03
		for _ in range(0, indices_depth):
			k = f.read_long() # + depth*4
			self.indices.append(k)

		x = self.flags & 0x3f
		if x == 0x3e or x == 0x3f:
			self.compressed = (x == 0x3f)
			if not self.compressed:
				print("NOT COMPRESSED!")
			
			self.data_size = f.read_offset() # +off
			self.data_mem_size = f.read_offset() # +off
			
			## read unknown word. seems to always be 0x0000, perhaps structure padding?
			pad1 = f.read(2) # +2
		else:
			self.data_mem_size = self.data_size = x
			self.compressed = False
		
# 		data = f.read(self.data_size)

		self.data_offset = f.tell()
		self.header_size = self.data_offset - self.offset
		self.size = self.header_size + self.data_size
		
		self.alt_data = None

	#------------------------------------------------------------------------------------------

	def read(self, offset = 0, size = -1):
		if size == -1:
			size = self.data_size - offset
		return self.file.reada(self.data_offset + offset, size)
 
	def read_offset(self):
		self.assert_read(self.file.offset_size)
		return self.file.reada_offset(self.data_offset)
	
	def read_long_long(self):
		self.assert_read(8)
		return self.file.reada_long_long(self.data_offset)
		
	def read_long(self):
		self.assert_read(4)
		return self.file.reada_long(self.data_offset)
		
	def read_byte(self):
		self.assert_read(1)
		return self.file.reada_byte(self.data_offset)

	def assrert_read(self, size):
		if self.data_size < size:
			raise TypeError("Attempt to read tag at {offset} with size {tag_size} as {read_size}".
				format(offset=self.data_offset, tag_size=self.data_size, read_size=size))

	#------------------------------------------------------------------------------------------

#	def write(self, offset = 0, str):
#		if size == -1:
#			size = self.data_size - offset
#		self.file.reada(self.data_offset + offset, size)

	def write(self, data):
		n = self.data_size
		if n > 0 and n > len(data):
			data += b'\x00' * (n - len(data))
		self.alt_data = data
		return self.group.set_tag(self)

	def write_offset(self, n):
		self.file.writea_offset(self.data_offset, n)
	
	def write_long_long(self, n):
		self.file.writea_long_long(self.data_offset, n)
		
	def write_long(self, n):
		self.file.writea_long(self.data_offset, n)
		
	def write_byte(self, n):
		self.file.writea_byte(self.data_offset , n)

	def write_alt_header(self, f, offset):
		f.seek(offset)
		if self.empty:
			f.write_byte(0)
			f.write_byte(0)
			return 2

		f.write_byte(self.flags) # (self.flags & ~0x3f) | 0x3e)
		f.write_byte(len(self.name))
		f.write(self.name.encode())
		for x in self.indices:
			f.write_long(x)

		data_size = len(self.alt_data)

		if self.flags & 0x3f == 0x3f or data_size >= 0x3e :
			f.write_offset(data_size) # data_size
			f.write_offset(data_size) # data_mem_size
			f.write_byte(0)
			f.write_byte(0)
		
		return f.tell() - offset
	
	#------------------------------------------------------------------------------------------

	def __str__(self):
		return str(self.parent) + self.name

	#------------------------------------------------------------------------------------------

	def dump(self):
		fname = self.group.snap.fname + "." + self.group.name + "." + self.name
		with open(fname, "wb") as fh:
			t = self.read()
			fh.write(t)
		return fname
		
	def tdump(self):
		fname = self.group.snap.fname + "." + self.group.name + "." + self.name
		with io.open(fname, "w", newline='\n', encoding=None) as fh:
			t = self.read()
			fh.write(str(t.decode().replace("\x00", "")))
		return fname

#==============================================================================================

class Group(object):
	def __init__(self, snap, index):
		self.snap = snap
		f = self.file = snap.file
		self.index = index
		self.offset = HEADER_SIZE + index * GROUP_SIZE
		self.name = self._read_name()
		self.tags_offset = f.reada_long_long(self.offset + GROUP_NAME_SIZE)
		
		off = self.tags_offset
		tag = Tag(self, off)
		tag0 = tag1 = None
		while not tag.empty:
			if tag0 is None:
				tag0 = tag
			tag1 = tag
			off += tag.size
			tag = Tag(self, off)

		if tag0 is None:
			self.first_byte_offset = 0
		else:
			self.first_byte_offset = tag0.offset
		if tag1 is None:
			self.last_byte_offset = 0
		else:
			self.last_byte_offset = tag1.offset + tag1.size - 1

		self.pad = 0
		self.alt_tags = []
		pass

	@classmethod
	def from_group(cls, snap, src_group, index, tags_offset):
		self = cls.__new__(cls)

		src_f = src_group.file
		
		self.snap = snap
		f = self.file = snap.file
		self.index = index
		self.offset = HEADER_SIZE + index * GROUP_SIZE

		self.name = src_group.name
		
		self.tags_offset = tags_offset
		
		self.pad = src_group.pad
		self.alt_tags = []

		new_tags = sorted(src_group.alt_tags, key = lambda tag: tag.offset)
		r0 = src_group.tags_offset
		w = tags_offset
		for src_tag in new_tags:
			r1 = src_tag.offset
			n = r1 - r0
			if n > 0:
				f.writea(w, src_f.reada(r0, n))
				w += n
			
			w += src_tag.write_alt_header(f, w)
			
			n = len(src_tag.alt_data)
			f.writea(w, src_tag.alt_data)
			w += n
			
			r0 = r1 + src_tag.size

		n = src_group.last_byte_offset - r0 + 1 + src_group.pad
		f.writea(w, src_f.reada(r0, n))
		w += n

		self.first_byte_offset = tags_offset
		self.last_byte_offset = w - 1 - self.pad

		return self

	#------------------------------------------------------------------------------------------

	def write_header(self, snap):
		pass

	def write_tags(self, snap):
		for tag in self.tags():
			tag.write(self)
		
	#------------------------------------------------------------------------------------------

	def tags(self):
		off = self.tags_offset
		tag = Tag(self, off)
		while not tag.empty:
			yield tag
			off += tag.size
			tag = Tag(self, off)

	#------------------------------------------------------------------------------------------

	def tag_names(self):
		return [t.name for t in self.tags()]

	def tags_size(self):
		n = 0
		for t in self.tags():
			n += t.size
		return n + self.pad
		
	#------------------------------------------------------------------------------------------

	def find_tag(self, name, *indices):
		off = self.tags_offset
		tag = Tag(self, off)
		while not tag.empty:
			if tag.name == name:
				if tuple(tag.indices) == indices:
					return tag
				if tuple(tag.indices[0:len(indices)]) == indices:
					return MetaTag(tag, indices)

			off += tag.size
			tag = Tag(self, off)

		return None

	#------------------------------------------------------------------------------------------

	def _read_name(self):
		f = self.file
		f.seek(self.offset)
		name = ""
		while True:
			c = f.read(1)
			if c == b'\x00':
				break
			name += c.decode()
		return name
	
	def size(self):
		return GROUP_SIZE + self.tags_size()
		
	#------------------------------------------------------------------------------------------

	def report(self, group_name, so_far):
		tags_size = 0
		tags_n = 0
		post = 0
		at = -1
		for tag in self.tags():
			if at == -1:
				at = tag.offset
			tags_n += 1
			tags_size += tag.size
			post = tag.offset + tag.size

		print(("group %s(%s): top=%s size=%s first=%s last=%s pad=%s" % (self.name, self.index, self.offset, 
			GROUP_SIZE + tags_size + 2, self.first_byte_offset, self.last_byte_offset, self.pad)))
				
		if self.name == group_name:
			for tag in self.tags():
				print(("\tname=%s top=%s header=%s data=%s data_at=%s size=%s idx=%s %s" % 
					(tag.name, tag.offset, tag.header_size, tag.data_size, tag.data_offset, 
					tag.size, str(tag.indices), ("C=" + str(tag.data_mem_size) if tag.compressed else ""))))

		return GROUP_SIZE + tags_size + self.pad
	
	#------------------------------------------------------------------------------------------

	def set_tag(self, tag):
		self.alt_tags.append(tag)
		if not self.snap.in_session:
			return self.snap.rewrite()
		return None

	#------------------------------------------------------------------------------------------

	def __str__(self):
		return self.name
	
	#------------------------------------------------------------------------------------------

	def __getitem__(self, tag_id, *tag_indices):
		tag = self.find_tag(tag_id, *tag_indices)
		if not tag:
			raise AttributeError("%s: tag %s not found. identifier should be tag name" % (self.name, tag_id))
		return tag # either Tag or MetaTag

	#------------------------------------------------------------------------------------------

	def __contains__(self, tag_id):
		return self.find_tag(tag_id) is not None
	
	#------------------------------------------------------------------------------------------

	def __setitem__(self, tag_id, value):
		raise NotImplementedError("Currently doesn't support write operations", tag_id, value)

	#------------------------------------------------------------------------------------------
	
	def __enter__(self):
		self.snap.in_session = True
		return self
		
	def __exit__(self, exc_type, exc_value, traceback):
		self.snap.in_session = False
		pass

#==============================================================================================

class Snapshot(object):
	
	def __init__(self, fname):
		self.fname = fname
		self.check_pads = False

		f = self.file = File(fname, 'rb')

		self.magic = f.reada_long_long(0)
		
		if self.magic == 0x8bed2bed0:
			self.version = 0
		elif self.magic == 0x8bad1bad1:
			self.version = 1
		elif self.magic == 0x8bed2bed2:
			self.version = 2
		elif self.magic == 0x8bed3bed3:
			self.version = 3
		else:
			raise Exception("Header signature invalid", magic)

		# this is used whenever the vmsn specifications use 4\8 byte ints dependant of version
		f.offset_size = 4 if self.version == 0 else 8
			
		self.group_count = f.reada_long(8)
		
		self._groups = {}
		for i in range(0, self.group_count):
			g = Group(self, i)
			self._groups[i] = g
			self._groups[g.name] = g

		self.init_pads()
		self.in_session = False

	def init_pads(self):
		f = self.file

		g1 = None
		for i in range(0, self.group_count):
			g = self._groups[i]
#			print("group %s(%s): (%s, %s)" % (g.name, i, g.first_byte_offset, g.last_byte_offset))
			if g1 is not None and g.first_byte_offset != 0:
				b0 = g1.last_byte_offset + 1
				b1 = g.first_byte_offset
				if b1 - b0 > 0:
					g1.pad = b1 - b0
					if self.check_pads:
						buf = f.reada(b0, b1-b0)
						sum = reduce(lambda s, n: s + ord(n), buf, 0)
						if sum > 0:
							count = reduce(lambda s, n: s + (0 if ord(n) == 0 else 1), buf, 0)
							raise Exception("group %s(%s): nonzero pad in (%s, %s): %s, %s times" % (g1.name, i-1, b0, b1, sum, count))
			if g.first_byte_offset != 0:
				g1 = g

		f.seek(0, os.SEEK_END)
		b0 = g1.last_byte_offset + 1
		b1 = f.tell()
		g1.pad = b1 - b0
	
	#------------------------------------------------------------------------------------------

	@classmethod
	def create(cls, fname, src_snap, group_names):
		self = cls.__new__(cls)
		
		self.fname = fname
		self.check_pads = False
		
		self.magic = src_snap.magic
		self.version = src_snap.version
		
		self.group_count = len(group_names)
		self._groups = {}

		f = self.file = File(fname, 'w+b', self.version)
		
		self.write_header()
		self.write_groups(src_snap, group_names)
		f.reopen('rb')
		
		self.in_session = False
		return self

	#------------------------------------------------------------------------------------------

	@classmethod
	def create_with_alt_tags(cls, src_snap, fname = ""):
		self = cls.__new__(cls)
		
		self.fname = fname if fname != "" else src_snap.fname + ".x" 
		self.check_pads = False
		
		self.magic = src_snap.magic
		self.version = src_snap.version
		
		self.group_count = src_snap.group_count
		self._groups = {}

		f = self.file = File(self.fname, 'w+b', self.version)
		
		self.write_header()
		self.write_groups_with_alt_tags(src_snap)
		f.reopen('rb')
		
		# src_snap.file.close()
		
		self.in_session = False
		return self

	#------------------------------------------------------------------------------------------

	def write_header(self):
		f = self.file
		f.seek(0)
		f.write_long_long(self.magic)
		f.write_long_long(self.group_count)

	#------------------------------------------------------------------------------------------

	def write_groups(self, src_snap, group_names):
		src_f = src_snap.file
		f = self.file
		tags_offset = HEADER_SIZE + GROUP_SIZE * self.group_count

		i = 0
		for name in group_names:
			src_group = src_snap[name]
			
			group_header = src_f.reada(HEADER_SIZE + src_group.index * GROUP_SIZE, GROUP_SIZE)
			f.writea(HEADER_SIZE + i * GROUP_SIZE, group_header)
			f.writea_offset(HEADER_SIZE + i * GROUP_SIZE + GROUP_NAME_SIZE, tags_offset)
			
			group = Group.from_group(self, src_group, i, tags_offset)
			self._groups[i] = group
			self._groups[name] = group
			
			tags_offset += src_group.tags_size()
			i += 1

	#------------------------------------------------------------------------------------------

	def write_groups_with_alt_tags(self, src_snap):
		src_f = src_snap.file
		f = self.file
		tags_offset = HEADER_SIZE + GROUP_SIZE * self.group_count

		for i in range(0, src_snap.group_count):
			src_group = src_snap[i]
			name = src_group.name
			
			group_header = src_f.reada(HEADER_SIZE + src_group.index * GROUP_SIZE, GROUP_SIZE)
			f.writea(HEADER_SIZE + i * GROUP_SIZE, group_header)
			f.writea_offset(HEADER_SIZE + i * GROUP_SIZE + GROUP_NAME_SIZE, tags_offset)
			
			group = Group.from_group(self, src_group, i, tags_offset)
			self._groups[i] = group
			self._groups[name] = group
			
			tags_offset += src_group.tags_size()

	def rewrite(self, fname = ""):
		return Snapshot.create_with_alt_tags(self, fname)

	#------------------------------------------------------------------------------------------

	def groups(self):
		for i in range(0, self.group_count):
			yield self._groups[i]

	def group_names(self):
		return [t.name for t in self.groups()]

	def find_group(self, spec):
		return self._groups[spec] if spec in self._groups else None

	#------------------------------------------------------------------------------------------
	
	def size(self):
		return reduce(lambda s, g: s + g.size(), self.groups(), HEADER_SIZE)

	#------------------------------------------------------------------------------------------

	def report(self, group_name):
		print(("groups: %s, tags start at %s" % (self.group_count, HEADER_SIZE + self.group_count * GROUP_SIZE)))
		size = HEADER_SIZE
		for group in self.groups():
			size += group.report(group_name, size)
		return size
	
	#------------------------------------------------------------------------------------------

	def __getitem__(self, group_id):
		group = self.find_group(group_id)
		if not group:
			raise KeyError("%s: group not found. identifier could be either group index or name" % (group_id))
		return group

	#------------------------------------------------------------------------------------------

	def __contains__(self, group_id):
		return self.find_group(group_id) == None
	
	#------------------------------------------------------------------------------------------

	def __setitem__(self, group_id):
		raise NotImplementedError("Currently doesn't support write operations,1")

	#------------------------------------------------------------------------------------------
	
	def __enter__(self):
		self.in_session = True
		return self
		
	def __exit__(self, exc_type, exc_value, traceback):
		self.in_session = False
		pass
	
#==============================================================================================

class File():
	def __init__(self, fname, mode, version = 2):
		self.fname = fname
		self.fh = open(fname, mode)

		if not "b" in self.fh.mode.lower():
			self.fh.close()
			raise Exception("Invalid file handler: file must be opened in binary mode (and not %s)" % (fh.mode))

		self.offset_size = 4 if version == 0 else 8

#		self._mem = array('c', ' ' * 4000000)
		pass

	#------------------------------------------------------------------------------------------

	def flush(self):
		self.fh.flush()

	def close(self):
		self.fh.close()

	def reopen(self, mode):
		self.close()
		self.fh = open(self.fname, mode)

	#------------------------------------------------------------------------------------------

	def _memset(self, size):
#		self._memseta(self.tell(), size)
		pass

	def _memseta(self, addr, size):
#		if size == 1:
#			self._mem[addr] = '*'
#		else:
#			self._mem[addr:addr+size-1] = array('c','*' * size)
		pass

	def _memwrite(fname):
		h = open(fname,'a')
		self._mem.tofile(h)
		h.close()

	#------------------------------------------------------------------------------------------

	def seek(self, addr, curr = os.SEEK_SET):
		return self.fh.seek(addr, curr)
	
	#------------------------------------------------------------------------------------------

	def tell(self):
		return self.fh.tell()

	def offset_fmt(self):
		if self.offset_size == 4:
			return '=I'
		elif self.offset_size == 8:
			return '=Q'
		else:
			raise Exception('cannot determine offset size')

	#------------------------------------------------------------------------------------------

	def read(self, size):
		self._memset(size)
		return self.fh.read(size)

	def write(self, str):
		self.fh.write(str)
		
	#------------------------------------------------------------------------------------------

	def reada(self, addr, size):
		self._memseta(addr, size)
		
		curr = self.tell()
		self.seek(addr)
		data = self.read(size)
		self.seek(curr)
		return data
	
	def writea(self, addr, str):
		curr = self.tell()
		self.seek(addr)
		self.write(str)
		self.seek(curr)
	
	#------------------------------------------------------------------------------------------

	def read_offset(self):
		self._memset(self.offset_size)
		
		s = self.read(self.offset_size)
		(n,) = struct.unpack(self.offset_fmt(), s)
		return n
	
	def write_offset(self, n):
		s = struct.pack(self.offset_fmt(), n)
		self.write(s)
	
	#------------------------------------------------------------------------------------------

	def read_long_long(self):
		self._memset(8)
		(n,) = struct.unpack('=Q', self.read(8))
		return n
		
	def write_long_long(self, n):
		self.write(struct.pack('=Q', n))

	#------------------------------------------------------------------------------------------

	def read_long(self):
		self._memset(4)
		(n,) = struct.unpack('=I', self.read(4))
		return n
	
	def write_long(self, n):
		self.write(struct.pack('=I', n))

	#------------------------------------------------------------------------------------------

	def read_byte(self):
		self._memset(1)
		(n,) = struct.unpack('=B', self.read(1))
		return n

	def write_byte(self, n):
		self.write(struct.pack('=B', n))

	#------------------------------------------------------------------------------------------
	# reada functions - read data in a specific address

	def reada_offset(self, addr):
		self._memseta(addr, self.offset_size)

		s = self.reada(addr, self.offset_size)
		(n,) = struct.unpack(self.offset_fmt(), s)
		return n

	def writea_offset(self, addr, n):
		s = struct.pack(self.offset_fmt(), n)
		self.writea(addr, s)

	#------------------------------------------------------------------------------------------

	def reada_long_long(self, addr):
		self._memseta(addr, 8)
		(n,) = struct.unpack('=Q', self.reada(addr, 8))
		return n

	def writea_long_long(self, addr, n):
		self.writea(addr, struct.pack('=Q', n))

	#------------------------------------------------------------------------------------------

	def reada_long(self, addr):
		self._memseta(addr, 4)
		(n,) = struct.unpack('=I', self.reada(addr, 4))
		return n

	def writea_long(self, addr, n):
		self.writea(addr, struct.pack('=I', n))
		
	#------------------------------------------------------------------------------------------

	def reada_byte(self, addr):
		self._memseta(addr, 1)
		(n,) = struct.unpack('=B', self.reada(addr, 1))
		return n

	def writea_byte(self, addr, n):
		self.writea(addr, struct.pack('=B', n))

	#------------------------------------------------------------------------------------------
