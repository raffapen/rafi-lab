
import os
import sys

"""
if 'PYDEBUG' in os.environ:
	import pdb; bb=pdb.set_trace
else:
	def bb(): pass
"""

import pdb; bb=pdb.set_trace

# sys.path.insert(0, 'vmsnparser-master/source')
import vmsn

# sys.path.insert(0, 'volatility-master')
# import volatility

#----------------------------------------------------------------------------------------------

def fread(fname):
	with open(fname, 'r') as file:
		return file.read()

#----------------------------------------------------------------------------------------------

def foo(snap):
	memory = snap["memory"]
	
	bb()
	if "regionsCount" in memory and memory["regionsCount"].read_long() > 0:
		region_count = memory["regionsCount"].read_long()
		print("Read region count from file: " + str(region_count))
	
		for region_i in range(0, region_count):
			memory_offset = memory["regionPPN"][region_i].read_long() * PAGE_SIZE
			file_offset = memory["regionPageNum"][region_i].read_long() * PAGE_SIZE + memory["Memory"][0][0].data_offset
			length = memory["regionSize"][region_i].read_long()*PAGE_SIZE
	
	else:
		memory_offset = 0
		file_offset = 0 + memory["Memory"][0][0].data_offset
		length = memory["Memory"][0][0].data_size
	
	print("RegionCount: %s" % (len(self.runs)))
	
	dtb = snap["cpu"]["CR"][0][3].read_long()
	print("dtb: {0:x}".format(dtb)) 

#----------------------------------------------------------------------------------------------

def foo1(snap):
	mem = snap["memory"]
	t1 = mem.search_tag('hotSet')

	print mem.tag_names()

	for i in range(0, snap.group_count - 1):
		g = snap[i]
		print g.name

#----------------------------------------------------------------------------------------------

def foo2():
	snap = vmsn.Snapshot('cygwin32-Snapshot1.vmsn')
	# snap = vmsn.Snapshot('cygwin32-Snapshot3.vmsn')
	# snap = vmsn.Snapshot('iphost1-Snapshot1.vmsn')
	# snap = vmsn.Snapshot('sn1.vmsn')

	sn1 = vmsn.Snapshot.create('sn1.vmsn', snap, ['Snapshot'])

	n = snap.report('Snapshot')
	print("total size: %s\n" % (n))

	n = sn1.report('Snapshot')
	print("total size: %s" % (n))

#----------------------------------------------------------------------------------------------

def foo3():
	snap = vmsn.Snapshot('cygwin32-Snapshot1.vmsn')
	snap['Snapshot']['cfgFile'].tdump()
	snap['Snapshot']['nvramFile'].dump()
	fn = snap['Snapshot']['extendedConfigFile'].tdump()


#----------------------------------------------------------------------------------------------

def foo4():
	snap1 = vmsn.Snapshot('cygwin32-Snapshot1.vmsn')
	snap7 = vmsn.Snapshot('iphost1-Snapshot1.vmsn')
	snap1a = vmsn.Snapshot.create('sn1.vmsn', snap1, ['Snapshot'])
	n = snap1a.report('Snapshot')
	print("total size: %s" % (n))

	fn = snap1['Snapshot']['cfgFile'].tdump()
	
	snap1b = snap1a['Snapshot']['cfgFile'].write(fread(fn))
	n = snap1b.report('Snapshot')
	print("total size: %s" % (n))

#----------------------------------------------------------------------------------------------

def foo5():
	# snap1 = vmsn.Snapshot('sn1.vmsn')
	snap1 = vmsn.Snapshot('cygwin32-Snapshot1.vmsn')
	n = snap1.report('Snapshot')
	print("total size: %s" % (n))

	fn = snap1['Snapshot']['cfgFile'].tdump()
	fn = snap1['Snapshot']['extendedConfigFile'].tdump()
	fn = snap1['Snapshot']['nvramFile'].dump()

#----------------------------------------------------------------------------------------------

def foo6():
	bb()
	snap1 = vmsn.Snapshot('cygwin32-Snapshot1.vmsn')
	snap1a = vmsn.Snapshot.create('sn1.vmsn', snap1, ['Snapshot'])
	# fn = snap1['Snapshot']['cfgFile'].tdump()
	snap1b = snap1a['Snapshot']['cfgFile'].write(fread('cygwin32-Snapshot1.vmsn.Snapshot.cfgFile-fixed'))

#----------------------------------------------------------------------------------------------

def foo6a():
	snap1 = vmsn.Snapshot('sn1.vmsn')
	with snap1['Snapshot']['cfgFile'] as g:
		g.write(fread('cygwin32-Snapshot1.vmsn.Snapshot.cfgFile-fixed'))
	snap1a = snap1.rewrite()

#----------------------------------------------------------------------------------------------

def foo7a():
	snap1 = vmsn.Snapshot('cygwin32-Snapshot1.vmsn')
	snap1a = vmsn.Snapshot.create('sn1.vmsn', snap1, ['Snapshot'])
	n = snap1a.report('Snapshot')
	print("total size: %s" % (n))
	with snap1a['Snapshot'] as g:
		g['cfgFile'].tdump()
		g['extendedConfigFile'].tdump()
		g['nvramFile'].dump()

def foo7b():
	snap1 = vmsn.Snapshot('sn1.vmsn')
	with snap1['Snapshot'] as g:
		g['cfgFile'].write(fread('sn1.vmsn.Snapshot.cfgFile-fixed'))
		g['nvramFile'].write(fread('sn1.vmsn.Snapshot.nvramFile-fixed'))
	snap1.rewrite('sn1-fixed.vmsn')

#----------------------------------------------------------------------------------------------

def foo8a():
	snap1 = vmsn.Snapshot('d:/vms/vmwx/portable-python/portable-python-Snapshot6.vmsn')
	snap1a = vmsn.Snapshot.create('sn1.vmsn', snap1, ['Snapshot'])
	with snap1a['Snapshot'] as g:
		g['cfgFile'].tdump()
		g['extendedConfigFile'].tdump()
		g['nvramFile'].dump()

def foo8b():
	snap1 = vmsn.Snapshot('sn1.vmsn')
	with snap1['Snapshot'] as g:
		g['cfgFile'].write(fread('sn1.vmsn.Snapshot.cfgFile'))
		g['nvramFile'].write(fread('sn1.vmsn.Snapshot.nvramFile'))
	snap1.rewrite('sn1-fixed.vmsn')

def foo8c():
	snap1 = vmsn.Snapshot('d:/vms/vmwx/portable-python/portable-python-Snapshot1.vmsn=')
	n = snap1.report('Snapshot')
	snap2 = vmsn.Snapshot('sn1-fixed.vmsn')
	n = snap2.report('Snapshot')

#----------------------------------------------------------------------------------------------

foo8a()
foo8b()
# foo8c()
print "fin"

