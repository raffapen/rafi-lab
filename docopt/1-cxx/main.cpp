//
//  main.cpp
//  docopt
//
//  Created by Jared Grubb on 2013-10-03.
//  Copyright (c) 2013 Jared Grubb. All rights reserved.
//

#include "docopt.h"
#include <iostream>

#if 0
static const char USAGE[] =
R"(Example of program with many options using docopt.

Usage:
  docopt1 [-hvqr -f NAME] [--exclude=PATTERNS]
          [--select=ERRORS | --ignore=ERRORS] [--show-source]
          [--statistics] [--count] [--benchmark] [PATH...]
  docopt1 (--doctest | --testsuite=DIR)
  docopt1 --version

Arguments:
  PATH  destination path

Options:
  -h --help            show this help message and exit
  --version            show version and exit
  -v --verbose         print status messages
  -q --quiet           report only file names
  -r --repeat          show all occurrences of the same error
  --exclude=PATTERNS   exclude files or directories which match these comma
                       separated patterns [default: .svn,CVS,.bzr,.hg,.git]
  -f NAME --file=NAME  when parsing directories, only check filenames matching
                       these comma separated patterns [default: *.py]
  --select=ERRORS      select errors and warnings (e.g. E,W6)
  --ignore=ERRORS      skip errors and warnings (e.g. E4,W)
  --show-source        show source code for each error
  --statistics         count errors and warnings
  --count              print total number of errors and warnings to standard
                       error and set exit code to 1 if total is not null
  --benchmark          measure processing speed
  --testsuite=DIR      run regression tests from dir
  --doctest            run doctest on myself

)";
#endif

#if 0
static const char USAGE[] =
R"(Naval Fate.

    Usage:
      naval_fate ship new <name>...
      naval_fate ship <name> move <x> <y> [--speed=<kn>]
      naval_fate ship shoot <x> <y>
      naval_fate mine (set|remove) <x> <y> [--moored | --drifting]
      naval_fate (-h | --help)
      naval_fate --version
      naval_fate -j<jj>

    Options:
      -h --help     Show this screen.
      --version     Show version.
      --speed=<kn>  Speed in knots [default: 10].
      --moored      Moored (anchored) mine.
      --drifting    Drifting mine.
      -j=<jj>       Jojo factor.
)";
#endif

#if 1
static const char USAGE[] =
R"(Notepad++ - Text editor

Usage:
  notepad++ [--multiInst] [--no-plugin] [--loadingTime]
            [--no-session] [--session=SESSION]
            [--globalcfg DIR] [--hostcfg DIR] [--usercfg DIR]
            [-l LANG] [-L CODE]
            [--alwaysOnTop] [--no-tabbar] [--systemtray] [-x LEFT] [-y TOP]
            [--qn=EGG | --qt=TEXT | --qf=FILE]
            [--ro] [-r] [-n LINE] [-c COL] [-p POS] [FILEPATH]
  notepad++ [--version | -h | --help]

Arguments:
  FILEPATH            File or folder name to open (absolute or relative path name)

Options:
  --multiInst         Launch another Notepad++ instance
  --no-plugin         Launch Notepad++ without loading any plugin
  --loadingTime       Display Notepad++ loading time

  --no-session        Launch Notepad++ without previous session
  --session=SESSION   Open a session from file SESSION

  --globalcfg DIR     Use DIR for global configuration directory (default is Notepad++ dir)
  --hostcfg DIR       Use DIR for host configuration directory
  --usercfg DIR       Use DIR for user configuration directory (default is %AppData%\Notepad++)

  -l LANG             Open FILEPATH by applying indicated programming language LANG
  -L CODE             Apply indicated localization, CODE is browser language code

  --alwaysOnTop       Make Notepad++ always on top
  --no-tabbar         Launch Notepad++ without tabbar
  --systemtray        Launch Notepad++ directly in system tray
  -x LEFT             Move Notepad++ to indicated left side position on the screen
  -y TOP              Move Notepad++ to indicated top position on the screen

  --ro                Make the filePath read only
  -r                  Open files recursively. Will be ignored if FILEPATH has no wildcards
  -n LINE             Scroll to indicated line on <filePath>
  -c COL              Scroll to indicated column on <filePath>
  -p POS              Scroll to indicated position on <filePath>

  --qn=EGG            Launch ghost typing to display easter egg via its name
  --qt=TEXT           Launch ghost typing to display a text via the given text
  --qf=FILE           Launch ghost typing to display a file content via the file path

  -h --help           Print help message
  --version           Print version
)";
#endif

int main(int argc, const char** argv)
{
	/* std::map<std::string, docopt::value> */ auto args = docopt::docopt(USAGE,
		{ argv + 1, argv + argc },
		true,              // show help if requested
		"Notepad++ 7.0");  // version string

	for (auto const& arg : args) {
		std::cout << arg.first << ": " << arg.second << std::endl;
	}

	auto name = args["<name>"];
	int n = name.asStringList().size();
	auto s = name.asStringList()[0];
	return 0;
}


