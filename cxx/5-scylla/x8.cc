
#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
#include <utility>

#include <string.h>

using namespace std;

void f1()
{
	vector<int> v{ 1, 10, 2, 9, 3, 8, 4, 7, 5, 6 };
	priority_queue<int, vector<int>, greater<int>> h(v.begin(), v.end());
	while (!h.empty())
	{
		auto x = h.top();
		h.pop();
		cout << x << "\n";
	}
}

struct rec_t
{
	unsigned char arr[10];

	rec_t(const char *s)
	{
		strncpy((char *) arr, s, sizeof(arr));
		arr[9] = '\0';
	};
	
	static bool lcompare(const rec_t &p, const rec_t &q)
	{
		return std::lexicographical_compare(&p.arr[0], (const unsigned char *) (&p.arr + 1), 
			&q.arr[0], (const unsigned char *) (&q.arr + 1));
	}

	const char *c_str() const { return (const char *) arr; }

#if 1
	bool operator==(const rec_t &x) const { return !strcmp((char *) arr, (char *) x.arr); }
	bool operator<(const rec_t &x) const { return rec_t::lcompare(*this, x); }
	bool operator>(const rec_t &x) const { return !(*this < x || *this == x); }
#endif
};

#if 0
bool operator==(const rec_t &p, const rec_t &q) { return !strcmp((char *) p.arr, (char *) q.arr); }
bool operator<(const rec_t &p, const rec_t &q) { return rec_t::lcompare(p, q); }
bool operator>(const rec_t &p, const rec_t &q) { return !(p < q || p == q); }
#endif

// using namespace std::rel_ops;

void f2()
{
	vector<rec_t> v{ "01", "10", "02", "09", "03", "08", "04", "07", "05", "06", "05", "01" };
	priority_queue<rec_t, vector<rec_t>, greater<rec_t>> h; //(v.begin(), v.end());
	for (auto &x: v)
		h.push(x);
	while (!h.empty())
	{
		auto x = h.top();
		h.pop();
		cout << x.c_str() << "\n";
	}
}

#if 0
void f3()
{
	rec_t a{"01"}, b{"02"};
	if (a < b)
		cout << "01 < 02\n";
	if (b > a)
		cout << "02 > 01\n";
}
#endif

int main()
{
	f2();
	//f3();
	return 0;
}
