
def foo1
	(1..10).reverse_each do |n|
		# puts '%04095d' % n
		puts '%09d' % n
	end
end

def foo2
	(1..10000).to_a.shuffle.each do |n|
		puts '%04095d' % n
	end
end

foo2
