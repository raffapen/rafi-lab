
#include <signal.h>
#include <memory.h>
#include <time.h>

#include <vector>
#include <queue>
#include <algorithm>
#include <chrono>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"
#include "core/sleep.hh"
#include "core/future-util.hh"
#include "core/thread.hh"

//---------------------------------------------------------------------------------------------

#define F_IN "fin"
#define F_OUT "fout"

// #define RAND_SORT_TIME

//---------------------------------------------------------------------------------------------

inline void __BB() { raise(SIGINT); }
#define BB __BB();

//---------------------------------------------------------------------------------------------

class Timer
{
	struct timespec t0, t1, dt;

public:
	void start()
	{
		timespec_get(&t0, TIME_UTC);
	}
	
	void stop()
	{
		timespec_get(&t1, TIME_UTC);
		
		if (t1.tv_nsec < t0.tv_nsec) 
		{
			dt.tv_sec = t1.tv_sec - t0.tv_sec - 1;
			dt.tv_nsec = t1.tv_nsec - t0.tv_nsec + 1000000000L;
		}
		else
		{
			dt.tv_sec = t1.tv_sec - t0.tv_sec;
			dt.tv_nsec = t1.tv_nsec - t0.tv_nsec;
		}
	}

	int hours() const { return dt.tv_sec / 3600; }
	int minutes() const { return (dt.tv_sec - hours() * 3600) / 60; }
	int seconds() const { return dt.tv_sec % 60; }
	int milliseconds() const { return dt.tv_nsec / 1000000; }

	std::string delta() const
	{
		char s[256];
		sprintf(s, "%2.2d:%2.2d:%2.2d.%d", hours(), minutes(), seconds(), milliseconds());
		return s;
	}
};

//---------------------------------------------------------------------------------------------

namespace extsort
{

int log_level = 1;

using namespace seastar;

using std::vector;
using std::cout;
using std::unique_ptr;
using std::shared_ptr;
using std::make_unique;
using std::make_shared;
using namespace std::chrono_literals;

inline future<> fu() { return make_ready_future<>(); }

//---------------------------------------------------------------------------------------------

class null_stream : public std::ostream 
{
public:
    struct nullbuf : public std::streambuf 
	{
        int overflow( int c ) { return c; }
    } _buf;

    null_stream() : std::ostream(&_buf) {}
};

inline std::ostream &log(int n)
{
	static null_stream nullstream;
	if (n <= extsort::log_level)
		return std::cout;
	else
		return nullstream;
}

//---------------------------------------------------------------------------------------------

struct rec_t
{
	unsigned char arr[4096];
	
	static bool compare(const rec_t &p, const rec_t &q)
	{
		return std::lexicographical_compare(&p.arr[0], (const unsigned char *) (&p.arr + 1), 
			&q.arr[0], (const unsigned char *) (&q.arr + 1));
	}
	
	std::string to_s() const
	{
		char s[10];
		memcpy(s, &arr[4096-6], 5);
		s[5] = '\0';
		return s;
	}
};

using recptr_t = unique_ptr<rec_t, free_deleter>;
using bufptr_t = unique_ptr<char [], free_deleter>;

//---------------------------------------------------------------------------------------------

class Chunk
{
	friend class Chunks;

public:
	class Subchu
	{
		Chunk &_chunk;
		uint64_t _pos; // position of record relative to chunk
		uint64_t _nrec;
		uint64_t _idx;
		
	public:
		Subchu(Chunk &chunk, uint64_t pos, uint64_t nrec) : _chunk(chunk), _pos(pos), _nrec(nrec),
			_idx(0)
		{
			log(4) << "    subchunk #" << _chunk._chi << " [" << _pos << ", " << _nrec << "]\n";			
		}
	
		void sort()
		{
			log(3) << "    sorting subchunk #" << _chunk._chi << " [" << _pos << ", " << _nrec << "] ...\n";
			rec_t *recs = reinterpret_cast<rec_t*>(_chunk._buf);
			Timer timer;
			timer.start();
			std::sort(&recs[_pos], &recs[_pos + _nrec], rec_t::compare);
#ifdef RAND_SORT_TIME
			std::this_thread::sleep_for(100ms * (std::rand() % 10));
#endif
			timer.stop();
			log(3) << "    done with subchunk #" << _chunk._chi << " [" << _pos << ", " << _nrec << "] time=" << timer.delta() << " ...\n";
		}

		bool empty() const { return _idx == _nrec; }
		
		future<bufptr_t> read_rec(uint64_t idx)
		{
			return _chunk.read_rec(_pos + idx);
		}

		struct heaprec_t
		{
			heaprec_t(rec_t &r, Subchu &subchu, int index) : subchu(subchu), index(index)
			{
				rec = r;
			}
			
			rec_t rec;
			Subchu &subchu;
			int index;

			struct heap_compare
			{
				bool operator()(const shared_ptr<heaprec_t> &p, const shared_ptr<heaprec_t> &q)
				{
					return !rec_t::compare(p->rec, q->rec);
				}
			};
		};
		
		using hrecptr_t = unique_ptr<heaprec_t>;

		future<hrecptr_t> pop()
		{
			if (empty())
				return make_ready_future<hrecptr_t>(nullptr);

			int i = _idx++;
			return read_rec(i).then([this, i] (auto bufptr) {
				rec_t *rec = reinterpret_cast<rec_t*>(bufptr.get());
				auto e = std::make_unique<heaprec_t>(*rec, *this, i);
				return make_ready_future<hrecptr_t>(std::move(e));
			});
		}
	};

protected:
	file &_file;
	unsigned char *_buf;
	int _chi;
	uint64_t _pos; // position (in recs) of chunk relative to start of file
	uint64_t _nrec;
	vector<shared_ptr<Subchu>> _subs;

	Timer _timer;
	std::vector<future<>> _subs_sort_fs;

	void make_subs()
	{
		uint64_t sub_nrec = _nrec / smp::count;
		if (sub_nrec * smp::count < _nrec)
			++sub_nrec;
		uint64_t p;
		for (p = 0; p + sub_nrec < _nrec; p += sub_nrec)
			_subs.push_back(make_shared<Subchu>(*this, p, sub_nrec));
		if (_nrec > p)
			_subs.push_back(make_shared<Subchu>(*this, p, _nrec - p));
	}

public:
	Chunk(file &fi, unsigned char *buf, int chi, uint64_t pos, uint64_t nrec) : 
		_file(fi), _buf(buf), _chi(chi), _pos(pos), _nrec(nrec)
	{
		log(3) << "  chunk " << _chi << " [" << _pos << ", " << _nrec << "]\n";
		make_subs();
	}

	struct sort_complete {};

	future<> sort()
	{
		_timer.start();
		log(2) << "  sorting chunk " << _chi << ": pos=" << _pos << " nrec=" << _nrec << "\n";
		return read().then([this] (size_t rd) {
			log(3) << "  read chunk " << _chi << ": " << rd << " bytes\n";

			int cpu = 0;
			return parallel_for_each(_subs.begin(), _subs.end(), [this, &cpu] (auto &sub) {
				auto f = smp::submit_to(cpu++, [sub] () { sub->sort(); }); 
				_subs_sort_fs.push_back(std::move(f));
			}).then([this] {
				return when_all(_subs_sort_fs.begin(), _subs_sort_fs.end()).discard_result();
			});

		}).then([this] () {
			_timer.stop();
			log(3) << "  completed sort for chunk " << _chi << "time=" << _timer.delta() << "\n";
			return write();
		});
	}

	future<size_t> read()
	{
		return _file.dma_read(_pos * sizeof(rec_t), _buf, sizeof(rec_t) * _nrec);
	}

	future<bufptr_t> read_rec(uint64_t idx)
	{
		auto buf = allocate_aligned_buffer<char>(sizeof(rec_t), 4096);
        auto rec = buf.get(); 
		return _file.dma_read((_pos + idx) * sizeof(rec_t), rec, sizeof(rec_t))
			.then([buf = std::move(buf), this] (auto size) mutable {
				bufptr_t bufptr(std::move(buf));
				return make_ready_future<bufptr_t>(std::move(bufptr));
			});
	}

	future<> write(bool flush = false)
	{
		log(3) << "  writing chunk " << _chi << "\n";
		return _file.dma_write(_pos * sizeof(rec_t), _buf, sizeof(rec_t) * _nrec)
			.then([this, flush] (size_t size) mutable {
				log(10) << "  after write " << _chi << ": " << size << " bytes\n";
				return flush ? _file.flush() : fu();
			}).then([this] () {
				log(4) << "  writing chunk " << _chi << ": complete\n";
				return fu();
			});
	}
};

//---------------------------------------------------------------------------------------------

template <typename T>
using min_heap = std::priority_queue<shared_ptr<T>, vector<shared_ptr<T>>, typename T::heap_compare>;

//---------------------------------------------------------------------------------------------

class Chunks : public vector<shared_ptr<Chunk>>
{
	file &_rfile;
	file *_wfile;
	unsigned char *_buf;
	uint64_t _nr_ram;

	// for merge operation
	min_heap<Chunk::Subchu::heaprec_t> _heap;
	uint64_t wf_idx = 0;
	uint64_t buf_idx = 0;
	rec_t *recs = 0;
	
public:
	Chunks(file &rf, unsigned char *buf, uint64_t nr_file, uint64_t nr_ram) : 
		_rfile(rf), _wfile(nullptr), _buf(buf), _nr_ram(nr_ram)
	{
		log(2) << "  setting up chunks...\n";
		int n_chunks = nr_file / nr_ram;
		int64_t suffix = nr_file - n_chunks * nr_ram;
		uint64_t pos = 0;
		int chi;
		for (chi = 0; chi < n_chunks; ++chi)
		{
			push_back(std::make_shared<Chunk>(_rfile, buf, chi, pos, nr_ram));
			pos += nr_ram;
		}
		if (suffix > 0)
		{
			++n_chunks;
			push_back(std::make_shared<Chunk>(_rfile, buf, chi, pos, suffix));
		}
		log(3) << "  created " << n_chunks << " chunks\n";
	}

	future<> sort()
	{
		log(1) << "sorting chunks...\n";
		return do_for_each(begin(), end(), [this] (auto &chunk) { return chunk->sort(); })
			.then([this] {
				log(2) << "  completed sort, flushing...\n";
				return _rfile.flush();
			});
	}

	future<> merge(file &sorted_file)
	{
		_wfile = &sorted_file;
		
		log(1) << "merge chunks...\n";
		return do_for_each(begin(), end(), [this] (auto &ch) {
			log(3) << "  merging subchunks from chunk " << ch->_chi << "\n";
			
			return do_for_each(ch->_subs, [this] (auto &sub) {
				if (!sub->empty())
				{
					return sub->pop().then([this] (Chunk::Subchu::hrecptr_t e) {
						_heap.push(std::move(e));
					});
				}
				else
					return make_ready_future<>();
			});
			
		}).then([this] {
			log(3) << "  initial heap size: " << _heap.size() << "\n";

			wf_idx = 0;
			buf_idx = 0;
			recs = reinterpret_cast<rec_t*>(_buf);
			
			return repeat([this] {
				if (_heap.empty())
					return make_ready_future<stop_iteration>(stop_iteration::yes);

				return repeat([this] {
					if (buf_idx == _nr_ram)
						return make_ready_future<stop_iteration>(stop_iteration::yes);

					if (_heap.empty())
					{
						log(10) << "  heap empty!\n";
						return make_ready_future<stop_iteration>(stop_iteration::yes);
					}
					auto e = _heap.top();
					_heap.pop();
					recs[buf_idx++] = e->rec;
					if (e->subchu.empty())
						return make_ready_future<stop_iteration>(stop_iteration::no);

					return e->subchu.pop().then([this] (auto e1) {
						_heap.push(std::move(e1));
						return make_ready_future<stop_iteration>(stop_iteration::no);
					});

				}).then([this] {
					return write_sorted(wf_idx, buf_idx)
						.then([this] {
							wf_idx += _nr_ram;
							buf_idx = 0;
							return make_ready_future<stop_iteration>(stop_iteration::no);
						});
				});
			});
		});
	}
	
	future<> write_sorted(uint64_t pos, uint64_t nr)
	{
		return _wfile->dma_write(pos * sizeof(rec_t), _buf, sizeof(rec_t) * nr)
			.then([this, pos, nr] (size_t size) mutable {
				log(2) << "  writing chunk pos=" << pos << " nrec=" << nr << "\n";
				return fu();
			});
	}
};

//---------------------------------------------------------------------------------------------

class Sort
{
	sstring _fname;
	file _rfile;
	uint64_t _nr_file;

	uint64_t _nr_ram;

	sstring _sorted_fname;
	file _wfile;

	unique_ptr<unsigned char[], seastar::free_deleter> _buffer;
	unsigned char *_buf;

	unique_ptr<Chunks> _chunks;

public:
	Sort(const sstring &fname, const sstring &sorted_fname, uint64_t nr_ram) :
		_fname(fname),
		_nr_file(0), _nr_ram(nr_ram),
		 _sorted_fname(sorted_fname),
		_buffer(allocate_aligned_buffer<unsigned char>(sizeof(rec_t) * _nr_ram, 4096))
	{
		_buf = _buffer.get();
	}
	
	future<> open_source_file()
	{
		log(1) << "open source file '" << _fname << "'...\n";
		return open_file_dma(_fname, open_flags::rw)
			.then([this] (file f) {
				_rfile = std::move(f);
				return _rfile.size();
			}).then([this] (uint64_t size) {
				_nr_file = size / sizeof(rec_t);
				log(2) << "  NR=" << _nr_file << "\n";
				return fu();
			});
	}

	future<> create_chunks()
	{
		log(1) << "creating chunks...\n";
		_chunks = std::make_unique<Chunks>(_rfile, _buf, _nr_file, _nr_ram);
		return fu();
	}

	future<> sort_chunks()
	{
		return _chunks->sort();
	}

	future<> create_sorted_file()
	{
		log(1) << "create sorted file '" << _sorted_fname << "'...\n";
		return open_file_dma(_sorted_fname, open_flags::wo | open_flags::create)
			.then([this] (file f) {
				_wfile = std::move(f);
				return _wfile.allocate(0, _nr_file * sizeof(rec_t));
			});
	}
	
	future<> merge_chunks()
	{
		return _chunks->merge(_wfile);
	}
	
	future<> run() 
	{
		return open_source_file()
			.then([this] {
				return create_chunks();
			}).then([this] {
				return sort_chunks();
			}).then([this] {
				return create_sorted_file();
			}).then([this] {
				return merge_chunks();
			}).finally([this] {
				finally();
			});
	}

	void finally()
	{
		log(1) << "done.\n";
	}
};

//---------------------------------------------------------------------------------------------

} // namespace extsort

//---------------------------------------------------------------------------------------------

int main(int argc, char* argv[])
{
	srand (time(NULL));
    seastar::app_template app;

	namespace args = boost::program_options;
    app.add_options()
        ("source-file", args::value<std::string>()->default_value("fin"), "Source file")
		("sorted-file", args::value<std::string>()->default_value("fout"), "Sorted file")
        ("records-in-ram", args::value<uint64_t>()->default_value(1000), "records in RAM")
        ("sort-log-level", args::value<int>()->default_value(2), "Log level of sort operation");

    app.run(argc, argv, [&] {
		auto &&config = app.configuration();
		extsort::log_level = config["sort-log-level"].as<int>();
		return seastar::do_with(extsort::Sort(config["source-file"].as<std::string>(), 
			config["sorted-file"].as<std::string>(), config["records-in-ram"].as<uint64_t>()), 
			[] (auto &f) { return f.run(); });
    });
}
