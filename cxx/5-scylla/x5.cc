
#include <signal.h>

#include <vector>
#include <algorithm>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"

#define BB raise(SIGINT);

using namespace seastar;

int main(int argc, char *argv[])
{
    seastar::app_template app;
    app.run(argc, argv, [] {
		return open_file_dma("r3", open_flags::create | open_flags::rw)
			.then([] (file f) {
				return do_with(std::move(f), [] (file& f) {
					auto size = 4096;
					auto buf = allocate_aligned_buffer<char>(4096, 4096);
					auto wbuf = buf.get();
					return f.dma_write(0, wbuf, size).then([&f] (size_t s) {
						std::cout << "after write: " << s << " -> flushing\n";
						f.flush();
					}).finally([&f] () mutable {
						std::cout << "closing\n";
						return f.close();
					});
				});
			}).then([] {
				std::cout << "done\n";
			});
	});
}


#if 0
		return open_file_dma("r3", open_flags::rw | open_flags::create)
			.then([] (file f) {
				auto buf = allocate_aligned_buffer<unsigned char>(4096, 4096);
				auto rbuf = buf.get();
				f.dma_write(0, rbuf, 4096);
#if 0
					.then([f] () mutable {
						f.close();
					});
#endif
			}).then([] {
				std::cout << "done\n";
			});
#endif
