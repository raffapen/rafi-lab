
#include <signal.h>

#include <vector>
#include <algorithm>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"

#define BB raise(SIGINT);

using namespace seastar;

struct rec_t
{
	unsigned char arr[4096];
	
	static bool compare(const rec_t &p, const rec_t &q)
	{
		return std::lexicographical_compare(&p.arr[0], (const unsigned char *) (&p.arr + 1), 
			&q.arr[0], (const unsigned char *) (&q.arr + 1));
	}
};

using std::string;

class F1
{
	const string _fname, _out_fname;
	int _nrec;
	std::unique_ptr<file> _rfile;
	std::unique_ptr<file> _wfile;
	std::unique_ptr<unsigned char[], seastar::free_deleter> _buf;
	unsigned char *_rbuf;

public:
	F1(const string &fname, const string &out_fname, int nrec) : 
		_fname(fname), _out_fname(out_fname),
		_nrec(nrec),
		_buf(allocate_aligned_buffer<unsigned char>(sizeof(rec_t) * _nrec, 4096))
	{
		_rbuf = _buf.get();
	}
	
	void sort()
	{
		std::cout << "sort\n";
		rec_t *recs = reinterpret_cast<rec_t*>(_rbuf);
		std::sort(&recs[0], &recs[_nrec], rec_t::compare);
	}
	
	future<> open_read()
	{
		return open_file_dma(_fname, open_flags::rw)
			.then([this] (file f) {
				_rfile = std::make_unique<file>(f);
				return make_ready_future<>();
			});
	}
	
	future<size_t> read()
	{
		return _rfile->dma_read(0, _rbuf, sizeof(rec_t) * _nrec);
	}
	
	future<> open_write()
	{
		return open_file_dma(_out_fname, open_flags::create | open_flags::rw)
			.then([this] (file f) {
				_wfile = std::make_unique<file>(f);
				return make_ready_future<>();
			});
	}

	future<> write_back()
	{
		std::cout << "write it\n";
		return _wfile->dma_write(0, _rbuf, sizeof(rec_t) * _nrec)
			.then([this] (size_t wsize) mutable {
				std::cout << "after write: " << wsize << " -> flushing\n";
				return _wfile->flush();
			}).then([this] () mutable {
				std::cout << "closing\n";
				return _wfile->close();
			}).finally([this] () mutable {
				std::cout << "write complete\n";
				_wfile = nullptr;
			});
	}
	
	future<> run()
	{
		return open_read().then([this] 
			{
				return read()
					.then([this] (size_t ret) mutable {
						std::cout << "got " << ret << "\n";
						sort();
						return open_write().then([this] {
							return write_back(); 
						});
					});
			}).finally([this] {
				finally();
			});
	}
	
	void finally()
	{
		std::cout << "done\n";
	}
};

int main(int argc, char *argv[])
{
    seastar::app_template app;
    app.run(argc, argv, [] {
		return do_with(F1("r1", "r3", 10), [] (auto &f) { return f.run(); });
	});
}
