
chunks = []

nr_disk = 110
nr_ram = 32
n_cores = 6

ch = (nr_ram / n_cores.to_f).ceil

n = nr_disk
while n > 0 do
	m = 0
	while m + ch < nr_ram && n - ch > 0 do
		chunks << ch
		puts ch
		m += ch
		n -= ch
	end
	break if n - ch < 0
	if m + ch > nr_ram
		x = nr_ram - m
		chunks << x
		puts x
		m += x
		n -= x
	end
	puts "- #{m} #{n}"
end
if n > 0
	puts n
	chunks << n
end

puts "---"
puts chunks.reduce(:+)