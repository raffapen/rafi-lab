
#include <signal.h>

#include <vector>
#include <algorithm>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"

#define BB raise(SIGINT);

using namespace seastar;

struct rec_t
{
	unsigned char arr[4096];
	
	static bool compare(const rec_t &p, const rec_t &q)
	{
		return std::lexicographical_compare(&p.arr[0], (const unsigned char *) (&p.arr + 1), 
			&q.arr[0], (const unsigned char *) (&q.arr + 1));
	}
};


int main(int argc, char *argv[])
{
    seastar::app_template app;
    app.run(argc, argv, [] {
		return open_file_dma("r1", open_flags::rw)
			.then([] (file f) {
				auto buf = allocate_aligned_buffer<unsigned char>(4096 * 10, 4096);
				auto rbuf = buf.get();
				return f.dma_read(0, rbuf, 4096 * 10)
					.then([f, rbuf = std::move(rbuf)] (size_t ret) mutable {
						std::cout << "got " << ret << "\n";
						std::cout << "sort\n";
						rec_t *buf = reinterpret_cast<rec_t*>(rbuf);
						std::sort(&buf[0], &buf[9 + 1], rec_t::compare);
						return open_file_dma("r3", open_flags::create | open_flags::rw)
							.then([rbuf = std::move(rbuf)] (file f1) {
								return do_with(std::move(f1), [rbuf = std::move(rbuf)] (file &f1) 
								{
									std::cout << "write it\n";
									return f1.dma_write(0, rbuf, 4096 * 10)
										.then([&f1] (size_t wsize) mutable {
											std::cout << "after write: " << wsize << " -> flushing\n";
											return f1.flush();
										}).finally([&f1] () mutable {
											std::cout << "closing\n";
											return f1.close();
										});
								});
							});
					});
			}).then([] {
				std::cout << "done\n";
			});
	});
}
