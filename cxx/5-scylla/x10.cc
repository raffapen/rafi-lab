#include <signal.h>

#include <vector>
#include <algorithm>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"

#include <boost/range/irange.hpp>

#define BB raise(SIGINT);

using namespace seastar;


int main(int argc, char *argv[])
{
    seastar::app_template app;
    app.run(argc, argv, [] {
#if 0
		return parallel_for_each(boost::irange<int>(0,4), [] (auto n) { 
			for (auto i: boost::irange<int>(n*10,n*10+10))
				std::cout << i << "\n"; 
		});
#else
		return parallel_for_each(boost::irange<int>(0,4), [] (auto n) {
			return smp::submit_to(n, [n] () {
				for (auto i: boost::irange<int>(n*10,n*10+10))
					std::cout << i << "\n"; 
			});
		});
#endif
	});
}  
