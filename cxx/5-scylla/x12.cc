#include <signal.h>

#include <vector>
#include <algorithm>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"
#include "core/thread.hh"

#include <boost/range/irange.hpp>

using namespace seastar;
using namespace std;

class E
{
public:
	int n;
	
	E(int n) : n(n) {}
	
	future<int> val() { return make_ready_future<int>(n); }
};

class F1
{
public:
#if 1
	vector<E> v{1, 2, 3, 4, 5, 6, 7};

#elif 0
	vector<int> v{0, 1, 2, 3, 4, 5, 6};

#endif

	future<> run()
	{
#if 1
		return repeat([this] {
			if (v.empty())
				return make_ready_future<stop_iteration>(stop_iteration::yes);
			
			auto e = v.back();
			v.pop_back();
			return e.val().then([this](auto n) {
				cout << n << "\n";
				return make_ready_future<stop_iteration>(stop_iteration::no);
			});
			
		}).then([this] {
			cout << "done" << "\n";
		});
#else
		return make_ready_future<>();
#endif
	}
};

int main(int argc, char *argv[])
{
    seastar::app_template app;
    app.run(argc, argv, [] {
		return do_with(F1(), [] (auto &f) { return f.run(); });
	});
}  
