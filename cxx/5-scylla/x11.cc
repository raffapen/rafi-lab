#include <signal.h>

#include <vector>
#include <algorithm>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"
#include "core/thread.hh"

#include <boost/range/irange.hpp>

using namespace seastar;
/*
using std::vector;
using std::cout;
using std::unique_ptr;
using std::shared_ptr;
*/
using namespace std;

class E
{
public:
	int n;
	
	E(int n) : n(n) {}
	
	future<int> val() { return make_ready_future<int>(n); }
};

class F1
{
public:
#if 0
	vector<E> v{1, 2, 3, 4, 5, 6, 7};
	vector<int> w;
	vector<E>::iterator i;

#elif 0
	vector<E> v{0, 1, 2, 3, 4, 5, 6};
	vector<vector<E>> w{{1}, {1,2}, {1,2,3}, {1,2,3,4}, {1,2,3,4,5}, {1,2,3,4,5,6}, {1,2,3,4,5,6,7}};
	vector<int> a;

#else
	vector<std::shared_ptr<E>> v{std::make_shared<E>(0), std::make_shared<E>(1), std::make_shared<E>(2), std::make_shared<E>(3), std::make_shared<E>(4)};
	vector<int> a;
#endif

	future<> run()
	{
#if 0
		return async([this] {
			for (auto e: v)
				cout << e.val().get0() << "\n";
		});

#elif 0
		return map_reduce(v, [] (auto e) { return e.val(); }, &w, [] (auto *w, auto n) { w->push_back(n); return w; })
			.then([] (auto *w) {
				for (auto n: *w)
					cout << n << "\n";
			});
#elif 0
		return map_reduce(v, [] (auto e) { return e.val(); }, w, [] (auto &w, auto n) { w.push_back(n); return w; });
			.then([] (auto &w) {
				for (auto n: w)
					cout << n << "\n";
			});

#elif 0
		return do_for_each(v, [this] (auto &e) {
			return e->val().then([this] (auto n) {
				cout << "v: " << n << "\n";
				return do_for_each(w[n], [this] (E &g) {
					cout << "w:   " << g.n << "\n";
					return g.val().then([this] (auto m) {
						a.push_back(m);
						//return make_ready_future<>();
					});
				});
			});
		}).then([this] {
			for (auto n: a)
				cout << n << "\n";
		});

#elif 1
		return do_for_each(v, [this] (auto &e) {
				cout << e->n << "\n";
				return e->val().then([&, this] (auto n) {
					a.push_back(n);
				});
			}).then([this] {
				for (auto n: a)
					cout << n << "\n";
			});

#elif 0
		unsigned int j = 0;
		return repeat([&j, this] () mutable {
			for (; j < v.size(); ++j)
			{
				auto &e = v[j];
				cout << e.n << "\n";
				return e.val().then([&, this] (auto n) mutable {
					w.push_back(n);
				}).then([&, this] {return make_ready_future<stop_iteration>(stop_iteration::no); });
			}
			return make_ready_future<stop_iteration>(stop_iteration::yes); 
		}).then([this] {
			for (auto n: w)
				cout << n << "\n";
		});

#elif 0
		i = v.begin();
		return repeat([&, this] () mutable {
			for (; i != v.end(); ++i)
			{
				auto &e = *i;
				cout << e.n << "\n";
				return e.val().then([&, this] (auto n) mutable {
					w.push_back(n);
				}).then([&, this] {return make_ready_future<stop_iteration>(stop_iteration::no); });
			}
			return make_ready_future<stop_iteration>(stop_iteration::yes); 
		}).then([this] {
			for (auto n: w)
				cout << n << "\n";
		});
#else
		return make_ready_future<>();
#endif
	}
};

int main(int argc, char *argv[])
{
    seastar::app_template app;
    app.run(argc, argv, [] {
		return do_with(F1(), [] (auto &f) { return f.run(); });
	});
}  
