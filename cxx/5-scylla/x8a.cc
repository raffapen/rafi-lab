
#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
#include <utility>
#include <memory>

#include <string.h>

using namespace std;

void f1()
{
	vector<int> v{ 1, 10, 2, 9, 3, 8, 4, 7, 5, 6 };
	priority_queue<int, vector<int>, greater<int>> h(v.begin(), v.end());
	while (!h.empty())
	{
		auto x = h.top();
		h.pop();
		cout << x << "\n";
	}
}

struct rec_t
{
	unsigned char arr[10];

	rec_t(const char *s)
	{
		strncpy((char *) arr, s, sizeof(arr));
		arr[9] = '\0';
	};
	
	static bool lcompare(const rec_t &p, const rec_t &q)
	{
		return std::lexicographical_compare(&p.arr[0], (const unsigned char *) (&p.arr + 1), 
			&q.arr[0], (const unsigned char *) (&q.arr + 1));
	}

	const char *c_str() const { return (const char *) arr; }

	struct heap_compare
	{
		bool operator()(const shared_ptr<rec_t> &p, const shared_ptr<rec_t> &q)
		{
			return !rec_t::lcompare(*p, *q);
		}
	};
};

void f2()
{
	vector<rec_t> v{ "01", "10", "02", "09", "03", "08", "04", "07", "05", "06", "05", "01" };
	priority_queue<shared_ptr<rec_t>, vector<shared_ptr<rec_t>>, rec_t::heap_compare> h;
	for (auto &x: v)
		h.push(std::move(make_shared<rec_t>(x)));
	while (!h.empty())
	{
		auto x = h.top();
		h.pop();
		cout << x->c_str() << "\n";
	}
}

int main()
{
	f2();
	return 0;
}
