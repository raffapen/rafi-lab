
#include <stdio.h>
#include <time.h>
#include <vector>
#include <algorithm>
#include <memory>
#include <iostream>
#include <chrono>
#include <unistd.h>
#include <thread> 

class Timer
{
	struct timespec t0, t1, dt;

public:
	void start()
	{
		timespec_get(&t0, TIME_UTC);
	}
	
	void stop()
	{
		timespec_get(&t1, TIME_UTC);
		
		if (t1.tv_nsec < t0.tv_nsec) 
		{
			dt.tv_sec = t1.tv_sec - t0.tv_sec - 1;
			dt.tv_nsec = t1.tv_nsec - t0.tv_nsec + 1000000000L;
		}
		else
		{
			dt.tv_sec = t1.tv_sec - t0.tv_sec;
			dt.tv_nsec = t1.tv_nsec - t0.tv_nsec;
		}
	}

	int hours() const { return dt.tv_sec / 3600; }
	int minutes() const { return (dt.tv_sec - hours() * 3600) / 60; }
	int seconds() const { return dt.tv_sec % 60; }
	int milliseconds() const { return dt.tv_nsec / 1000000; }

	std::string delta() const
	{
		char s[256];
		sprintf(s, "%2.2d:%2.2d:%2.2d.%d", hours(), minutes(), seconds(), milliseconds());
		return s;
	}
};

#if 0

typedef unsigned char *cp_t;

bool compare(unsigned char *p, unsigned char *q)
{
	return std::lexicographical_compare(p, p + 4096 - 1, q, q + 4096 - 1);
}

int main(int argc, char *argv[])
{
	auto f = fopen("r1", "rb");
	auto buf = new char[10][4096];
	int n = fread(buf, 4096, 10, f);
	bool b = compare((cp_t) &buf[0], (cp_t) &buf[1]);
	b = compare((cp_t) &buf[1], (cp_t) &buf[0]);
	unsigned char **p = (unsigned char **)buf;
	unsigned char **q = &p[4096 * 9];
	std::sort(p, q, compare);
	fclose(f);
}

#endif

struct rec_t
{
	//unsigned char arr[4096];
	unsigned char arr[10];
	
	static bool compare(const rec_t &p, const rec_t &q)
	{
		const unsigned char *x0 = &p.arr[0];
		//const unsigned char *x1 = &p.arr[0] + sizeof(arr);
		const unsigned char *x2 = (const unsigned char *) (&p.arr + 1);
		const unsigned char *y0 = &q.arr[0];
		//const unsigned char *y1 = &q.arr[0] + sizeof(arr);
		const unsigned char *y2 = (const unsigned char *) (&q.arr + 1);
		//return std::lexicographical_compare(&p.arr, &p.arr + 1, &q.arr, &q.arr + 1);
		//return std::lexicographical_compare(x0, x2, y0, y2);
		return std::lexicographical_compare(&p.arr[0], (const unsigned char *) (&p.arr + 1), 
			&q.arr[0], (const unsigned char *) (&q.arr + 1));
	}
};

using namespace std::chrono_literals;


int main(int argc, char *argv[])
{
#if 0
	Timer timer;
	timer.start();
	std::this_thread::sleep_for(2200ms);
	timer.stop();
	std::cout << "elapsed: " << timer.delta() << "\n";
	return 0;
#endif
	auto f = fopen("r1", "rb");
	rec_t *buf = new rec_t[10];
	int n = fread((unsigned char *) buf, sizeof(rec_t), 10, f);
	fclose(f);
	bool b = rec_t::compare(buf[0], buf[1]);
	b = rec_t::compare(buf[1], buf[0]);
	
	std::sort(&buf[0], &buf[10], rec_t::compare);
	auto fw = fopen("r2", "wb");
	fwrite((unsigned char *) buf, sizeof(rec_t), 10, fw);
	fclose(fw);
}
