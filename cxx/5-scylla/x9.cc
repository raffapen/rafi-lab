
#include <signal.h>

#include <vector>
#include <algorithm>

#include <seastar/core/seastar.hh>
#include "core/app-template.hh"
#include "core/reactor.hh"
#include "core/thread.hh"

#define BB raise(SIGINT);

using namespace seastar;

int main(int argc, char *argv[])
{
    seastar::app_template app;
    app.run(argc, argv, [] {
#if 1
		return async([] () {
			auto f = open_file_dma("r2", open_flags::rw).get0();
			auto buf = allocate_aligned_buffer<char>(4096, 4096);
			auto wbuf = buf.get();
			size_t n = f.dma_read(0, wbuf, 10).get0();
			wbuf[10] = '\0';
			std::cout << "read [" << n << "]: " << wbuf;
			f.close().get();
		});
#else
		return open_file_dma("r2", open_flags::rw)
			.then([] (file f) {
				return do_with(std::move(f), [] (file& f) {
					auto buf = allocate_aligned_buffer<char>(4096, 4096);
					auto wbuf = buf.get();
					size_t n = f.dma_read(0, wbuf, 10).get0();
					std::cout << "read [" << n << "]: " << wbuf;
					return f.close();
				});
			}).then([] {
				std::cout << "done\n";
			});
#endif
	});
}
