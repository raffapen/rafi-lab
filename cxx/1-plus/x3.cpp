
#include <string>
#include <stdio.h>
#include <cassert>

using std::string;

class text : public string
{
	typedef string Super;
	
	string &super() { return *this; }
	const string &super() const { return *this; }

public:
	text(const char *p) : Super(p) {}
	text(const string &s) : Super(s) {}
	//text(string &&s) : Super(s) {}
	
	string &operator*() { return super(); }
	const string &operator*() const { return super(); }

	text &operator=(const string &s) { super() = s; return *this; }
	//text &&operator=(string &&s) { super() = s; return std::move(*this); }
};

#if 0

class Thing : public text
{
public:
	text &value() { return *this; }
	const text &value() const { return *this; }

public:
	Thing(const string &name) : text(name) {}

	const text &operator*() const { return value(); }
		
	Thing &operator=(const text &s) { value() = s; return *this; }
};

#else

class Thing
{
	text _val;

public:
	text &value() { return _val; }
	const text &value() const { return _val; }

public:
	Thing(const char *name) : _val(name) {}
	Thing(const string &name) : _val(name) {}

	operator text&() { return value(); }
	operator const text&() const { return value(); }

	bool operator==(const Thing &t) const { return value() == t.value(); }

	const text &operator*() const { return value(); }
		
	Thing &operator=(const text &s) { value() = s; return *this; }
};

inline Thing operator+(const Thing &t, const Thing &s) { return Thing(*t + *s); }

#endif

void f0()
{
	const char *a = "a";
	string b("b");
	text c("c");
	Thing d("d");
#define x(s) Thing(#s)

	assert(a + b + c + d == x(abcd));
	assert(a + b + d + c == x(abdc));
	assert(a + c + b + d == x(acbd));
	assert(a + c + d + b == x(acdb));
	assert(a + d + b + c == x(adbc));
	assert(a + d + c + b == x(adcb));
	assert(b + a + c + d == x(bacd));
	assert(b + a + d + c == x(badc));
	assert(b + c + a + d == x(bcad));
	assert(b + c + d + a == x(bcda));
	assert(b + d + a + c == x(bdac));
	assert(b + d + c + a == x(bdca));
	assert(c + a + b + d == x(cabd));
	assert(c + a + d + b == x(cadb));
	assert(c + b + a + d == x(cbad));
	assert(c + b + d + a == x(cbda));
	assert(c + d + a + b == x(cdab));
	assert(c + d + b + a == x(cdba));
	assert(d + a + b + c == x(dabc));
	assert(d + a + c + b == x(dacb));
	assert(d + b + a + c == x(dbac));
	assert(d + b + c + a == x(dbca));
	assert(d + c + a + b == x(dcab));
	assert(d + c + b + a == x(dcba));
	
#undef x	
}

void f1()
{
	text p1 = "jojo";
	text t = "foo";
	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string b = p1 + string(" ");// + t + string("/sys/bin/mk.pl");
	string c = string(" ") + p1;
	string d = p1 + string(" ") + t + "/sys/bin/mk.pl";
}

void f2()
{
	Thing t = "jojo";
	Thing p1(
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;");

	Thing p2 =
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;";

	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string c = string(" ") + p1;
	string c1 = " " + p1;
	string d = p1 + string(" ") + t + string("/sys/bin/mk.pl");
}

void f3()
{
	string s = "abc";
	string s1 = s + "def";
	string s2 = "def" + s;
	auto s2a = "def" + s;
	string s3 = s + "def" + s;
	string s4 = s + "def" + "ghi";
}

void f4()
{
	text t = "abc";
	text t1 = t + "def";
	text t2 = "def" + t;
	auto t2a = "def" + t;
	text t3 = t + "def" + t;
	text t4 = t + "def" + "ghi";
}

void f5()
{
	text t = "abc";
	string s = "def";
	text t1 = t + s;
	text t2 = s + t;
	auto t2a = s + t;
	text t3 = t + "def" + s + "ghi" + t;
}

void f6()
{
	Thing g = "123";
	text t = "abc";
	string s = "def";
	Thing g1 = g + t;
	Thing g2 = t + g;
	auto t2a = t + g;
	Thing g3 = g + "def" + s + "ghi" + t;
}

int main()
{
	f1();
	f2();
	f3();
	f4();
	f5();
	f6();
	return 0;
}
