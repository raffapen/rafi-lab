
#include <string>
#include <stdio.h>

using std::string;

#if 1

class text : public string
{
	typedef string Super;
	
	string &super() { return *this; }
	const string &super() const { return *this; }

public:
	text(const char *p) : Super(p) {}
	text(const string &s) : Super(s) {}
	//text(string &&s) : Super(s) {}
	
	string &operator*() { return super(); }
	const string &operator*() const { return super(); }

	text &operator=(const string &s) { super() = s; return *this; }
	//text &&operator=(string &&s) { super() = s; return std::move(*this); }
};

#else

class text
{
	string _val;
	
	typedef string Super;
	
	string &super() { return _val; }
	const string &super() const { return _val; }

public:
	text(const char *p) : _val(p) {}
	text(const string &s) : _val(s) {}
	//text(string &&s) : _val(s) {}
	
	operator string&() { return _val; }
	operator const string&() const { return _val; }
	
	string &operator*() { return _val; }
	const string &operator*() const { return _val; }
	
	text &operator=(const string &s) { super() = s; return *this; }
	//text &&operator=(string &&s) { super() = s; return std::move(*this); }
};

inline text operator+(const text &t, const text &s) { return text(*t + *s); }

#endif

//---------------------------------------------------------------------------------------------

void f1()
{
	text p1 = "jojo";
	text t = "foo";
	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string b = p1 + string(" ");// + t + string("/sys/bin/mk.pl");
	string c = string(" ") + p1;
	string d = p1 + string(" ") + t + string("/sys/bin/mk.pl");
}

void f2()
{
#if 0
	text t("jojo");
	text p1 = t + "/a;";
#else
	text t("jojo");
	text p1(
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;");

#if 1
	text p2 =
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;";
#endif

	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string c = string(" ") + p1;
	string d = p1 + string(" ") + t + string("/sys/bin/mk.pl");
#endif
}

void f3()
{
	string s = "abc";
	string s1 = s + "def";
	string s2 = "def" + s;
	auto s2a = "def" + s;
	string s3 = s + "def" + s;
	string s4 = s + "def" + "ghi";
}

void f4()
{
	text t = "abc";
	text t1 = t + "def";
	text t2 = "def" + t;
	auto t2a = "def" + t;
	text t3 = t + "def" + t;
	text t4 = t + "def" + "ghi";
}

void f5()
{
	text t = "abc";
	string s = "def";
	text t1 = t + s;
	text t2 = s + t;
	auto t2a = s + t;
	text t3 = t + "def" + s + "ghi" + t;
	text t4 = "abc" + s + t;
}

void f6()
{
	text g("123");
	text t = "abc";
	string s = "def";
	text g1(g + t);
	text g2 = t + g;
	auto t2a = t + g;
	text g3(g + "def" + s + "ghi" + t);
}

int main()
{
	f1();
	f2();
	f3();
	f4();
	return 0;
}
