
#include <string>
#include <stdio.h>

using std::string;

class text : public string
{
	typedef string Super;
	
	string &super() { return *this; }
	const string &super() const { return *this; }

public:
	text(const char *p) : Super(p) {}
	text(const string &s) : Super(s) {}
//	text(text &&s) : Super(s) {}
//	text(const text &t) : Super(t) {}

//	text operator+(const text &t) const { return super() + t.super(); }
//	text operator+(const string &s) const { return super() + s; }
//	text operator+(const char *p) const { return super() + p; }
};

//inline text operator+(const text &t, const string &s) { return (string) t + s; }
//inline text operator+(const string &s, const text &t) { return s + (string) t; }

#if 0
#elif 1

class Thing
{
	text _val;

public:
	text &value() { return _val; }
	const text &value() const { return _val; }

public:
	Thing(const text &name) : _val(name) {}
	//Thing(text &&name) : _val(name) {}

	operator text&() { return value(); }
	operator const text&() const { return value(); }

	const text &operator*() const { return value(); }
		
	Thing &operator=(const text &s) { value() = s; return *this; }

	//Thing operator+(const text &t) const { return text(_val + t); }
	// Thing operator+(const string &s) const { return _val + s; }
	//Thing operator+(const char *p) const { return _val + p; }
};

#else

class Thing : public text
{
public:
	text &value() { return *this; }
	const text &value() const { return *this; }

public:
	Thing(const text &name) : text(name) {}
	//Thing(text &&name) : text(name) {}

//	operator text&() { return value(); }
//	operator const text&() const { return value(); }

	const text &operator*() const { return value(); }
		
	Thing &operator=(const text &s) { value() = s; return *this; }

	// Thing operator+(const text &t) const { return text(value() + t); }
};

//inline Thing operator+(const string &s, const Thing &t) { return Thing(s + *t); }
//inline Thing operator+(const char *p, const Thing &t) { return Thing(string(p) + *t); }

#endif

// inline Thing operator+(const Thing &t, const text &s) { return Thing(t.value() + s); }

void f1()
{
	text p1 = "jojo";
	text t = "foo";
	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string b = p1 + string(" ");// + t + string("/sys/bin/mk.pl");
	string c = string(" ") + p1;
	string d = p1 + string(" ") + t + string("/sys/bin/mk.pl");
}

void f2()
{
#if 0
	Thing t("jojo");
	text p1 = t + "/a;";
#else
	Thing t("jojo");
	Thing p1(
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;");

#if 0
	Thing p2 =
		t + "/a;" +
		t + "/b;" +
		t + "/c;" +
		t + "/d;";
#endif

	string a = p1 + " " + t + "/sys/bin/mk.pl";
	string c = string(" ") + p1;
	string d = p1 + string(" ") + t + string("/sys/bin/mk.pl");
#endif
}

void f3()
{
	string s = "abc";
	string s1 = s + "def";
	string s2 = "def" + s;
	auto s2a = "def" + s;
	string s3 = s + "def" + s;
	string s4 = s + "def" + "ghi";
}

void f4()
{
	text t = "abc";
	text t1 = t + "def";
	text t2 = "def" + t;
	auto t2a = "def" + t;
	text t3 = t + "def" + t;
	text t4 = t + "def" + "ghi";
}

void f5()
{
	text t = "abc";
	string s = "def";
	text t1 = t + s;
	text t2 = s + t;
	auto t2a = s + t;
	text t3 = t + "def" + s + "ghi" + t;
}

void f6()
{
	Thing g("123");
	text t = "abc";
	string s = "def";
	Thing g1(g + t);
	Thing g2 = t + g;
	auto t2a = t + g;
	Thing g3(g + "def" + s + "ghi" + t);
}

int main()
{
	f1();
	f2();
	f3();
	f4();
	return 0;
}