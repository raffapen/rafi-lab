
#ifndef _magenta_text_text_
#define _magenta_text_text_

#include <stdexcept>
#include <string>
#include <cstdarg>

//#ifdef _WIN32
#include <regex>
//#else
//#include <boost/regex.hpp>
//#endif

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <limits>

//#include "magenta/types/classes.h"
//#include "magenta/types/general.h"
//#include "magenta/types/vector.h"

namespace magenta
{
class span;
}

namespace magenta
{

//#ifdef _WIN32
#define  regex_namespace std
//#define  regex_namespace std::tr1	
//#else
//#define  regex_namespace boost	
//#endif

using std::string;
using std::numeric_limits;

///////////////////////////////////////////////////////////////////////////////////////////////

struct Null {};

const Null null = Null();

//---------------------------------------------------------------------------------------------

inline bool operator!(const string &s) { return s.empty(); }

inline const char *operator+(const string &s) { return s.c_str(); }

inline bool start_with(const string &s, const string &part) { return s.find(part) == 0; }
 
///////////////////////////////////////////////////////////////////////////////////////////////

template <class T = int>
class Range
{
public:
	T min, max;

//	static Null null = magenta::null;

	Range(T min = 0, T max = 0) : min(min), max(max) {}
	Range(Null) : min(numeric_limits<T>::max()), max(numeric_limits<T>::max()) {}

	void set(T min_, T max_)
	{
		min = min_;
		max = max_;
	}

	Range<T> &operator=(const Range<T> &r) { set(r.min, r.max); return *this; }
	Range<T> &operator=(const Null &null) { setNull(); return *this; }
	void setNull() { min = max = numeric_limits<T>::max(); }

	bool operator!() const { return min == numeric_limits<T>::max() && max == numeric_limits<T>::max(); }
	bool isIn(T n) const { return n >= min && n <= max; }
	bool isOut(T n) const { return !isIn(n); }

	T size() const { return max - min + 1; }

	Range<T> real(T size = 0) const
	{
		T from = min, to = max;
		if (size == 0)
		{
			if (from < 0 || to < 0)
				return null;
			size = from - to + 1;	
		}
		if (from < 0)
			from = size + from;
		if (to < 0)
			to = size + to;
		if (to < from)
			return null;
		return Range<T>(from, to);
	}

	magenta::span span(T size = 0) const;
};

//typedef Range<int> range;
using range = Range<int>;

//---------------------------------------------------------------------------------------------

class span
{
public:
	int at;
	int size;

	span(int at = 0, int size = 0) : at(at), size(size) {}

	void set(int at_, int size_)
	{
		at = at_;
		size = size_;
	}

	span &operator=(const span &s)
	{
		set(s.at, s.size);
		return *this;
	}

	bool operator!() const { return !size; }
	bool isIn(int k) const { return k >= at && k < at + size; }
	bool isOut(int k) const { return !isIn(k); }

	magenta::range range() const { return magenta::range(at, at + size - 1); }
};

//---------------------------------------------------------------------------------------------

template <class T>
span Range<T>::span(T size) const
{
	auto rr = real(size);
	if (!rr)
		return magenta::span();
	return magenta::span(rr.min, rr.max - rr.min + 1);
}

///////////////////////////////////////////////////////////////////////////////////////////////
 
class text : public string
{
public:
	text() {}
	text(const char *cp) : string(cp) {}
	text(const char *cp, int n) : string(cp, n) {}
	text(const string &s) : string(s) {}
	text(const text &s) : string(s) {}
//	text(long n) : string(regex_namespace::to_string()) {}
//	text(const Vector<text> &v, const text &sep = "");

	bool startwith(const text &s) const{ return find(s) == 0; }

	text ltrim() const;
	text rtrim() const;
	text trim() const { return rtrim().ltrim(); }

	long to_int(const char *what = 0) const;
	double to_num(const char *what = 0) const;

	text tolower() const;
	text toupper() const;

	bool blank() const { return empty() || trim().empty(); }
	bool operator!() const { return blank(); }

	bool ieq(const string &s) const;

	text operator[](int i) const;
	text operator()(int i, int j) const { return (*this)(range(i, j)); }
	text operator()(const range &r) const;
	text operator()(const span &s) const;
	text span(int i, int n) const;

	bool contains(const string &s) const;
	bool startswith(const string &s) const;
	bool endswith(const string &s) const;

	bool equalsto_one_of(const char *p, ...) const;
	bool startswith_one_of(const char *p, ...) const;
	bool endswith_one_of(const char *p, ...) const;

	text replace(const text &t, const text &with);

	//class Path operator~() const;

	//-----------------------------------------------------------------------------------------

	class regex
	{
	public:
		class Match;
		class Group;

		//-------------------------------------------------------------------------------------

		class Match : protected regex_namespace::smatch
		{
			friend class text;
			typedef regex_namespace::smatch Super;

			const Super &super() const { return *this; }

		public:
			Match() {}
			Match(const Super &m) : Super(m) {}

			bool operator!() const { return super().size() == 0; }

			text str() const { return super().str(); }
			text prefix() const { return (const string &) super().prefix(); }
			text suffix() const { return (const string &) super().suffix(); }
			int at() const { return super().position(); }

			text operator*() const { return ngroups() == 0 ? str() : group(1); }
			const Match *operator->() const { return this; }

			Group group(int i) const { return super()[i]; }
			Group operator[](int i) { return group(i); }
			int ngroups() const { return super().size() - 1; }
		};

		//-------------------------------------------------------------------------------------

		class Group : protected regex_namespace::sub_match<string::const_iterator>
		{
			friend class text;
			typedef regex_namespace::sub_match<string::const_iterator> Super;

			const Super &super() const { return *this; }

		public:
			Group(const Super &g) : Super(g) {}

			operator string() const { return super().str(); }
			operator text() const { return super().str(); }

			text str() const { return super().str(); }
			text operator*() const { return str(); }

			//int at() const { return super().}
		};

		//-------------------------------------------------------------------------------------

		class iterator : protected regex_namespace::sregex_iterator
		{
			friend class text;
			typedef regex_namespace::sregex_iterator Super;

			regex_namespace::regex *_re;

			static regex_namespace::sregex_iterator _end;

			Super &super() { return *this; }
			const Super &super() const { return *this; }

			Super _la;
			const string &_str;

		protected:
			void ctor()
			{
				_la = super();
				if (_la != _end)
					++_la;
			}

			iterator(const text &text, regex_namespace::regex *re) : Super(text.begin(), text.end(), *re), _str(text), _re(re)
			{
				ctor();
			}

		public:
			iterator(const text &text, regex_namespace::regex &re) : Super(text.begin(), text.end(), re), _str(text), _re(0)
			{
				ctor();
			}

			~iterator();

			bool operator!() const { return *this == _end; }

			Match match() const
			{
				if (!*this)
					return Match();
				return Match(*super());
			}

			const Match operator->() const { return match(); }

			text prefix() const;
			text full_prefix() const;
			text suffix() const;
			text full_suffix();

			int ngroups() const { return match().ngroups(); }
			Group group(int i) const { return match().group(i); }
			Group operator[](int i) const { return group(i); }
			// int at(int i) const { return group(i).at(); }

			text operator*() const { return group(1).str(); }

			iterator &operator++() { next(); return *this; }
			iterator operator++(int) { auto i = *this; next(); return i; }

			bool is_last() const { return !*this || _la == _end; }

		protected:
			void next()
			{
				++super();
				if (_la != _end)
					++_la;
			}
		};

	};

//	text(const regex::match &m) : string(m.str()) {}
//	text(const regex_namespace::ssub_match &m) : string(m.str()) {}

//	text(const Match &m) : string(m.str()) {}
//	text(const regex_namespace::ssub_match &m) : string(m.str()) {}

//	bool search(const char *re, regex::match &match) const;

	bool match(const char *re, regex::Match &match) const;
	regex::Match match(const char *re) const;
	regex::iterator scan(const char *re) const;
	regex::iterator matches(const char *re) const { return scan(re); }

	enum class split_type
	{
		open,
		close
	};

//	Vector<text> split(const char *re, split_type type = split_type::open) const;
//	Vector<text> close_split(const char *re) const;
};

///////////////////////////////////////////////////////////////////////////////////////////////

#if 0

class Strings : public Vector<text>
{
public:
	Strings(const Vector<string> &ss);
	Strings(const Vector<text> &ss);
	Strings(const Vector<char*> &ss);
	Strings(const text &s, const text &sep);

	text join(const text &sep) const;// { return join(*this, sep); }
};

#endif

///////////////////////////////////////////////////////////////////////////////////////////////

//text join(const Vector<text> &strs, const text &sep);

// inline text operator+(const text &t, const text &s) { return (string) t + (string) s; }

//inline text operator+(const text &s, const char *p); // { return std::operator+(+s, p); }
//inline text operator+(const text &s, const text &t); // { return std::operator+(+s, p); }
// inline text operator+(const char *p, const text &s); // { return std::operator+(p, +s); }


///////////////////////////////////////////////////////////////////////////////////////////////

} // namespace magenta

#endif // _magenta_text_text_
