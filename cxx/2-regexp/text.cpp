
#include "text.h"
//#include "text/string.h"

//#include "types/defs.h"
//#include "files/path.h"

namespace magenta
{

using namespace std;

///////////////////////////////////////////////////////////////////////////////////////////////

#if 0
Path text::operator~() const
{
	return *Path(*this);
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////

text text::operator[](int i) const
{
	return span(i, 1);
}

//---------------------------------------------------------------------------------------------

text text::operator()(const range &r) const
{
	auto s = r.span(length());
	if (!s)
		return "";
	return substr(s.at, s.size);
}

//---------------------------------------------------------------------------------------------

text text::operator()(const magenta::span &s) const
{
	return (*this)(s.range());
}

//---------------------------------------------------------------------------------------------

text text::span(int i, int n) const
{
	return (*this)(magenta::span(i, n));
}

///////////////////////////////////////////////////////////////////////////////////////////////

text text::ltrim() const
{
	string s = *this;
	s.erase(s.begin(), find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

//---------------------------------------------------------------------------------------------

text text::rtrim() const
{
	string s = *this;
	s.erase(find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

///////////////////////////////////////////////////////////////////////////////////////////////

long text::to_int(const char *what) const
{
	long n;
	int k = sscanf(c_str(), "%ld", &n);
#if 0
	if (!k)
		if (what)
			throw std::invalid_argument(stringf("%s: cannot convert '%s' to integer", what, c_str()));
		else
			throw std::invalid_argument(stringf("cannot convert '%s' to integer", c_str()));
#endif
	return n;
}

//---------------------------------------------------------------------------------------------

double text::to_num(const char *what) const
{
	double n;
	int k = sscanf(c_str(), "%lf", &n);
#if 0
	if (!k)
	{
		if (what)
			throw std::invalid_argument(stringf("%s: cannot convert '%s' to number", what, c_str()));
		else
			throw std::invalid_argument(stringf("cannot convert '%s' to number", c_str()));
	}
#endif
	return n;
}

///////////////////////////////////////////////////////////////////////////////////////////////

bool text::ieq(const string &s) const
{
	struct icmp
	{
		bool operator()(int a, int b) const { return std::toupper(a) == std::toupper(b); }
	};

	return std::equal(begin(), end(), s.begin(), s.end(), icmp());
}

//---------------------------------------------------------------------------------------------

text text::tolower() const
{
	text t = *this;
	int n = t.length();
	for (int i = 0; i < n; ++i)
	{
		char &c = t.at(i);
		c = ::tolower(c);
	}
	return t; 
}

//---------------------------------------------------------------------------------------------

text text::toupper() const
{
	text t = *this;
	int n = t.length();
	for (int i = 0; i < n; ++i)
	{
		char &c = t.at(i);
		c = ::toupper(c);
	}
	return t; 
}

///////////////////////////////////////////////////////////////////////////////////////////////

bool text::contains(const string &s) const
{
	return find(s) != string::npos;
}

//---------------------------------------------------------------------------------------------

bool text::startswith(const string &s) const
{
	return find(s) == 0;
}

//---------------------------------------------------------------------------------------------

bool text::endswith(const string &s) const
{
	size_t i = rfind(s);
	return i != string::npos && i == length() - s.length();
}

//---------------------------------------------------------------------------------------------

bool text::equalsto_one_of(const char *p, ...) const
{
	bool rc = false;
	va_list args;
	va_start(args, p);
	for (;;)
	{
		if (!p)
			break;
		if (*this == p)
		{
			rc = true;
			break;
		}
		p = va_arg(args, char*);
	}
	va_end(args);
	return rc;
}

//---------------------------------------------------------------------------------------------

bool text::startswith_one_of(const char *p, ...) const
{
	bool rc = false;
	va_list args;
	va_start(args, p);
	for (;;)
	{
		if (!p)
			break;
		if (startswith(p))
		{
			rc = true;
			break;
		}
		p = va_arg(args, char*);
	}
	va_end(args);
	return rc;
}

//---------------------------------------------------------------------------------------------

bool text::endswith_one_of(const char *p, ...) const
{
	bool rc = false;
	va_list args;
	va_start(args, p);
	for (;;)
	{
		if (!p)
			break;
		if (startswith(p))
		{
			rc = true;
			break;
		}
		p = va_arg(args, char*);
	}

	va_end(args);
	return rc;
}

///////////////////////////////////////////////////////////////////////////////////////////////

text text::replace(const text &t, const text &with)
{
	string s = *this;
	size_t n_t = t.length(), n_w = with.length();
	for (size_t i = 0; (i = s.find(t, i)) != string::npos; i += n_w)
		s.replace(i, n_t, +with);
	return s;
}

///////////////////////////////////////////////////////////////////////////////////////////////

#if 0
bool text::search(const char *re_p, regex_namespace::smatch &match) const
{
	regex_namespace::regex re(re_p);
	bool b = regex_search(*this, match, re);
	return b;
}
#endif

//---------------------------------------------------------------------------------------------

bool text::match(const char *re_p, text::regex::Match &match) const
{
	text re_s(re_p);
	re_s += ".*";
	regex_namespace::regex re(re_s);
	bool b = regex_match(begin(), end(), match, re);//, regex_constants::match_not_eol);
	return b;
}

//---------------------------------------------------------------------------------------------

text::regex::Match text::match(const char *re_p) const
{
	text::regex::Match match;

	text re_s(re_p);
	re_s += ".*";
	regex_namespace::regex re(re_s);
	bool b = regex_match(begin(), end(), match, re);//, regex_constants::match_not_eol);
	return match;
}

//---------------------------------------------------------------------------------------------

text::regex::iterator text::scan(const char *re) const
{
	regex_namespace::regex *pre = new regex_namespace::regex(re);
	return text::regex::iterator(*this, pre);
}

//---------------------------------------------------------------------------------------------
#if 0
Vector<text> text::split(const char *re_s, split_type type) const
{
	Vector<text> v;
	regex_namespace::regex re(re_s);
	regex::iterator i(*this, re);
	if (!!i)
		if (type == split_type::close || !!i.prefix())
			v << i.prefix();

	for (; !i.is_last(); ++i)
	{
		auto s = i.suffix();
		v << s;
	}
	auto p = i.prefix();
	auto s = i.suffix();
	if (type == split_type::close || !!i.suffix())
		v << s;

	return v;
}

//---------------------------------------------------------------------------------------------

Vector<text> text::close_split(const char *re) const
{
	return split(re, split_type::close);
}
#endif
///////////////////////////////////////////////////////////////////////////////////////////////

regex_namespace::sregex_iterator text::regex::iterator::_end;

//---------------------------------------------------------------------------------------------

text::regex::iterator::~iterator()
{
	delete _re;
}

//---------------------------------------------------------------------------------------------

text text::regex::iterator::prefix() const
{
	return match().prefix();
}

//---------------------------------------------------------------------------------------------

text text::regex::iterator::full_prefix() const
{
	return string(_str.begin(), _str.begin() + match().at());
}

//---------------------------------------------------------------------------------------------

text text::regex::iterator::suffix() const
{
	if (_la != _end)
		return Match(*_la).prefix();
	return match().suffix();
}

//---------------------------------------------------------------------------------------------

text text::regex::iterator::full_suffix()
{
	return match().suffix();
}


///////////////////////////////////////////////////////////////////////////////////////////////
#if 0
text join(const Vector<text> &strs, const text &sep)
{
	if (!strs)
		return "";
	text s = strs[0];
	foreach (t, strs.begin()+1)
		s += sep + *t;
	return s;
}
#endif
///////////////////////////////////////////////////////////////////////////////////////////////

} // namespace magenta
