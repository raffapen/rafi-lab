#!/usr/bin/env ruby

require 'Bento'
require 'open3'

fout = 'fout'
ferr = 'ferr'
frc = 'frc'
prog = 'ls /'

bashcmd = <<~END
	_exit() { echo $? > #{frc}; }
	trap _exit EXIT
	eval #{prog}
	END

out, pstat = Open3.capture2e('bash', '-c', <<~END)
	{ nohup /usr/bin/bash -c '#{bashcmd}' </dev/null 1> #{fout} 2> #{ferr} & }
	jobs -p
	disown
	END

bb
puts "fin"
