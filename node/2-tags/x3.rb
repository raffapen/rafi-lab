#!ruby

require 'Machina'

q = ARGV[0]

def fix_tags(t)
	t.gsub(/[-.]/,"_")
end

class TagQuery

	@@defaults_t = <<~END
		<% for c in query_tags_commands %>
		var <%= c %>;
		<% end %>

		END

	@@func_t = <<~END
		function <%= name %>()
		{
		<% for c in obj_tags_commands %>
			var <%= c %>;
		<% end %>

			return <%= @query %>;
		}

		if (<%= name %>()) console.log('<%= name %>');

		END

	def initialize(objects, query)
		@query = fix_tags(query)

		query_tags = @query.scan(/[_A-Za-z]\w+/).uniq
		query_tags_commands = query_tags.map { |t| "#{t} = 0" }

		js = Bento.mold(@@defaults_t)
		objects.each do |name, tags|
			tags = Set[*tags].to_a
			obj_tags_commands = tags.map{|n,v| "#{n} = #{v}"}

			func = Bento.mold(@@func_t)
			js << func
		end

		# puts js
		
		# we rely on not having a " inside funcs
		# otherwise a temp file should be created
		# @names = `node -e "#{js.gsub(/\n/, "")}"`.lines.map { |x| x.strip }
		
		cmd = <<~END
			node -e "base64 = require('base-64'); eval(base64.decode(process.argv[1]))" "#{Base64.strict_encode64(js)}"
			END
		@names = `#{cmd}`.lines.map { |x| x.strip }
	end
	
	attr_reader :names
end

db = Machina::DB.db["select id,name,json from machines"]
h_m = db.reduce({}) { |h, m| h[m["id"]] = m["name"]; h }

h_t = db.reduce({}) do |h, m|
	id = m["id"]
	tags = Bento::JSON.from_json(m["json"]).to_h.tags
	tk = tags.map{|t| fix_tags(t)}
	h["m_#{id}"] = Bento.keyhash(tk)
	h
end

bb
tq = TagQuery.new(h_t, q)
m = tq.names.map{|n| h_m[n[2..-1].to_i]}
bb
puts m
