#!ruby

require 'Bento'
# require 'json'
# require 'ERB'

class TagQuery

	@@defaults_t = <<~END
		<% for c in query_tags_commands %>
		var <%= c %>;
		<% end %>

		END

	@@func_t = <<~END
		function <%= name %>()
		{
		<% for c in obj_tags_commands %>
			var <%= c %>;
		<% end %>

			return <%= query %>;
		}

		if (<%= name %>()) console.log('<%= name %>');

		END

	def initialize(objects_json, query)
		@query = query

		objects = JSON.parse(objects_json)

		query_tags = @query.scan(/[_A-Za-z]\w+/).uniq
		query_tags_commands = query_tags.map { |t| "#{t} = 0" }

		defaults_erb = ERB.new(@@defaults_t, 0, ">")
		func_erb = ERB.new(@@func_t, 0, ">")

		funcs = defaults_erb.result(binding)
		for obj in objects do
			name = obj.keys[0]
			tags = obj.values[0]
		
			obj_tags_commands = tags.map { |t| t.is_a?(String) ? "#{t} = 1" : "#{t.keys[0]} = #{t.values[0]}" }

			func = func_erb.result(binding)
			funcs << func
		end

		puts funcs
		# we rely on not having a " inside funcs
		# otherwise a temp file should be created
		@names = `node -e "#{funcs.gsub(/\n/, "")}"`.lines.map { |x| x.strip }
	end
	
	attr_reader :names
end

objects = <<END
[
	{ "test1": ["t1", "t3", { "a1" : 10 }] },
	{ "test2": ["t3", { "a2" : 1 }] }
]
END

names2 = <<END
	name1: t1, t3, a1 = 10
	name2: t3, a2 = 1
END

names3 = <<END
	(name1 :t1 :t2 :a1 10)
	name1: t1, t3, a1 = 10
	name2: t3, a2 = 1
END

bb
# query = "(t1 || a1 > 1) && (!t2 || t3)"
query = ARGV[0]
# puts objects

tq = TagQuery.new(objects, query)
puts tq.names
