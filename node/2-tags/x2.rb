#!ruby

require 'Bento'

class TagQuery

	@@defaults_t = <<~END
		<% for c in query_tags_commands %>
		var <%= c %>;
		<% end %>

		END

	@@func_t = <<~END
		function <%= name %>()
		{
		<% for c in obj_tags_commands %>
			var <%= c %>;
		<% end %>

			return <%= query %>;
		}

		if (<%= name %>()) console.log('<%= name %>');

		END

	def initialize(objects, query)
		@query = query

		query_tags = @query.scan(/[_A-Za-z]\w+/).uniq
		query_tags_commands = query_tags.map { |t| "#{t} = 0" }

		js = Bento.mold(@@defaults_t)
		bb
		objects.each do |name, tags|
			obj_tags_commands = tags.map{|n,v| "#{n} = #{v}"}

			func = Bento.mold(@@func_t)
			js << func
		end

		puts js
		# we rely on not having a " inside js
		# otherwise a temp file should be created
		@names = `node -e "#{js.gsub(/\n/, "")}"`.lines.map { |x| x.strip }
	end
	
	attr_reader :names
end

objects = {
	m1: { t1: 1, t3: 1, a1: 10 },
	m2: { t3: 1, a2: 1 }
}


bb
# query = "(t1 || a1 > 1) && (!t2 || t3)"
query = ARGV[0]
# puts objects

tq = TagQuery.new(objects, query)
puts tq.names
