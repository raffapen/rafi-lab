
use strict;
use File::Basename;

my $dep_file;
my $src_file;
my $obj_file;
my $pp_file;

while (@ARGV)
{
	my $a = $ARGV[0];
	if ($a eq '--dep')
	{
		shift;
		$dep_file = shift;
	}
	elsif ($a eq '--src')
	{
		shift;
		$src_file = shift;
	}
	elsif ($a eq '--obj')
	{
		shift;
		$obj_file = shift;
	}
	elsif ($a eq '--pp')
	{
		shift;
		$pp_file = shift;
	}
	elsif ($a eq '-h' || $a eq '-help')
	{
		usage();
		exit(1);
	}
	else
	{
		last;
	}
}

if (!$src_file || !$obj_file || !$pp_file)
{
	usage();
	die "missing arguments\n";
}

make_dep_file($src_file, $obj_file, $pp_file, $dep_file);
exit 0;

sub make_dep_file
{
	my ($src_file, $obj_file, $pp_file, $dep_file) = @_;

	$dep_file = "$obj_file.d";
	my $dep_file = dirname($obj_file) . "/" . basename($src_file) . ".d" if !$dep_file;

	my %hash = {};
	my @files;

	open PFILE, "<$pp_file" or die "cannot open $pp_file\n";
	foreach (<PFILE>)
	{
		next if !(/^#line \d+ (.*)/);
		my $f =$1;
		$f =~ s/\\\\/\//g;
		$f =~ s/\"//g if $f =~ /\w/;
		$f = trim($f);
		next if exists $hash{$f};
		$hash{$f} = 1;
		push @files, $f;
	}
	close PFILE;

	shift @files; # shift source file out of @files

	if ($dep_file)
	{
		open DFILE, ">$dep_file" or die "cannot create $dep_file\n";
	}
	else
	{
		*DFILE = STDOUT;
	}

	print DFILE "$obj_file: $src_file";
	foreach my $f (@files)
	{
		print DFILE " \\\n\t$f";
	}
	print DFILE "\n\n";

	foreach my $f (@files)
	{
		print DFILE "$f:\n\n";
	}

	close DFILE if $dep_file;
}

sub trim
{
	my $s = shift;
	$s =~ s/^\s+//;
	$s =~ s/\s+$//;
	return $s;
}

sub usage
{
	print "usage: cldep.pl [-n] --pp pp-file --src src-file --obj obj-file [--dep dep-file]\n";
	print "       cldep.pl [-h]\n";
}
