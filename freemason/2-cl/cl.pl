
use strict;
use File::Basename;
use File::Temp qw(tmpnam);

my $pp_file;
my $keep_pp = 0;
my $show_commands = 0;
my $nop = 0;

while (@ARGV)
{
	my $a = $ARGV[0];
	if ($a eq '--show-commands')
	{
		shift;
		$show_commands = 1;
	}
	elsif ($a eq '--pp-file')
	{
		shift;
		$pp_file = shift;
	}
	elsif ($a eq '--keep-pp')
	{
		shift;
		$keep_pp = 1;
	}
	elsif ($a eq '-n')
	{
		shift;
		$show_commands = 1;
		$nop = 1;
	}
	elsif ($a eq '-h' || $a eq '--help')
	{
		usage();
		exit(1);
	}
	else
	{
		last;
	}
}

my $obj_file;
my @opt;
my @pp_opt;

my $cl_exe = shift;
foreach (@ARGV)
{
	if (/^\/(.*)/)
	{
		$_ = "-$1";
		print "$_\n";
	}
	if (/^-D/ || /^-I/)
	{
		push @pp_opt, $_;
	}
	elsif (/^-/)
	{
		push @opt, $_;
	}
	if (/^-Fo(.*)/) # obj file
	{
		$obj_file = $1;
	}
}

my $opt = join(" ", @opt);
my $pp_opt = join(" ", @pp_opt);

my $src_file = @ARGV[-1];
$src_file =~ /^.*(\.\w+)$/;
my $ext = $1;

if ($keep_pp)
{
	$obj_file =~ /^(.*)\.\w+$/;
	$pp_file = "$1.i$ext";
}
my $pp_dir;
if (!$pp_file)
{
	$pp_dir = tmpnam();
	mkdir($pp_dir);
	$pp_file = "$pp_dir/" . basename($src_file);
}

my $c1 = "$cl_exe $opt $pp_opt -P -Fi$pp_file $src_file";
my $c2 = "$cl_exe $opt -u $pp_file";

print "$c1\n" if $show_commands;
if (!$nop)
{
	system($c1);
	my $rc1 = $? == -1 ? -1 : ($? >> 8);
	exit($rc1) if $rc1;
}

print "$c2\n" if $show_commands;
if (!$nop)
{
	system($c2);
	my $rc2 = $? == -1 ? -1 : ($? >> 8);
	exit($rc2) if $rc2;
}

print "cldep.pl --pp $pp_file --src $src_file --obj $obj_file\n" if $show_commands;
make_dep_file($src_file, $obj_file, $pp_file) if !$nop;

exit(0);

sub END
{
	if ($pp_dir)
	{
		unlink($pp_file);
		rmdir($pp_dir);
	}
}

sub make_dep_file
{
	my ($src_file, $obj_file, $pp_file) = @_;

	my $dep_file = dirname($obj_file) . "/" . basename($src_file) . ".d";

	my %hash = {};
	my @files;

	open PFILE, "<$pp_file" or die "cannot open $pp_file\n";
	foreach (<PFILE>)
	{
		next if !(/^#line \d+ (.*)/);
		my $f =$1;
		$f =~ s/\\\\/\//g;
		$f =~ s/\"//g if $f =~ /\w/;
		$f = trim($f);
		next if exists $hash{$f};
		$hash{$f} = 1;
		push @files, $f;
	}
	close PFILE;

	shift @files; # shift source file out of @files

	open DFILE, ">$dep_file" or die "cannot create $dep_file\n";

	print DFILE "$obj_file: $src_file";
	foreach my $f (@files)
	{
		print DFILE " \\\n\t$f";
	}
	print DFILE "\n\n";

	foreach my $f (@files)
	{
		print DFILE "$f:\n\n";
	}

	close DFILE;
}

sub trim
{
	my $s = shift;
	$s =~ s/^\s+//;
	$s =~ s/\s+$//;
	return $s;
}

sub usage
{
	print "usage: cl.pl [-n] [--pp-file FILE] [--keep-pp] [--show-commands] CL_EXE [options] <source-file>\n";
	print "       cl.pl [-h|--help]\n";
}
