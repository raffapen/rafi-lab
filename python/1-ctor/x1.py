import pdb; bb=pdb.set_trace

"""
def constructor(*args, **kwargs):
	def decorator(f):
		pass
	bb()
	return decorator
	

class Foo():
	def __init__(self): pass
	def __init__(self):
			raise NotImplementedError()

	def __new__(cls):
		bare_instance = object.__new__(cls)
		# you may want to have some common initialisation code here
		return bare_instance

	@classmethod
	def from_whatever(cls, arg):
		instance = cls.__new__(cls)
		instance.arg = arg
		return instance

	@constructor
	def create(self, arg1, arg2):
		pass
"""

class Foo(object):

#	@classmethod
#	def __new1__(cls):
#		return cls.__new__(cls)
		
#	def constructor(func):
#		def ctor(*args, **kwargs):
#			self = Foo.__new1__(cls)
#			func(*args, **kwargs)
#			return self
#		return ctor

	def __init__(self):
		raise Exception("cannot create from thin air")

	@classmethod
	def create(cls, arg1, arg2):
		self = cls.__new__(cls)
		self._create(arg1, arg2)
		return self
		
	@constructor
	def _create(self, arg1, arg2):
		self.arg1 = arg1
		self.arg2 = arg2
		pass
	

"""
class Foo1(metaclass=Meta)
	@constructor
	def is(self, id) => __init__(self, id)
		self.id = id

	@constructor
	def from_file(self, file)
		self.arg1 = file
		self.arg2 = file
"""
		
bb()
# foo1 = Foo(1)
foo2 = Foo.create(1, 2)
# foo2 = Foo.create1(1, 2)
print("fin")
