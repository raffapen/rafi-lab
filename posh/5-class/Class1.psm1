
class Class1
{
	[String] $name
	
	Class1([string] $name)
	{
		$this.name = $name
	}
	
	[String] Foo([String] $arg1, [String] $arg2, [Switch] $maybe)
	{
		return "$($this.name) '$arg1' '$arg2' $maybe"
	}

	[String] Bar([Object] $args)
	{
		return "$($this.name) '$($args.arg1)' '$($args.arg2)'"
	}
}

