# ($s = get-pssession -name vsphere -computer localhost -WarningAction SilentlyContinue) | out-null
# if ($s -eq $null)
# {
# 	& { .\new-pss.ps1 }
# 	($s = get-pssession -name vsphere -computer localhost -WarningAction SilentlyContinue) | out-null
# 	if ($s -eq $null)
# 	{
# 		throw "cannot create vsphere pssession"
# 	}
# }
if ($DefaultVIServer -ne $null)
{
	return $DefaultVIServer
}

write-host "asking for vis"
$vis = & { .\get-vis.ps1 }
# connect-pssession -session $s | out-null
# $vis = invoke-command -session $s -ScriptBlock {$sessionid} | out-null
if ($vis -eq $null)
{
	throw "cannot establish vsphere session"
}
write-host "got vis"
# disconnect-pssession -session $s -WarningAction SilentlyContinue | out-null
($s = connect-viserver -server vcenter -session $vis -ErrorAction SilentlyContinue) | out-null
if ($DefaultVIServer -eq $null)
{
	write-host "new pss"
	& { .\new-pss.ps1 }
	$vis = & { .\get-vis.ps1 }
	connect-viserver -server vcenter -session $vis #| out-null
}

write-host "connected to vserver"
return $DefaultVIServer
