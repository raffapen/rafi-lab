while ($true)
{
	try
	{
		disconnect-viserver -server vcenter -confirm:$false | out-null
	}
	catch
	{
		break
	}
}
	
remove-pssession -session (get-pssession -name vsphere -computer localhost) -ErrorAction SilentlyContinue | out-null
