
if ($DefaultVIServer -ne $null)
{
	write "no default viserver"
	($v = get-datacenter -ErrorAction SilentlyContinue) | out-null
	if ($v -ne $null) { return $DefaultVIServer }
}

write-host "asking for vis"
$vis = & { .\get-vis.ps1 }
if ($vis -eq $null)
{
	throw "cannot establish vsphere session"
}
write-host "got vis: $vis"
write-host "connecting viserver..."
$t = measure-command { $v = connect-viserver -server vcenter -session $vis -ErrorAction SilentlyContinue | out-null }
write-host "done ($t)."
if ($DefaultVIServer -eq $null -or $v -eq $null)
{
	write-host "new pss"
	& { .\new-pss.ps1 }
	$vis = & { .\get-vis.ps1 }
	connect-viserver -server vcenter -session $vis | out-null
}

write-host "connected to vserver"
return $DefaultVIServer
