write-host "asking for session"
($s = get-pssession -name vsphere -computer localhost -WarningAction SilentlyContinue) | out-null
if ($s -eq $null)
{
	write-host "need new session"
	& .\new-pss.ps1
	($s = get-pssession -name vsphere -computer localhost -WarningAction SilentlyContinue) | out-null
	if ($s -eq $null)
	{
		throw "cannot create vsphere pssession"
	}
}
connect-pssession -session $s  | out-null
write-host "connected"
enter-pssession -session (get-pssession -name vsphere -computer localhost)
disconnect-pssession -session $s -WarningAction SilentlyContinue  | out-null
write-host "disconnected"
