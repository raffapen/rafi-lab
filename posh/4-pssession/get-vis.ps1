
($s = get-pssession -name vsphere -computer localhost -ErrorAction SilentlyContinue) | out-null
if ($s -eq $null)
{
	& { .\new-pss.ps1 }
	($s = get-pssession -name vsphere -computer localhost) | out-null
	if ($s -eq $null)
	{
		throw "cannot create vsphere pssession"
	}
}
$i = 0
while ($i -lt 10)
{
	try
	{
		write-host "round $i"
		connect-pssession -session $s -ErrorAction Stop | out-null
		write-host "  connected pssession"
		($vis = invoke-command -session $s -ScriptBlock {$sessionid} -ErrorAction Stop)  | out-null
		write-host "  got sessionid: $vis"
		disconnect-pssession -session $s -WarningAction SilentlyContinue | out-null
		write-host "  disconnected pssession"
		if ($vis -ne $null)
		{
			write-output $vis
			# return $vis
			return
		}
		else
		{
			remove-pssession -session $s -ErrorAction SilentlyContinue | out-null
			& { .\new-pss.ps1 }
		}
	}
	catch
	{
		$t = get-random -min 100 -max 500
		Start-Sleep -milliseconds $t
	}
	++$i
	
	return $null
}
