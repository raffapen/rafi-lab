
$s1 = get-pssession -name vsphere -computer localhost -ErrorAction SilentlyContinue
if ($s1 -ne $null)
{
	remove-pssession -session $s1 -ErrorAction SilentlyContinue | out-null 
}

$o1 = @{
	IdleTimeout = -1
	NoMachineProfile = $true
	SkipCACheck = $true
	SkipCNCheck = $true
	SkipRevocationCheck = $true
	}
$options = New-PSSessionOption @o1

$o2 = @{
	ComputerName = "localhost"
	EnableNetworkAccess = $true
	Name = "vsphere"
	SessionOption = $options
#	Credential = $Credential
	}
$pss = New-PSSession @o2

Invoke-Command -session $pss -ScriptBlock {
	$epref = $ErrorActionPreference
	$wpref = $WarningPreference

	write-host "mapping drives"
	$net = new-object -ComObject WScript.Network
	$ErrorActionPreference = 'SilentlyContinue'
	$net.MapNetworkDrive("x:", "\\devito1\devito\vue\devito1\root", $false, "devito1\root", "root") | Out-Null
	$net.MapNetworkDrive("v:", "\\devito1\devito\vue", $false, "devito1\root", "root") | Out-Null
	$ErrorActionPreference = $epref
	
	write-host "importing classes..."
	$env:PSModulePath = "x:\classico\posh;x:\classico\dotnet\packages;" + $env:PSModulePath
	$t = measure-command { import-module Classico.Posh -DisableNameChecking | Out-Null }
	write-host "done ($t)."
	
	write-host "connecting viserver..."
	$WarningPreference = 'SilentlyContinue'
	$ErrorActionPreference = 'SilentlyContinue'
	$t = measure-command { ($s = connect-viserver -server vcenter -user root -password Taligent) | Out-Null }
	$ErrorActionPreference = $epref
	$WarningPreference = $wpref
	write-host "connected ($t)."

	$sessionid = ($s | select -exp sessionid)
	write-host "sessionid is $sessionid"
	return $sessionid
}

Disconnect-PSSession -Session $pss -Confirm:$false -WarningAction SilentlyContinue | Out-Null
