
param(
[parameter(Mandatory=$true)]
[ValidateNotNullOrEmpty()]
[string] $Name
) 

if ((get-vm $name).PowerState -ne "PoweredOn")
{
	start-vm $name | out-null
}

$v = get-vm $name
while ($v.PowerState -ne "PoweredOn")
{
	start-sleep -m 500
	$v = get-vm $name
}

$g = get-vmguest $name
while ($v.PowerState -eq "PoweredOn" -and $g.State -ne "Running")
{
	start-sleep -m 500
	$v = get-vm $name
	$g = get-vmguest $name
}

$g = get-vmguest $name
while ($v.PowerState -eq "PoweredOn" -and $g.State -eq "Running" -and $g.IPAddress.Count -le 1)
{
	start-sleep -m 500
	$v = get-vm $name
	$g = get-vmguest $name
}

write-warning ("PowerState: " + $v.PowerState)
write-warning ("State: " + $g.State)
write-warning ("IPAddress: " + $g.IPAddress.Count)
write-output $g.IPAddress[0]
