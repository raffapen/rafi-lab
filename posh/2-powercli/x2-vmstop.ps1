
$name = "rafi_win7-1"

if ((get-vm $name).PowerState -ne "PoweredOn")
{
	return
}

stop-vmguest $name -Confirm:$false | out-null

$v = get-vm $name
while ($v.PowerState -ne "PoweredOff")
{
	start-sleep -m 500
	$v = get-vm $name
}
