
function get-folderx($name)
{
	if ("/","\" -contains $name[0])
	{
		$f = get-folder -NoRecursion
		$f = get-folder "vm" -location $f
		$parts = $name -split "/" -split "\\" | where { $_ -ne "" } | select -skip 1
		foreach ($e in $parts)
		{
			$f = get-folder $e -location $f
		}
		$id = $f | select -exp id
	}
	else
	{
		$id = Get-Folder -id $name | select -exp id
	}
	$id
}

function get-foldery($name)
{
	$f = get-folder -id $name

	$a = @()
	while ($f -ne $null)
	{
		$a = ,$f.name + $a
		$p = $f.parentid
		$f = $null
		$f = get-folder -id $p -ErrorAction SilentlyContinue
	}
	$name = "/" + (($a | select -skip 1) -join "/")
	$name
}

function try1($text)
{
	try
	{
		try
		{
			# throw "error1"
			ls jojo -ErrorAction stop
		}
		catch
		{
			write-host $_.Exception.ScriptStackTrace
			write-host $_.Exception.Message
			throw [System.Exception]::new('try1 failed', $_.Exception) #$PSItem.Exception)
		}
	}
	catch
	{
		write-host $_.Exception.ScriptStackTrace
		write-host $_.Exception.InnerException.Message
		write-host $_.Exception.Message
	}
}
