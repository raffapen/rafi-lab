function get-snapshots($vm)
{
	function snapshots($snaps, $parent = "")
	{
		$ss = @()
		foreach ($x in $snaps)
		{
			$s = @{Name=$x.Name; Time=$(get-date -date $x.CreateTime -format s); Desc=$x.Description; Parent=$parent}
			$ss += $s
			if ($x.ChildSnapshotList -ne $null)
			{
				$ss += snapshots -snaps $x.ChildSnapshotList -parent $x.Name
			}
		}
		$ss
	}
	
	if ($vm.ExtensionData.Snapshot -eq $null)
	{
		return @()
	}

	$h = snapshots -snaps $vm.ExtensionData.Snapshot.RootSnapshotList -parent ""
#	return $h
	# $h | select Name,Time,Desc,Parent | sort Time -desc
	$h1 = h2o($h) | select Name,Time,Desc,Parent | sort Time -Desc
	return $h1
}

function get-snapshots1($snaps, $parent)
{
	$ss = @()
	foreach ($x in $snaps)
	{
		$s = @{Name=$x.Name; Desc=$x.Description; Time=$x.CreateTime; Parent=$parent}
		$ss += $s
		if ($x.ChildSnapshotList -ne $null)
		{
			$ss += snapshots -snaps $x.ChildSnapshotList -parent $x.Name
		}
	}
	$ss
}

<#
function hash-to-obj($hash, $select = $null)
{
	# Param([parameter(ValueFromPipeline)] [Hashtable] $hash, $select = $null)
	
	$h = $hash.foreach({[PSCustomObject]$_})
	if ($select -ne $null)
	{
		$h = $h | select $select
	}
	$h
}

function typename($x, $is = $null)
{
	#Param([AllowNull()] $x, $is = $null)

	if ($x -eq $null)
	{
		if ($is -eq $null)
		{
			return $null;
		}
		else
		{
			return $false;
		}
	}
	if ($is -eq $null) 
	{
		$x.GetType().Name
	}
	else
	{
		$x.GetType().Name -eq $is
	}
}
#>
