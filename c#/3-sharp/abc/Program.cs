﻿using System;
using System.Management.Automation;
using System.Collections.ObjectModel;
//using VMware.Vim;

class PS
{
	public PS()
	{
	}
}

namespace abc
{
	class PoshCommand : IDisposable
	{
		PowerShell _ps = PowerShell.Create();

		public class Result
		{
			public string output = "", errors = "";
			public int result;
		}
		
		public Result run()
		{
			var r = new Result();
			r.result = 0;

			try
			{
				Collection<PSObject> oo = _ps.Invoke();
				foreach (PSObject o in oo)
				{
					if (o == null)
						continue;
					r.output += o + "\n"; 
				}
				if (_ps.Streams.Error.Count > 0)
				{
					foreach (var e in _ps.Streams.Error)
						r.errors += e + "\n";
				}
			}
			catch (Exception x)
			{
				r.result = 1;
				r.errors += x.ToString() + "\n";
			}

			return r;
		}
  
		public void add(string cmd)
		{
			_ps.AddScript(cmd);
		}

		public void Dispose()
		{
			_ps.Dispose();
	    }
	}

	class Program
	{
		public static void Main(string[] args)
		{
			var ps1 = new PoshCommand();
			ps1.add("Import-Module VMware.VimAutomation.Core");
			//ps.AddScript("connect-viserver -server vcenter -user root -password Taligent | select * | fl | out-string");
			ps1.add("connect-viserver -server vcenter -user root -password Taligent");
			var r = ps1.run();
			Console.WriteLine(r.output);
			if (r.errors != "")
				Console.Error.WriteLine(r.errors.ToString());

			var ps2 = new PoshCommand();
			ps2.add("Import-Module VMware.VimAutomation.Core");
			ps2.add("get-vm rafi_win7-1 | select * -exclude Client,ExtensionData | convertto-json -depth 1");
			var r2 = ps2.run();
			Console.WriteLine(r2.output);
			if (r2.errors != "")
				Console.Error.WriteLine(r2.errors.ToString());
#if false
		    using (PowerShell ps = PowerShell.Create())
		    {
		    	try
		    	{
			        // use "AddScript" to add the contents of a script file to the end of the execution pipeline.
			        // use "AddCommand" to add individual commands/cmdlets to the end of the execution pipeline.
					//ps.AddCommand("connect-viserver -server vcenter -user root -password Taligent");
					//ps.AddCommand("get-vm rafi_win7-1 | select-object name | convertto-json");
					//ps.AddScript("write-output $env:psmodulepath");
					//ps.AddScript("get-module -listavailable");
	
					ps.AddScript("Import-Module VMware.VimAutomation.Core");
					//ps.AddScript("connect-viserver -server vcenter -user root -password Taligent | select * | fl | out-string");
					ps.AddScript("connect-viserver -server vcenter -user root -password Taligent"); // | select * | convertto-json");
					ps.AddScript("get-vm rafi_win7-1 | select * -exclude Client,ExtensionData | convertto-json -depth 1");
					//PowerShellInstance.AddScript("Import-Module VMware.VimAutomation.Core");
					//PowerShellInstance.AddScript("get-vm rafi_win7-1");
			
			        Collection<PSObject> outs = ps.Invoke();
	
			        foreach (PSObject o1 in outs)
			        {
			            // if null object was dumped to the pipeline during the script then a null
			            // object may be present here. check for null to prevent potential NRE.
			            if (o1 != null)
			                Console.WriteLine(o1); // o1.BaseObject); 
			        }
			        if (ps.Streams.Error.Count > 0)
			        {
			        	Console.Write("Errors:\n");
			        	foreach (var e in ps.Streams.Error)
			        		Console.WriteLine(e);
			        }
		    	}
		    	catch (Exception x)
		    	{
		    		Console.Error.WriteLine(x.ToString());
		    	}
		    }
#endif    
		}
	}
}
