﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using System.Management.Automation;
using System.Collections.ObjectModel;
using VMware.Vim;


namespace x1
{
    class Program
    {
        static void Main(string[] args)
        {
            //string text = System.IO.File.ReadAllText(@"C:\Program Files (x86)\Backup Reporter\Required\edit_website.ps1");

            new 
            using (PowerShell PowerShellInstance = PowerShell.Create())
            {
                // use "AddScript" to add the contents of a script file to the end of the execution pipeline.
                // use "AddCommand" to add individual commands/cmdlets to the end of the execution pipeline.
                //PowerShellInstance.AddCommand("connect-viserver -server vcenter -user root -password Taligent");
                //PowerShellInstance.AddCommand("get-vm rafi_win7-1 | select-object name | convertto-json");
                //PowerShellInstance.AddScript("write-output $env:psmodulepath");
                //PowerShellInstance.AddScript("get-module -listavailable");
                //PowerShellInstance.AddScript("connect-viserver -server vcenter -user root -password Taligent");
                PowerShellInstance.AddScript("Import-Module VMware.VimAutomation.Core");
                //PowerShellInstance.AddScript("get-vm rafi_win7-1");

                Collection<PSObject> PSOutput = PowerShellInstance.Invoke();
                foreach (PSObject x in PSOutput)
                {
                    // if null object was dumped to the pipeline during the script then a null
                    // object may be present here. check for null to prevent potential NRE.
                    if (x != null)
                    {
                        Console.WriteLine(x.BaseObject.ToString() + "\n");
                    }
                }
                if (PowerShellInstance.Streams.Error.Count > 0)
                {
                    Console.Write("Errors:\n");
                    foreach (var e in PowerShellInstance.Streams.Error)
                    {
                        Console.Write(e.ToString() + "\n");
                    }
                }
                //Console.ReadKey();
            }
        }
    }
}
