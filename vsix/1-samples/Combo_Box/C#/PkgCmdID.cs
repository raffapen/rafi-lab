﻿
namespace Microsoft.Samples.VisualStudio.ComboBox
{
    static class PkgCmdIDList
    {
        public const uint cmdidMyDropDownCombo          = 0x101;
        public const uint cmdidMyDropDownComboGetList   = 0x102;
        public const uint cmdidMyIndexCombo             = 0x103;
        public const uint cmdidMyIndexComboGetList      = 0x104;
        public const uint cmdidMyMRUCombo               = 0x105;
        public const uint cmdidMyDynamicCombo           = 0x107;
        public const uint cmdidMyDynamicComboGetList    = 0x108;
    };
}