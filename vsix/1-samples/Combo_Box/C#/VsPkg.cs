﻿
using System;
using System.ComponentModel.Design;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Runtime.InteropServices;

using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

[assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", 
	Scope = "namespace", 
	Target = "Microsoft.Samples.VisualStudio.ComboBox")]
namespace Microsoft.Samples.VisualStudio.ComboBox
{
    [PackageRegistration(UseManagedResourcesOnly = true)]

    [InstalledProductRegistration("#100", "#102", "1.0", IconResourceID = 400)]
        
    [ProvideMenuResource("Menus.ctmenu", 1)]

    [Guid(GuidList.guidComboBoxPkgString)]
    public sealed class ComboBoxPackage : Package
    {
        public ComboBoxPackage()
        {
        }

        #region Package Members

        protected override void Initialize()
        {
            base.Initialize();

            OleMenuCommandService mcs = GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (null != mcs)
            {
                var menuMyDropDownComboCommandID = new CommandID(GuidList.guidComboBoxCmdSet, (int)PkgCmdIDList.cmdidMyDropDownCombo);
                var menuMyDropDownComboCommand = new OleMenuCommand(new EventHandler(OnMenuMyDropDownCombo), menuMyDropDownComboCommandID);
                mcs.AddCommand(menuMyDropDownComboCommand);

                var menuMyDropDownComboGetListCommandID = new CommandID(GuidList.guidComboBoxCmdSet, (int)PkgCmdIDList.cmdidMyDropDownComboGetList);
                var menuMyDropDownComboGetListCommand = new OleMenuCommand(new EventHandler(OnMenuMyDropDownComboGetList), menuMyDropDownComboGetListCommandID);
                mcs.AddCommand(menuMyDropDownComboGetListCommand);

                var menuMyIndexComboCommandID = new CommandID(GuidList.guidComboBoxCmdSet, (int)PkgCmdIDList.cmdidMyIndexCombo);
                var menuMyIndexComboCommand = new OleMenuCommand(new EventHandler(OnMenuMyIndexCombo), menuMyIndexComboCommandID);
                mcs.AddCommand(menuMyIndexComboCommand);

                var menuMyIndexComboGetListCommandID = new CommandID(GuidList.guidComboBoxCmdSet, (int)PkgCmdIDList.cmdidMyIndexComboGetList);
                var menuMyIndexComboGetListCommand = new OleMenuCommand(new EventHandler(OnMenuMyIndexComboGetList), menuMyIndexComboGetListCommandID);
                mcs.AddCommand(menuMyIndexComboGetListCommand);

                var menuMyMRUComboCommandID = new CommandID(GuidList.guidComboBoxCmdSet, (int)PkgCmdIDList.cmdidMyMRUCombo);
                var menuMyMRUComboCommand = new OleMenuCommand(new EventHandler(OnMenuMyMRUCombo), menuMyMRUComboCommandID);
                mcs.AddCommand(menuMyMRUComboCommand);

                var menuMyDynamicComboCommandID = new CommandID(GuidList.guidComboBoxCmdSet, (int)PkgCmdIDList.cmdidMyDynamicCombo);
                var menuMyDynamicComboCommand = new OleMenuCommand(new EventHandler(OnMenuMyDynamicCombo), menuMyDynamicComboCommandID);
                mcs.AddCommand(menuMyDynamicComboCommand);

                var menuMyDynamicComboGetListCommandID = new CommandID(GuidList.guidComboBoxCmdSet, (int)PkgCmdIDList.cmdidMyDynamicComboGetList);
                var menuMyDynamicComboGetListCommand = new OleMenuCommand(new EventHandler(OnMenuMyDynamicComboGetList), menuMyDynamicComboGetListCommandID);
                mcs.AddCommand(menuMyDynamicComboGetListCommand);
			}
        }

        #endregion

        #region Combo Box Commands

        private string[] dropDownComboChoices = { Resources.Apples, Resources.Oranges, Resources.Pears, Resources.Bananas };
        private string currentDropDownComboChoice = Resources.Apples;

        private void OnMenuMyDropDownCombo(object sender, EventArgs e)
        {
            OleMenuCmdEventArgs eventArgs = e as OleMenuCmdEventArgs;

            if (eventArgs == null)
				throw (new ArgumentException(Resources.EventArgsRequired));

			string newChoice = eventArgs.InValue as string;
			IntPtr vOut = eventArgs.OutValue;

			if (vOut != IntPtr.Zero)
			{
				Marshal.GetNativeVariantForObject(currentDropDownComboChoice, vOut);
				return;
			}
			if (newChoice == null)
				return;

			bool validInput = false;
			int indexInput = -1;
			for (indexInput = 0; indexInput < dropDownComboChoices.Length; indexInput++)
			{
				if (string.Compare(dropDownComboChoices[indexInput], newChoice, StringComparison.CurrentCultureIgnoreCase) == 0)
				{
					validInput = true;
					break;
				}
			}

			if (!validInput)
				throw (new ArgumentException(Resources.ParamNotValidStringInList));

			currentDropDownComboChoice = dropDownComboChoices[indexInput];
			ShowMessage(Resources.MyDropDownCombo, currentDropDownComboChoice);
        }

        private void OnMenuMyDropDownComboGetList(object sender, EventArgs e)
        {
            OleMenuCmdEventArgs eventArgs = e as OleMenuCmdEventArgs;

            if (eventArgs == null)
				return;

			object inParam = eventArgs.InValue;
			IntPtr vOut = eventArgs.OutValue;

			if (inParam != null)
				throw (new ArgumentException(Resources.InParamIllegal));
			else if (vOut == IntPtr.Zero)
				throw (new ArgumentException(Resources.OutParamRequired));

			Marshal.GetNativeVariantForObject(dropDownComboChoices, vOut);
        }

        private string[] indexComboChoices = { Resources.Lions, Resources.Tigers, Resources.Bears};
        private int currentIndexComboChoice = 0;

        private void OnMenuMyIndexCombo(object sender, EventArgs e)
        {
            if ((null == e) || (e == EventArgs.Empty))
                throw (new ArgumentException(Resources.EventArgsRequired));

            OleMenuCmdEventArgs eventArgs = e as OleMenuCmdEventArgs;

            if (eventArgs != null)
            {
                object input = eventArgs.InValue;
                IntPtr vOut = eventArgs.OutValue;

                if (vOut != IntPtr.Zero && input != null)
                    throw (new ArgumentException(Resources.BothInOutParamsIllegal));
                if (vOut != IntPtr.Zero)
                    Marshal.GetNativeVariantForObject(indexComboChoices[currentIndexComboChoice], vOut);
                else if (input != null)
                {
                    int newChoice = -1;
                    if(!int.TryParse(input.ToString(), out newChoice))
                    {
                        // user typed a string argument in command window.
                        for (int i = 0; i < indexComboChoices.Length; i++)
                        {
                            if (string.Compare(indexComboChoices[i], input.ToString(), StringComparison.CurrentCultureIgnoreCase) == 0)
                            {
                                newChoice = i;
                                break;
                            }
                        }
                    }

                    // new value was selected or typed in
                    if (newChoice != -1)
                    {
                        currentIndexComboChoice = newChoice;
                        ShowMessage(Resources.MyIndexCombo, currentIndexComboChoice.ToString(CultureInfo.CurrentCulture));
                    }
                    else
                        throw (new ArgumentException(Resources.ParamMustBeValidIndexOrStringInList));
                }
                else
                    throw (new ArgumentException(Resources.EventArgsRequired));
            }
            else
                throw (new ArgumentException(Resources.EventArgsRequired));
        }

        private void OnMenuMyIndexComboGetList(object sender, EventArgs e)
        {
            if (e == EventArgs.Empty)
                throw (new ArgumentException(Resources.EventArgsRequired));

            OleMenuCmdEventArgs eventArgs = e as OleMenuCmdEventArgs;

            if (eventArgs == null)
				return;

			object inParam = eventArgs.InValue;
			IntPtr vOut = eventArgs.OutValue;

			if (inParam != null)
				throw (new ArgumentException(Resources.InParamIllegal));
			else if (vOut != IntPtr.Zero)
				Marshal.GetNativeVariantForObject(indexComboChoices, vOut);
			else
				throw (new ArgumentException(Resources.OutParamRequired));
        }

        private string currentMRUComboChoice = null;

        private void OnMenuMyMRUCombo(object sender, EventArgs e)
        {
            if (e == EventArgs.Empty)
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentException(Resources.EventArgsRequired));
            }

            OleMenuCmdEventArgs eventArgs = e as OleMenuCmdEventArgs;

            if (eventArgs != null)
            {
                object input = eventArgs.InValue;
                IntPtr vOut = eventArgs.OutValue;

                if (vOut != IntPtr.Zero && input != null)
                {
                    throw (new ArgumentException(Resources.BothInOutParamsIllegal));
                }
                else if (vOut != IntPtr.Zero)
                {
                    // when vOut is non-NULL, the IDE is requesting the current value for the combo
                    Marshal.GetNativeVariantForObject(currentMRUComboChoice, vOut);
                }

                else if (input != null)
                {
                    string newChoice = input.ToString();

                    // new value was selected or typed in
                    if (!string.IsNullOrEmpty(newChoice))
                    {
                        currentMRUComboChoice = newChoice;
                        ShowMessage(Resources.MyMRUCombo, currentMRUComboChoice);
                    }
                    else
                    {
                        // We should never get here
                        throw (new ArgumentException(Resources.EmptyStringIllegal));
                    }
                }
                else
                {
                    throw (new ArgumentException(Resources.BothInOutParamsIllegal));
                }
            }
            else
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentException(Resources.EventArgsRequired));
            }
        }

        private double[] numericZoomLevels = { 4.0, 3.0, 2.0, 1.5, 1.25, 1.0, .75, .66, .50, .33, .25, .10 };
        private string zoomToFit = Resources.ZoomToFit;
        private string zoom_to_Fit = Resources.Zoom_to_Fit;
        private string[] zoomLevels = null;
        private NumberFormatInfo numberFormatInfo;
        private double currentZoomFactor = 1.0;

        private void OnMenuMyDynamicCombo(object sender, EventArgs e)
        {
            if ((null == e) || (e == EventArgs.Empty))
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentException(Resources.EventArgsRequired));
            }

            OleMenuCmdEventArgs eventArgs = e as OleMenuCmdEventArgs;

            if (eventArgs != null)
            {
                object input = eventArgs.InValue;
                IntPtr vOut = eventArgs.OutValue;

                if (vOut != IntPtr.Zero && input != null)
                {
                    throw (new ArgumentException(Resources.BothInOutParamsIllegal));
                }
                else if (vOut != IntPtr.Zero)
                {
                    // when vOut is non-NULL, the IDE is requesting the current value for the combo
                    if (currentZoomFactor == 0)
                    {
                        Marshal.GetNativeVariantForObject(zoom_to_Fit, vOut);
                    }
                    else
                    {
                        string factorString = currentZoomFactor.ToString("P0", numberFormatInfo);
                        Marshal.GetNativeVariantForObject(factorString, vOut);
                    }

                }
                else if (input != null)
                {
                    // new zoom value was selected or typed in
                    string inputString = input.ToString();

                    if (inputString.Equals(zoomToFit) || inputString.Equals(zoom_to_Fit))
                    {
                        currentZoomFactor = 0;
                        ShowMessage(Resources.MyDynamicCombo, zoom_to_Fit);
                    }
                    else
                    {
                        // There doesn't appear to be any percent-parsing routines in the framework (even though you can create
                        // a localized percentage in a string!).  So, we need to remove any occurence of the localized Percent 
                        // symbol, then parse the value that's left
                        try
                        {
                            float newZoom = Single.Parse(inputString.Replace(NumberFormatInfo.InvariantInfo.PercentSymbol, ""), CultureInfo.CurrentCulture);

                            newZoom = (float)Math.Round(newZoom);
                            if (newZoom < 0)
                            {
                                throw (new ArgumentException(Resources.ZoomMustBeGTZero));
                            }

                            currentZoomFactor = newZoom / (float)100.0;

                            ShowMessage(Resources.MyDynamicCombo, newZoom.ToString(CultureInfo.CurrentCulture));
                        }
                        catch (FormatException)
                        {
                            // user typed in a non-numeric value, ignore it
                        }
                        catch (OverflowException)
                        {
                            // user typed in too large of a number, ignore it
                        }
                    }
                }
                else
                {
                    // We should never get here
                    throw (new ArgumentException(Resources.InOutParamCantBeNULL));
                }
            }
            else
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentException(Resources.EventArgsRequired));
            }
        }

        private void OnMenuMyDynamicComboGetList(object sender, EventArgs e)
        {
            if ((null == e) || (e == EventArgs.Empty))
            {
                // We should never get here; EventArgs are required.
                throw (new ArgumentNullException(Resources.EventArgsRequired));
            }

            OleMenuCmdEventArgs eventArgs = e as OleMenuCmdEventArgs;

            if (eventArgs != null)
            {
                object inParam = eventArgs.InValue;
                IntPtr vOut = eventArgs.OutValue;

                if (inParam != null)
                {
                    throw (new ArgumentException(Resources.InParamIllegal));
                }
                else if (vOut != IntPtr.Zero)
                {
                    // initialize the zoom value array if needed
                    if (zoomLevels == null)
                    {
                        numberFormatInfo = (NumberFormatInfo)CultureInfo.CurrentUICulture.NumberFormat.Clone();
                        if (numberFormatInfo.PercentPositivePattern == 0)
                            numberFormatInfo.PercentPositivePattern = 1;
                        if (numberFormatInfo.PercentNegativePattern == 0)
                            numberFormatInfo.PercentNegativePattern = 1;

                        zoomLevels = new string[numericZoomLevels.Length + 1];
                        for (int i = 0; i < numericZoomLevels.Length; i++)
                        {
                            zoomLevels[i] = numericZoomLevels[i].ToString("P0", numberFormatInfo);
                        }

                        zoomLevels[zoomLevels.Length - 1] = zoom_to_Fit;
                    }

                    Marshal.GetNativeVariantForObject(zoomLevels, vOut);
                }
                else
                {
                    throw (new ArgumentException(Resources.OutParamRequired));
                }
            }
        }
        #endregion

        // Helper method to show a message box using the SVsUiShell/IVsUiShell service
        public void ShowMessage(string title, string message)
        {            
            IVsUIShell uiShell = (IVsUIShell)GetService(typeof(SVsUIShell));
            Guid clsid = Guid.Empty;
            int result = VSConstants.S_OK;
            int hr = uiShell.ShowMessageBox(0,
                                ref clsid,
                                title,
                                message,
                                null,
                                0,
                                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST,
                                OLEMSGICON.OLEMSGICON_INFO,
                                0,        // false = application modal; true would make it system modal
                                out result);
            ErrorHandler.ThrowOnFailure(hr);
        }
    }
}