@echo off

setlocal
cd /d %~dp0

call fab-setenv.bat

call fab-create.bat
call fab-deploy.bat
call fab-gems.bat
