
setlocal

mkdir %fabroot%
call dovito psubst d: %fabroot% /p

set pkg_root=%DEVITO_XROOT%\pkg\ruby-windows-%RUBYVER%

pushd %fabroot%
call dovito 7z x %pkg_root%\ruby-%RUBYVER%-x64-mingw32.7z
mklink /d %fabroot%\Ruby %fabroot%\ruby-%RUBYVER%-x64-mingw32

set devkit=DevKit-mingw64-64-4.7.2
mkdir %devkit%
cd %devkit%
call dovito 7z x %pkg_root%\DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe
cd ..
mklink /d %fabroot%\DevKit %fabroot%\%devkit%
