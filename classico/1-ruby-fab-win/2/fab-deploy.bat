
setlocal

mkdir %RUBYROOT%
pushd %RUBYROOT%

mkdir ruby
xcopy /s /e /q %fabroot%\Ruby\*.* ruby

mkdir gems
xcopy /s /e /q %fabroot%\Ruby\lib\ruby\gems\%RUBYVER%\*.* gems

mkdir libs\x64-mingw32

xcopy /q %fabroot%\Gemfile .
