: @echo off

setlocal
: call fab-setenv.bat

mkdir %RUBYROOT%
pushd %RUBYROOT%

mkdir ruby
xcopy /s /e /q c:\root\Ruby\*.* ruby

mkdir gems
xcopy /s /e /q c:\root\Ruby\lib\ruby\gems\%RUBYVER%\*.* gems

mkdir libs\x64-mingw32

xcopy /q c:\root\Gemfile .

gem install rake bundler
bundle install
