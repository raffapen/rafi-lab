@echo off

set RUBYVER=2.3.0
set RUBYROOT=c:\root\ruby-%RUBYVER%-windows-x64

set RUBYARCH=x64-mingw32
set RUBYVARCH=%RUBYVER%\%RUBYARCH%
set RUBYBINARCH=x64-mingw32
set RUBYLIBROOT=%RUBYROOT%\ruby\lib\ruby

set GEM_HOME=%RUBYROOT%\gems

set GEM_PATH=^
%RUBYROOT%\gems;^
%GEM_HOME%

set RUBY_PATH=^
%RUBYROOT%\gems\bin;^
%RUBYROOT%\ruby\bin;^
%RUBYROOT%\ruby\lib;^
%RUBYROOT%\ruby\lib\ruby\%RUBYVARCH%

set RUBYLIB=^
%RUBYLIBROOT%\%RUBYVER%;^
%RUBYLIBROOT%\%RUBYVARCH%

set PATH=%RUBY_PATH%;%PATH%

: call d:\DevKit\devkitvars.bat
set RI_DEVKIT=c:\root\DevKit
set PATH=%RI_DEVKIT%bin;%RI_DEVKIT%mingw\bin;%PATH%
