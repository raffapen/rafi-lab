@echo off

setlocal

if not exist v:\ net use v: \\nx1\site\vue
if not exist x:\ net use x: \\nx1\site\vue\devito1\root

set DEVITO_XROOT=x:

set PATH=%PATH%;%DEVITO_XROOT%\os\windows\scripts\bin

pushd v:\%view%\classico-ruby\fab\windows

call fab-setenv.bat

call fab-base.bat
call fab-ruby.bat
call fab-mkenv.bat
