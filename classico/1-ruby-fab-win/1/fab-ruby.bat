: @echo off

setlocal
: set DEVITO_XROOT=n:\lab\devito
: call %DEVITO_XROOT%\os\windows\scripts\bin\devito-guest-setenv.bat

set RUBYVER=2.3.0

set pkg_dir=%DEVITO_SITE%\pkg\ruby-windows-%RUBYVER%

pushd d:\
call dovito 7z x %pkg_dir%\ruby-%RUBYVER%-x64-mingw32.7z
mklink /d c:\root\Ruby c:\root\ruby-%RUBYVER%-x64-mingw32

set devkit=DevKit-mingw64-64-4.7.2
mkdir %devkit%
cd %devkit%
call dovito 7z x %pkg_dir%\DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe
cd ..
mklink /d c:\root\DevKit c:\root\%devkit%
