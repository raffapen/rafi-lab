@echo off

setlocal

choco install choco install ruby --version 2.3.0 -my
mklink /d d:\Ruby c:\tools\ruby23

choco install -y ruby2.devkit
mklink /d d:\DevKit c:\tools\DevKit2
