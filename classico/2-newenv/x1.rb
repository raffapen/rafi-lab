
Bento.chdir_script

def setenv
	@fabroot = "c:\\root"
	@view = Devito.pwv
	@vroot = Devito.vroot

	E.RUBYVER = "2.3.0"
	E.RUBYROOT = "#{@vroot}\\ruby-$RUBYVER-windows-x64"

	E.RUBYARCH = "x64-mingw32"
	E.RUBYVARCH = "$RUBYVER\\%RUBYARCH"
	E.RUBYBINARCH = "x64-mingw32"
	E.RUBYLIBROOT = "$RUBYROOT\\ruby\\lib\\ruby"

	E.GEM_HOME = "$RUBYROOT\\gems"

	E.GEM_PATH = %{
		$RUBYROOT\\gems;
		$GEM_HOME}

	E.RUBY_PATH = %{
		$RUBYROOT\\gems\\bin;
		$RUBYROOT\\ruby\\bin;
		$RUBYROOT\\ruby\\lib;
		$RUBYROOT\\ruby\\lib\\ruby\\$RUBYVARCH}.u2w

	set RUBYLIB=^
	%RUBYLIBROOT%\%RUBYVER%;^
	%RUBYLIBROOT%\%RUBYVARCH%

	set PATH=%RUBY_PATH%;%PATH%

	set RI_DEVKIT=%fabroot%\DevKit
	set PATH=%RI_DEVKIT%\bin;%RI_DEVKIT%\mingw\bin;%PATH%
end

def fab_create
	mkdir(%fabroot%)
	call dovito psubst d: %fabroot% /p

	set pkg_root=%DEVITO_XROOT%\pkg\ruby-windows-%RUBYVER%

	pushd %fabroot%
	call dovito 7z x %pkg_root%\ruby-%RUBYVER%-x64-mingw32.7z
	mklink /d %fabroot%\Ruby %fabroot%\ruby-%RUBYVER%-x64-mingw32

	set devkit=DevKit-mingw64-64-4.7.2
	mkdir %devkit%
	cd %devkit%
	call dovito 7z x %pkg_root%\DevKit-mingw64-64-4.7.2-20130224-1432-sfx.exe
	cd ..
	mklink /d %fabroot%\DevKit %fabroot%\%devkit%
end

def fab_deploy
end

def fab_gems
end
