#!/usr/bin/env ruby

require 'Bento'

prog = ". /etc/devito; ls $DEVITO_SCRIPTS"
user = Bento::System.user
localhost = Bento::System.hostname
rhost = ARGV[0] # "ub15"
bb

cmd = <<~ENDX
	set -x

	FBACKD=$(mktemp -d /tmp/fback.XXXXXX)
	prog='#{prog}'
	at=""

	ssh #{user}@#{rhost} "HOST=#{user}@#{localhost} PROG='$prog' AT='$at' FBACK='$FBACKD/fback.tgz' bash -s" <<'END'
		set -x

		fbackd=$(mktemp -d /tmp/fback.XXXXXX)
		fout=$fbackd/out
		ferr=$fbackd/err
		frc=$fbackd/rc
		fpack=$fbackd/pack.tgz

		[[ -z $AT ]] && cd "$AT"
		# echo "{ $PROG ;}"
		{ eval $PROG ;} 1> $fout 2> $ferr
		rc=$?
		echo $rc > $frc
		tar czf $fpack -C $fbackd out err rc
		scp $fpack $HOST:$FBACK
		[[ -d $fbackd ]] && rm -rf $fbackd
		exit $rc
	END

	tar xf $FBACKD/fback.tgz -m -C $FBACKD
	cat $FBACKD/out
	# cat $FBACKD/err
	rc=`cat $FBACKD/rc`
	[[ -d $FBACKD ]] && rm -rf $FBACKD
	exit $rc
	ENDX
c1 = Bento.tempfile(cmd)
bb
c2 = systema(c1)
puts c2.out_s if c2.ok?
(~c1).unlink
bb
# puts "fin"
